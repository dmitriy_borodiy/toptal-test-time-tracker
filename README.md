# Time Tracker App - Test Project from TopTal

Implemented by Dmitriy Borodiy aka dmitru.
Email: dmitriy.borodiy@gmail.com

## How to Run

### Locally

Install all project dependencies first: `npm i`.

The app needs certain environment variables to be set. In development, it's handy
to specify them in the `PROJECT_ROOT/.env` file.

Here's an example of `.env` file:
```
MAILGUN_USERNAME='postmaster@sandboxabcdef.mailgun.org'
MAILGUN_PASSWORD='abcfeg'

MONGODB='mongodb://localhost/time-tracker'

TOKEN_SECRET='a-secret-string'

RECAPTCHA_SECRET='recaptcha secret string'
RECAPTCHA_SITE_KEY='recaptcha site key'
```

The names of the variables should be self-explanatory.

For development, run `npm run start:dev` and then access the app at http://localhost:3000.


### ... on Heroku

The project is ready to be deployed to Heroku (e.g. via Heroku git deployment method).
Before doing that, please provide a MongoDB server and set the environment
variables listed above.

## After Deployment

### Create Generated Data

There's a script for generating lots of test users and entries.
**Warning! Doing this will delete all existing data from the database.**

#### Locally:
```
NODE_ENV=test node bin/create_test_data.js
```

#### For a Remote MongoDB Database:
```
NODE_ENV=test MONGODB=mongodb://user:MONGO_USER@MONGO_SERVER:MONGO_PORT node bin/create_test_data.js
```

### Create an Account for System Administrator User

There's a specia luser account in the system for recovery purposes (imagine the last admin user has removed her account). There's a script for creation of such a user:
```
NODE_ENV=test SYSTEM_ADMIN_EMAIL=example@mail.com SYSTEM_ADMIN_PASSWORD=super-secure-password node bin/create_system_admin.js
```
**The script will delete an existing system administrator user.**


## Unit Tests

There are unit tests for the API, use `npm run test:server` to run them.

## Project Specification

* User must be able to create an account and log in.
* User can add (and edit and delete) a row what he has worked on, what date, for how long.
* User can add a setting (Preferred working hours per day).
* If on a particular date a user has worked under the PreferredWorkingHourPerDay, these rows are red, otherwise green.
* Implement at least three roles with different permission levels.
  * a regular user would only be able to CRUD on their owned records,
  * a user manager would be able to CRUD users,
  * and an admin would be able to CRUD all records and users.
* Filter entries by date from-to.
* Export the filtered times to a sheet in HTML:

```
Date: 21.5
- Total time: 9h
- Notes:
   - Note1
   - Note2
   - Note3
```
* REST API. Make it possible to perform all user actions via the API, including authentication (If a mobile application and you don’t know how to create your own backend you can use Firebase.com or similar services to create the API).
 * In any case you should be able to explain how a REST API works and demonstrate that by creating functional tests that use the REST Layer directly. Please be prepared to use REST clients like Postman, cURL, etc. for this purpose.
All actions need to be done client side using AJAX, refreshing the page is not acceptable. (If a mobile app, disregard this)
* Bonus: unit and e2e tests!
* You will not be marked on graphic design, however, do try to keep it as tidy as possible.
