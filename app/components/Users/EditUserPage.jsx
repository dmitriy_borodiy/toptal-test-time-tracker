import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import * as actions from 'root/app/actions/users';
import { clearMessages } from 'root/app/actions/messages';

import { userPropType, messagesPropType } from 'root/app/proptypes';
import { USER_ROLES } from 'root/server/models/constants';

import EditUserForm from 'root/app/components/Users/EditUserForm';
import Loader from 'root/app/components/Loader';
import Messages from 'root/app/components/Messages';

class EditUserPage extends React.Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDeleteUser = this.handleDeleteUser.bind(this);
  }

  componentDidMount() {
    if (!this.props.user) {
      this.props.fetchUserForEditPage(this.props.params.userId);
    }
  }

  handleSubmit(data) {
    const { user } = this.props;
    const saveUserData = Object.assign({}, { id: user.id }, data);
    this.props.saveUser(saveUserData);
  }

  handleDeleteUser(event) {
    event.preventDefault();
    this.props.deleteUser(this.props.user.id);
  }

  render() {
    const {
      me,
      user,
      canEditRole,
      messages,
      isLoading,
      isSubmitting,
    } = this.props;

    let content;
    if (user) {
      content = (
        <EditUserForm
          user={user}
          onChange={this.props.clearMessages}
          onSubmit={this.handleSubmit}
          canEditRole={canEditRole}
          isSubmitting={isSubmitting}
        />
      );
    } else if (isLoading) {
      content = <Loader />;
    }

    const canCreateEntriesForUser = me.role === USER_ROLES.ADMIN;

    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className="panel">
              <div className="panel-body">
                <Messages messages={messages} />
                {content}
              </div>
            </div>

            {user && canCreateEntriesForUser && (
              <div className="panel">
                <div className="panel-body">
                  <legend>Manage user entries</legend>
                  <div className="form-group">
                    <div className="col-sm-offset-3 col-sm-9">
                      <Link
                        className="btn btn-primary"
                        to={`/users/${user.id}/new-entry/`}
                      >
                        Create entry for user
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            )}

            {user && user.id !== me.id && (
              <div className="panel">
                <div className="panel-body">
                  <form onSubmit={this.handleDeleteUser} className="form-horizontal">
                    <legend>Delete User</legend>
                    <div className="form-group">
                      <div className="col-sm-offset-3 col-sm-9">
                        <button type="submit" className="btn btn-danger">Delete user</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

EditUserPage.propTypes = {
  user: userPropType,
  me: userPropType,
  canEditRole: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
  fetchUserForEditPage: PropTypes.func.isRequired,
  saveUser: PropTypes.func.isRequired,
  deleteUser: PropTypes.func.isRequired,
  clearMessages: PropTypes.func.isRequired,
  messages: messagesPropType,
  params: PropTypes.shape({
    userId: PropTypes.string.isRequired,
  }).isRequired,
};

EditUserPage.defaultProps = {
  messages: null,
  user: null,
  me: null,
};

const mapStateToProps = state => ({
  canEditRole: state.auth.user && state.auth.user.role === USER_ROLES.ADMIN,
  messages: state.messages,
  me: state.auth.user,
  user: state.editUserPage.user,
  isLoading: state.editUserPage.isLoading,
  isSubmitting: state.editUserPage.isSubmitting,
});

const actionCreators = dispatch => bindActionCreators({
  fetchUserForEditPage: actions.fetchUserForEditPage,
  saveUser: actions.saveUser,
  deleteUser: actions.deleteUser,
  clearMessages,
}, dispatch);

export default connect(mapStateToProps, actionCreators)(EditUserPage);
