import React, { PropTypes } from 'react';
import Button from 'react-bootstrap-button-loader';

import { userPropType } from 'root/app/proptypes';
import { USER_ROLES } from 'root/server/models/constants';

class EditUserForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: props.user.email,
      name: props.user.name,
      location: props.user.location,
      website: props.user.website,
      gravatar: props.user.gravatar,
      preferredHoursPerDay: props.user.preferredHoursPerDay,
      role: props.user.role,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
    if (this.props.onChange) {
      this.props.onChange(this.state);
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.onSubmit(this.state);
  }

  render() {
    const { canEditRole, isSubmitting } = this.props;
    return (
      <form onSubmit={this.handleSubmit} className="form-horizontal">
        <legend>Profile</legend>
        <div className="form-group">
          <label htmlFor="email" className="col-sm-3">Email</label>
          <div className="col-sm-7">
            <input
              required
              type="email"
              name="email"
              id="email"
              className="form-control"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div className="form-group">
          <label htmlFor="name" className="col-sm-3">Name</label>
          <div className="col-sm-7">
            <input
              required
              type="text"
              name="name"
              id="name"
              className="form-control"
              value={this.state.name}
              onChange={this.handleChange}
            />
          </div>
        </div>
        {canEditRole && (
          <div className="form-group">
            <label htmlFor="role" className="col-sm-3">Role</label>
            <div className="col-sm-7">
              <select
                className="form-control"
                id="role"
                name="role"
                onChange={this.handleChange}
                required
                value={this.state.role}
              >
                <option value={USER_ROLES.USER}>User</option>
                <option value={USER_ROLES.MANAGER}>Manager</option>
                <option value={USER_ROLES.ADMIN}>Admin</option>
              </select>
            </div>
          </div>
        )}
        <div className="form-group">
          <label htmlFor="preferredHoursPerDay" className="col-sm-3">Preferred hours per day</label>
          <div className="col-sm-7">
            <input
              type="number"
              name="preferredHoursPerDay"
              id="preferredHoursPerDay"
              min="0"
              max="24"
              required
              className="form-control"
              value={this.state.preferredHoursPerDay}
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div className="form-group">
          <label htmlFor="location" className="col-sm-3">Location</label>
          <div className="col-sm-7">
            <input
              type="text"
              name="location"
              id="location"
              className="form-control"
              value={this.state.location}
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div className="form-group">
          <label htmlFor="website" className="col-sm-3">Website</label>
          <div className="col-sm-7">
            <input
              type="text"
              name="website"
              id="website"
              className="form-control"
              value={this.state.website}
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div className="form-group">
          <label className="col-sm-3" htmlFor="gravatar">Gravatar</label>
          <div className="col-sm-4">
            <img src={this.state.gravatar} width="100" height="100" className="profile" alt="Profile" />
          </div>
        </div>
        <div className="form-group">
          <div className="col-sm-offset-3 col-sm-4">
            <Button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
              loading={isSubmitting}
            >
              Update Profile
            </Button>
          </div>
        </div>
      </form>
    );
  }
}

EditUserForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onChange: PropTypes.func,
  user: userPropType.isRequired,
  canEditRole: PropTypes.bool.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
};

EditUserForm.defaultProps = {
  onChange: null,
  canEditRole: false,
};

export default EditUserForm;
