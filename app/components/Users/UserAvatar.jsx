import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import { userPropType } from 'root/app/proptypes';

function UserAvatar({ user, me, usernameClassName, avatarClassName, title, alwaysToUserEditPage }) {
  let userName = (
    <span className={usernameClassName}>
      {(user && user.name) || 'Deleted user'}
    </span>
  );
  const userAvatarSrc = (user && (user.picture || user.gravatar)) ||
    'https://gravatar.com/avatar/?s=200&d=mm';
  let userAvatar = (
    <img
      className={avatarClassName}
      src={userAvatarSrc}
      alt={`Avatar for user ${userName}`}
    />
  );
  if (user) {
    const userLink = user.id === me.id && !alwaysToUserEditPage ? '/account' : `/users/${user.id}`;
    userAvatar = <Link title={title} to={userLink}>{userAvatar}</Link>;
    userName = <Link title={title} to={userLink}>{userName}</Link>;
  }

  return (
    <div>
      {userAvatar}
      {userName}
    </div>
  );
}

UserAvatar.propTypes = {
  user: userPropType,
  me: userPropType.isRequired,
  avatarClassName: PropTypes.string,
  usernameClassName: PropTypes.string,
  title: PropTypes.string,
  alwaysToUserEditPage: PropTypes.bool,
};

UserAvatar.defaultProps = {
  user: null,
  avatarClassName: null,
  usernameClassName: null,
  title: 'Open user profile',
  alwaysToUserEditPage: false,
};

const mapStateToProps = state => ({
  me: state.auth.user,
});

export default connect(mapStateToProps)(UserAvatar);
