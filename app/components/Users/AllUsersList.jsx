import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import { USER_ROLES } from 'root/server/models/constants';
import PaginatedList from 'root/app/components/PaginatedList';
import { userPropType } from 'root/app/proptypes';

import UserAvatar from 'root/app/components/Users/UserAvatar';
import { fetchUsers, usersChangePage } from 'root/app/actions/users';

class AllUsersList extends Component {
  constructor(props) {
    super(props);
    this.renderUserItem = this.renderUserItem.bind(this);
  }

  renderUserItem(user) {
    const { me } = this.props;

    let color;
    if (user.role === USER_ROLES.ADMIN) {
      color = 'warning';
    } else if (user.role === USER_ROLES.MANAGER) {
      color = 'success';
    } else {
      color = 'primary';
    }

    const canEditUser = me.role === USER_ROLES.ADMIN ||
      (me.role === USER_ROLES.MANAGER && user.role === USER_ROLES.USER);
    const canCreateUserEntries = me.role === USER_ROLES.ADMIN;

    return (
      <div className="user-item">
        <UserAvatar
          user={user}
          avatarClassName="user-item_avatar"
          usernameClassName="user-item_name"
          title="Edit user profile"
          alwaysToUserEditPage
        />
        <span className="user-item_email">
          {user.email}
        </span>
        <div
          title={`User role is ${user.role}`}
          className={`user-item_role label label-${color}`}
        >
          {user.role}
        </div>
        {canEditUser && (
          <Link
            title="Edit user profile"
            className="items-list_edit-button btn btn-default"
            to={`/users/${user.id}`}
          >
            Edit
          </Link>
        )}
        {canCreateUserEntries && (
          <Link
            title="Create an entry for this user"
            className="items-list_new-entry-button btn btn-primary"
            to={`/users/${user.id}/new-entry/`}
          >
            Add Entry
          </Link>
        )}
      </div>
    );
  }

  render() {
    const {
      users,
      isLoading,
      page,
      pages,
      perPage,
      onChangePage,
    } = this.props;

    return (
      <PaginatedList
        items={users}
        renderItem={this.renderUserItem}
        fetchItems={this.props.fetchUsers}
        isLoading={isLoading}
        page={page}
        pages={pages}
        perPage={perPage}
        onChangePage={onChangePage}
      />
    );
  }
}

AllUsersList.propTypes = {
  me: userPropType.isRequired,
  users: PropTypes.arrayOf(userPropType),
  isLoading: PropTypes.bool.isRequired,
  page: PropTypes.number.isRequired,
  pages: PropTypes.number,
  perPage: PropTypes.number.isRequired,
  fetchUsers: PropTypes.func.isRequired,
  onChangePage: PropTypes.func.isRequired,
};

AllUsersList.defaultProps = {
  entries: null,
  pages: null,
  users: null,
};

const mapStateToProps = state => ({
  me: state.auth.user,
  users: state.allUsersPage.users,
  isLoading: state.allUsersPage.isLoading,
  page: state.allUsersPage.page,
  pages: state.allUsersPage.pages,
  perPage: state.allUsersPage.perPage,
});

const actionCreators = dispatch => bindActionCreators({
  fetchUsers,
  onChangePage: usersChangePage,
}, dispatch);


export default connect(mapStateToProps, actionCreators)(AllUsersList);
