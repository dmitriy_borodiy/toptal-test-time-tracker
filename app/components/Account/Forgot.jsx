import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ReCAPTCHA from 'react-google-recaptcha';
import Button from 'react-bootstrap-button-loader';

import { messagesPropType } from '../../proptypes';
import * as actions from '../../actions/auth';
import Messages from '../Messages';

class Forgot extends React.Component {
  constructor(props) {
    super(props);
    this.state = { email: '', captcha: null };
    this.handleChange = this.handleChange.bind(this);
    this.handleForgot = this.handleForgot.bind(this);
    this.handleCaptchaChange = this.handleCaptchaChange.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleCaptchaChange(value) {
    this.setState({ captcha: value });
  }

  handleForgot(event) {
    event.preventDefault();
    this.props.forgotPassword(this.state);
  }

  render() {
    return (
      <div className="container">
        <div className="panel">
          <div className="panel-body">
            <Messages messages={this.props.messages} />
            <form onSubmit={this.handleForgot}>
              <legend>Forgot Password</legend>
              <div className="form-group">
                <p>{ "Enter your email address below and we'll send you password reset instructions."}</p>
                <label htmlFor="email">Email</label>
                <input
                  required
                  type="email"
                  name="email"
                  id="email"
                  placeholder="Email"
                  className="form-control"
                  autoFocus
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <ReCAPTCHA
                  sitekey={process.env.RECAPTCHA_SITE_KEY}
                  onChange={this.handleCaptchaChange}
                />
              </div>
              <Button
                type="submit"
                className="btn btn-success"
                loading={this.props.isSubmitting}
                disabled={this.props.isSubmitting}
              >
                Reset Password
              </Button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

Forgot.propTypes = {
  forgotPassword: PropTypes.func.isRequired,
  messages: messagesPropType,
  isSubmitting: PropTypes.bool.isRequired,
};

Forgot.defaultProps = {
  messages: null,
  token: null,
  user: null,
};

const mapStateToProps = state => ({
  token: state.auth.token,
  user: state.auth.user,
  messages: state.messages,
  isSubmitting: state.forgotPasswordPage.isSubmitting,
});

const actionCreators = dispatch => bindActionCreators({
  forgotPassword: actions.forgotPassword,
}, dispatch);

export default connect(mapStateToProps, actionCreators)(Forgot);
