import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { userPropType, messagesPropType } from 'root/app/proptypes';
import * as actions from 'root/app/actions/auth';
import Messages from 'root/app/components/Messages';

import EditUserForm from 'root/app/components/Users/EditUserForm';

class Profile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      password: '',
      confirm: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleDeleteAccount = this.handleDeleteAccount.bind(this);
    this.handleProfileUpdate = this.handleProfileUpdate.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleProfileUpdate(data) {
    this.props.updateProfile(data, this.props.token);
  }

  handleChangePassword(event) {
    event.preventDefault();
    this.props.changePassword(this.state.password, this.state.confirm, this.props.token);
  }

  handleDeleteAccount(event) {
    event.preventDefault();
    this.props.deleteAccount(this.props.token);
  }

  render() {
    return (
      <div className="container">
        <div className="panel">
          <div className="panel-body">
            <Messages messages={this.props.messages} />
            <EditUserForm
              user={this.props.user}
              onSubmit={this.handleProfileUpdate}
              isSubmitting={this.props.isSubmitting}
            />
          </div>
        </div>
        <div className="panel">
          <div className="panel-body">
            <form onSubmit={this.handleChangePassword} className="form-horizontal">
              <legend>Change Password</legend>
              <div className="form-group">
                <label htmlFor="password" className="col-sm-3">New Password</label>
                <div className="col-sm-7">
                  <input
                    required
                    type="password"
                    name="password"
                    id="password"
                    className="form-control"
                    value={this.state.password}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div className="form-group">
                <label htmlFor="confirm" className="col-sm-3">Confirm Password</label>
                <div className="col-sm-7">
                  <input
                    required
                    type="password"
                    name="confirm"
                    id="confirm"
                    className="form-control"
                    value={this.state.confirm}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div className="form-group">
                <div className="col-sm-4 col-sm-offset-3">
                  <button type="submit" className="btn btn-success">Change Password</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div className="panel">
          <div className="panel-body">
            <form onSubmit={this.handleDeleteAccount} className="form-horizontal">
              <legend>Delete Account</legend>
              <div className="form-group">
                <p className="col-sm-offset-3 col-sm-9">You can delete your account, but keep in mind this action is irreversible.</p>
                <div className="col-sm-offset-3 col-sm-9">
                  <button type="submit" className="btn btn-danger">Delete my account</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

Profile.propTypes = {
  updateProfile: PropTypes.func.isRequired,
  changePassword: PropTypes.func.isRequired,
  deleteAccount: PropTypes.func.isRequired,
  token: PropTypes.string,
  messages: messagesPropType,
  user: userPropType,
  isSubmitting: PropTypes.bool.isRequired,
};

Profile.defaultProps = {
  messages: null,
  token: null,
  user: null,
};

const mapStateToProps = state => ({
  token: state.auth.token,
  user: state.auth.user,
  messages: state.messages,
  isSubmitting: state.profilePage.isSubmitting,
});

const actionCreators = dispatch => bindActionCreators({
  updateProfile: actions.updateProfile,
  changePassword: actions.changePassword,
  deleteAccount: actions.deleteAccount,
}, dispatch);

export default connect(mapStateToProps, actionCreators)(Profile);
