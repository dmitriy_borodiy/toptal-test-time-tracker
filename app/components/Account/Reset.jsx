import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PasswordInput, { PASSWORD_STRENGTH_SUCCESS } from 'root/app/components/PasswordInput';

import * as actions from '../../actions/auth';
import Messages from '../Messages';

import { messagesPropType } from '../../proptypes';

class Reset extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      passwordStrengthPercent: 0,
      confirm: '',
    };

    this.handleReset = this.handleReset.bind(this);
    this.handleConfirmChange = this.handleConfirmChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

  handlePasswordChange(event, strengthPercent) {
    this.setState({ password: event.target.value, passwordStrengthPercent: strengthPercent });
  }

  handleConfirmChange(event) {
    this.setState({ confirm: event.target.value });
  }

  handleReset(event) {
    event.preventDefault();
    const { resetPassword } = this.props;
    resetPassword(this.state.password, this.state.confirm, this.props.token);
  }

  render() {
    const { messages } = this.props;
    return (
      <div className="container">
        <div className="panel">
          <div className="panel-body">
            <Messages messages={messages} />
            <form onSubmit={this.handleReset}>
              <legend>Reset Password</legend>
              <div className="form-group">
                <label htmlFor="password">New Password</label>
                <PasswordInput
                  required
                  type="password"
                  name="password"
                  id="password"
                  placeholder="New password"
                  className="form-control"
                  autoFocus
                  value={this.state.password}
                  onChange={this.handlePasswordChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="confirm">Confirm Password</label>
                <input
                  required
                  type="password"
                  name="confirm"
                  id="confirm"
                  placeholder="Confirm password"
                  className="form-control"
                  value={this.state.confirm}
                  onChange={this.handleConfirmChange}
                />
              </div>
              <div className="form-group">
                <button
                  type="submit"
                  className="btn btn-success"
                  disabled={this.state.passwordStrengthPercent < PASSWORD_STRENGTH_SUCCESS}
                >
                  Change Password
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}


Reset.propTypes = {
  resetPassword: PropTypes.func.isRequired,
  token: PropTypes.string,
  messages: messagesPropType,
};

Reset.defaultProps = {
  messages: null,
  token: null,
};

const mapStateToProps = (state, ownProps) => ({
  token: ownProps.params.token,
});

const actionCreators = dispatch => bindActionCreators({
  resetPassword: actions.resetPassword,
}, dispatch);

export default connect(mapStateToProps, actionCreators)(Reset);
