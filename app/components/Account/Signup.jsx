import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ReCAPTCHA from 'react-google-recaptcha';
import Button from 'react-bootstrap-button-loader';

import PasswordInput, { PASSWORD_STRENGTH_SUCCESS } from 'root/app/components/PasswordInput';

import { messagesPropType } from '../../proptypes';
import * as actions from '../../actions/auth';
import Messages from '../Messages';

class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
      captcha: null,
      passwordStrengthPercent: 0,
    };

    this.handleSignup = this.handleSignup.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleCaptchaChange = this.handleCaptchaChange.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handlePasswordChange(event, strengthPercent) {
    this.setState({ password: event.target.value, passwordStrengthPercent: strengthPercent });
  }

  handleCaptchaChange(value) {
    this.setState({ captcha: value });
  }

  handleSignup(event) {
    event.preventDefault();
    this.props.signup(this.state);
  }

  render() {
    return (
      <div className="login-container container">
        <div className="panel">
          <div className="panel-body">
            <Messages messages={this.props.messages} />
            <form onSubmit={this.handleSignup}>
              <legend>Create an account</legend>
              <div className="form-group">
                <label htmlFor="name">Name</label>
                <input
                  required
                  type="text"
                  name="name"
                  id="name"
                  placeholder="Name"
                  autoFocus
                  className="form-control"
                  value={this.state.name}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input
                  required
                  type="email"
                  name="email"
                  id="email"
                  placeholder="Email"
                  className="form-control"
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <PasswordInput
                  required
                  type="password"
                  name="password"
                  id="password"
                  placeholder="Password"
                  className="form-control"
                  value={this.state.password}
                  onChange={this.handlePasswordChange}
                />
              </div>
              <div className="form-group">
                <ReCAPTCHA
                  sitekey={process.env.RECAPTCHA_SITE_KEY}
                  onChange={this.handleCaptchaChange}
                />
              </div>
              <Button
                type="submit"
                className="btn btn-success"
                disabled={
                  this.state.passwordStrengthPercent < PASSWORD_STRENGTH_SUCCESS ||
                  this.props.isSubmitting
                }
                loading={this.props.isSubmitting}
              >
                Create an account
              </Button>
            </form>
          </div>
        </div>
        <p className="text-center">
          Already have an account? <Link to="/login"><strong>Log in</strong></Link>
        </p>
      </div>
    );
  }
}

Signup.propTypes = {
  signup: PropTypes.func.isRequired,
  messages: messagesPropType,
  isSubmitting: PropTypes.bool.isRequired,
};

Signup.defaultProps = {
  messages: null,
};

const mapStateToProps = state => ({
  messages: state.messages,
  isSubmitting: state.signupPage.isSubmitting,
});

const actionCreators = dispatch => bindActionCreators({
  signup: actions.signup,
}, dispatch);

export default connect(mapStateToProps, actionCreators)(Signup);
