import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Button from 'react-bootstrap-button-loader';

import { messagesPropType } from '../../proptypes';
import * as actions from '../../actions/auth';
import Messages from '../Messages';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = { email: '', password: '' };
    this.handleLogin = this.handleLogin.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleLogin(event) {
    event.preventDefault();
    this.props.login(this.state.email, this.state.password);
  }

  render() {
    return (
      <div className="login-container container">
        <div className="panel">
          <div className="panel-body">
            <Messages messages={this.props.messages} />
            <form onSubmit={this.handleLogin}>
              <legend>Log In</legend>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input
                  required
                  type="email"
                  name="email"
                  id="email"
                  placeholder="Email"
                  autoFocus
                  className="form-control"
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input
                  required
                  type="password"
                  name="password"
                  id="password"
                  placeholder="Password"
                  className="form-control"
                  value={this.state.password}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group"><Link to="/forgot"><strong>Forgot your password?</strong></Link></div>
              <Button
                type="submit"
                className="btn btn-success"
                disabled={this.props.isSubmitting}
                loading={this.props.isSubmitting}
              >
                Log in
              </Button>
            </form>
          </div>
        </div>
        <p className="text-center">
          { "Don't have an account?" } <Link to="/signup"><strong>Sign up</strong></Link>
        </p>
      </div>
    );
  }
}


Login.propTypes = {
  login: PropTypes.func.isRequired,
  messages: messagesPropType,
  isSubmitting: PropTypes.bool.isRequired,
};

Login.defaultProps = {
  messages: null,
  token: null,
  user: null,
};

const mapStateToProps = state => ({
  token: state.auth.token,
  user: state.auth.user,
  messages: state.messages,
  isSubmitting: state.loginPage.isSubmitting,
});

const actionCreators = dispatch => bindActionCreators({
  login: actions.login,
}, dispatch);

export default connect(mapStateToProps, actionCreators)(Login);
