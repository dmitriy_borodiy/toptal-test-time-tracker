import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { IndexLink, Link } from 'react-router';
import { connect } from 'react-redux';

import { USER_ROLES } from 'root/server/models/constants';
import { userPropType } from '../proptypes';
import * as actions from '../actions/auth';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogout(event) {
    event.preventDefault();
    this.props.logout();
  }

  render() {
    const { user } = this.props;
    const active = { borderBottomColor: '#3f51b5' };
    const displayName = user && (user.name || user.email);
    const canShowAllEntries = user && (
      user.role === USER_ROLES.ADMIN
    );
    const showAllUsers = user && (
      user.role === USER_ROLES.ADMIN || user.role === USER_ROLES.MANAGER
    );

    const rightNav = user ? (
      <ul className="nav navbar-nav navbar-right">
        <li className="dropdown">
          <a href="#" data-toggle="dropdown" className="navbar-avatar dropdown-toggle">
            <img src={user.picture || user.gravatar} alt="User avatar" />
            {` ${displayName} `}
            <i className="caret" />
          </a>
          <ul className="dropdown-menu">
            <li><Link to="/account">My Account</Link></li>
            <li className="divider" />
            <li><a href="#" onClick={this.handleLogout}>Logout</a></li>
          </ul>
        </li>
      </ul>
    ) : (
      <ul className="nav navbar-nav navbar-right">
        <li><Link to="/login" activeStyle={active}>Log in</Link></li>
        <li><Link to="/signup" activeStyle={active}>Sign up</Link></li>
      </ul>
    );
    return (
      <nav className="navbar navbar-default navbar-static-top">
        <div className="container">
          <div className="navbar-header">
            <button type="button" data-toggle="collapse" data-target="#navbar" className="navbar-toggle collapsed">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar" />
              <span className="icon-bar" />
              <span className="icon-bar" />
            </button>
            <IndexLink to="/" className="navbar-brand">Time Tracker</IndexLink>
          </div>
          <div id="navbar" className="navbar-collapse collapse">
            {user && (
              <ul className="nav navbar-nav">
                <li><IndexLink to="/" activeStyle={active}>My Entries</IndexLink></li>
              </ul>
            )}
            {canShowAllEntries && (
              <ul className="nav navbar-nav">
                <li><IndexLink to="/entries/" activeStyle={active}>All Entries</IndexLink></li>
              </ul>
            )}
            {showAllUsers && (
              <ul className="nav navbar-nav">
                <li><IndexLink to="/users/" activeStyle={active}>Users</IndexLink></li>
              </ul>
            )}
            {rightNav}
          </div>
        </div>
      </nav>
    );
  }
}

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  user: userPropType,

  /* eslint-disable react/no-unused-prop-types */
  location: PropTypes.string,
};

Header.defaultProps = {
  user: null,
  location: null,
};

const mapStateToProps = state => ({
  user: state.auth.user,
  location: state.routing.locationBeforeTransitions &&
    state.routing.locationBeforeTransitions.pathname,
});

const actionCreators = dispatch => bindActionCreators({
  logout: actions.logout,
}, dispatch);

export default connect(mapStateToProps, actionCreators)(Header);
