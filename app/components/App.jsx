import React, { PropTypes } from 'react';
import Header from './Header';

function App({ children }) {
  return (
    <div>
      <Header />
      {children}
    </div>
  );
}

App.propTypes = {
  children: PropTypes.node,
};

App.defaultProps = {
  children: null,
};

export default App;
