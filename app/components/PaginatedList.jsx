import React, { Component, PropTypes } from 'react';
import Paginator from 'react-pagify';
import segmentize from 'segmentize';
import pagifyBootstrapPreset from 'react-pagify-preset-bootstrap';

import Loader from 'root/app/components/Loader';

class PaginatedList extends Component {
  constructor(props) {
    super(props);
    this.handlePageSelect = this.handlePageSelect.bind(this);
  }

  componentDidMount() {
    const { items, perPage, page, fetchItems } = this.props;
    if (!items) {
      fetchItems({
        limit: perPage,
        offset: perPage * (page - 1),
      });
    }
  }

  handlePageSelect(newPage, event) {
    event.preventDefault();
    const { pages, onChangePage } = this.props;
    if (newPage > 0 && newPage <= pages) {
      onChangePage(newPage);
    }
  }

  render() {
    const { isLoading, isFiltered, page, pages, items, renderItem } = this.props;

    if (!items && isLoading) {
      return <Loader />;
    }

    let pagination;
    if (items && pages && pages > 1) {
      const segments = segmentize({
        page,
        pages,
        beginPages: 2,
        endPages: 1,
        sidePages: 1,
      });
      pagination = (
        <div className="items-list_pagination">
          <Paginator.Context
            {...pagifyBootstrapPreset}
            className="pagination"
            segments={segments}
            onSelect={this.handlePageSelect}
          >
            <Paginator.Button
              page={page - 1}
              className={page === 1 && 'disabled'}
            >
              Previous
            </Paginator.Button>
            <Paginator.Segment field="beginPages" />
            <Paginator.Ellipsis previousField="beginPages" nextField="previousPages" />
            <Paginator.Segment field="previousPages" />
            <Paginator.Segment field="centerPage" className="active" />
            <Paginator.Segment field="nextPages" />
            <Paginator.Ellipsis previousField="nextPages" nextField="endPages" />
            <Paginator.Segment field="endPages" />
            <Paginator.Button
              page={page + 1}
              className={page === pages && 'disabled'}
            >
              Next
            </Paginator.Button>
          </Paginator.Context>
        </div>
      );
    }
    return (
      <div className="items-list_container">
        {isLoading && (
          <div className="loading-overlay">
            <Loader />
          </div>
        )}
        <div className="items-list">
          { pagination }
          <div className="items-list_content">
            {items && items.length === 0 && (
              isFiltered ? (
                <p>
                  There are no entries for the given dates.
                </p>
              ) : (
                <p>
                  There are no entries yet.
                </p>
              )
            )}
            {items && items.map(item => (
              <div
                key={item.id}
                className="items-list_item"
              >
                {renderItem(item)}
              </div>
            ))}
          </div>
          { pagination }
        </div>
      </div>
    );
  }
}

PaginatedList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object),
  isLoading: PropTypes.bool.isRequired,
  page: PropTypes.number.isRequired,
  pages: PropTypes.number,
  perPage: PropTypes.number.isRequired,
  isFiltered: PropTypes.bool,
  renderItem: PropTypes.func.isRequired,
  fetchItems: PropTypes.func.isRequired,
  onChangePage: PropTypes.func.isRequired,
};

PaginatedList.defaultProps = {
  items: null,
  pages: null,
  isFiltered: false,
};

export default PaginatedList;
