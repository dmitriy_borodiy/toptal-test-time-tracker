import React, { PropTypes } from 'react';

function Messages({ messages }) {
  let alertClass;
  let msgs;
  if (!messages) {
    return null;
  }
  if (messages.success) {
    alertClass = 'success';
    msgs = messages.success;
  } else if (messages.error) {
    alertClass = 'danger';
    msgs = messages.error;
  } else if (messages.info) {
    alertClass = 'info';
    msgs = messages.info;
  } else {
    return null;
  }
  return (
    <div role="alert" className={`alert alert-${alertClass}`}>
      {msgs.map(msg => <div key={msg}>{msg}</div>)}
    </div>
  );
}

Messages.propTypes = {
  messages: PropTypes.shape({
    success: PropTypes.arrayOf(PropTypes.string),
    error: PropTypes.arrayOf(PropTypes.string),
    info: PropTypes.arrayOf(PropTypes.string),
  }),
};

Messages.defaultProps = {
  messages: null,
};

export default Messages;
