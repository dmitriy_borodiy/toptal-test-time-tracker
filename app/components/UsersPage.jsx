import React, { Component } from 'react';
import { connect } from 'react-redux';

import Messages from 'root/app/components/Messages';
import AllUsersList from 'root/app/components/Users/AllUsersList';

import { messagesPropType } from 'root/app/proptypes';

class UsersPage extends Component {
  render() {
    const { messages } = this.props;

    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className="panel">
              <div className="panel-body">
                <h3>Users</h3>
                <Messages messages={messages} />
                <AllUsersList />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  messages: state.messages,
});

UsersPage.propTypes = {
  messages: messagesPropType,
};

UsersPage.defaultProps = {
  messages: null,
};

export default connect(mapStateToProps)(UsersPage);
