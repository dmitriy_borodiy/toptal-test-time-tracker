import React, { PropTypes, Component } from 'react';
import { Link } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import DateFilters from 'root/app/components/Entries/DateFilters';
import Messages from 'root/app/components/Messages';
import MyEntriesList from 'root/app/components/Entries/MyEntriesList';
import MyEntriesExportForPrint from 'root/app/components/Entries/MyEntriesExportForPrint';

import {
  changeMyEntriesFilterStartDate,
  changeMyEntriesFilterEndDate,
} from 'root/app/actions/entries';

import { messagesPropType } from 'root/app/proptypes';

class Home extends Component {
  render() {
    const { isLoading, myUserId, messages, filterStartDate, filterEndDate } = this.props;

    const toolbar = !isLoading && (
      <div className="items-list_toolbar">
        <Link
          to={`/users/${myUserId}/new-entry/`}
          className="btn btn-primary items-list_toolbar_item"
        >
          Add new entry
        </Link>
        <span className="items-list_toolbar_item">
          <DateFilters
            startDate={filterStartDate}
            endDate={filterEndDate}
            onChangeStartDate={this.props.onChangeFilterStartDate}
            onChangeEndDate={this.props.onChangeFilterEndDate}
          />
        </span>
        <span className="items-list_toolbar_item">
          <MyEntriesExportForPrint />
        </span>
      </div>
    );

    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className="panel">
              <div className="panel-body">
                <h3>My Entries</h3>
                <Messages messages={messages} />
                {toolbar}
                <MyEntriesList />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  messages: state.messages,
  myUserId: state.auth.user && state.auth.user.id,
  isLoading: !state.myEntriesPage.entries && state.myEntriesPage.isLoading,
  filterStartDate: state.myEntriesPage.filterStartDate,
  filterEndDate: state.myEntriesPage.filterEndDate,
});

const actionCreators = dispatch => bindActionCreators({
  onChangeFilterStartDate: changeMyEntriesFilterStartDate,
  onChangeFilterEndDate: changeMyEntriesFilterEndDate,
}, dispatch);

Home.propTypes = {
  myUserId: PropTypes.string.isRequired,
  messages: messagesPropType,
  isLoading: PropTypes.bool.isRequired,
  filterStartDate: PropTypes.number,
  filterEndDate: PropTypes.number,
  onChangeFilterStartDate: PropTypes.func.isRequired,
  onChangeFilterEndDate: PropTypes.func.isRequired,
};

Home.defaultProps = {
  messages: null,
  filterStartDate: null,
  filterEndDate: null,
};

export default connect(mapStateToProps, actionCreators)(Home);
