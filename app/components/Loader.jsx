import React from 'react';

function Loader() {
  return <div className="loader-spinner" />;
}

export default Loader;
