import React, { PropTypes, Component } from 'react';
import makeAsyncScriptLoader from 'react-async-script';

const ZXCVBN_URL = 'https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.4.2/zxcvbn.js';

export const PASSWORD_STRENGTH_SUCCESS = 60;
export const PASSWORD_STRENGTH_WARNING = 40;

function getProgressBarParams(zxcvbnResult) {
  if (!zxcvbnResult) {
    return {
      percent: 0,
      style: 'danger',
      feedback: [],
      password: '',
    };
  }
  const percent = Math.round(Math.min(1.0, zxcvbnResult.guesses_log10 / 9.5) * 100);
  let style = 'danger';
  if (percent > PASSWORD_STRENGTH_SUCCESS) {
    style = 'success';
  } else if (percent > PASSWORD_STRENGTH_WARNING) {
    style = 'warning';
  }

  const feedback = zxcvbnResult.feedback.suggestions.slice() || [];
  if (zxcvbnResult.feedback.warning) {
    feedback.unshift(zxcvbnResult.feedback.warning);
  }
  return {
    percent,
    style,
    feedback,
    password: zxcvbnResult.password,
  };
}

class PasswordInput extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      strength: null,
    };
  }

  handleChange(e) {
    const input = e.target.value;
    let currentStrength = this.state.strength;
    if (this.props.zxcvbn) {
      const strength = this.props.zxcvbn(input);
      this.setState({
        strength,
      });
      currentStrength = strength;
    }
    if (this.props.onChange != null) {
      const params = getProgressBarParams(currentStrength);
      this.props.onChange(e, params.percent);
    }
  }

  render() {
    const { showSuggestions } = this.props;
    const progressBarParams = getProgressBarParams(this.state.strength);
    const { password, percent, style, feedback } = progressBarParams;

    const showPasswordSuggestions = showSuggestions &&
      percent < PASSWORD_STRENGTH_SUCCESS &&
      password !== '' &&
      feedback.length > 0;

    return (
      <span className="password-input">
        <input
          className="password-input_input"
          type="password"
          {...this.props}
          onChange={this.handleChange}
        />
        <div className="progress password-input_progress">
          <div
            className={`progress-bar progress-bar-${style}`}
            role="progressbar"
            ariaValuenow={percent}
            ariaValuemin="0"
            ariaValuemax="100"
            style={{ width: `${percent}%` }}
          />
        </div>
        {showPasswordSuggestions && (
          <div className="password-input_suggestions">
            You password is too weak. To improve it, please try the following:
            <ul>
              {feedback.map((text, index) => {
                const key = `${text}-${index}`;
                return <li key={key}>{text}</li>;
              })}
            </ul>
          </div>
        )}
      </span>
    );
  }
}

PasswordInput.propTypes = {
  onChange: PropTypes.func,
  showSuggestions: PropTypes.bool,
  zxcvbn: PropTypes.any,  // eslint-disable-line react/forbid-prop-types
};

PasswordInput.defaultProps = {
  onChange: null,
  zxcvbn: null,
  showSuggestions: true,
};

export default makeAsyncScriptLoader(
  PasswordInput,
  ZXCVBN_URL,
  {
    globalName: 'zxcvbn',
  },
);
