import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import EntriesExportForPrint from 'root/app/components/Entries/EntriesExportForPrint';
import {
  fetchAllEntriesForPrint,
} from 'root/app/actions/entries';


const mapStateToProps = state => ({
  total: state.allEntriesPage.total,
  filterStartDate: state.allEntriesPage.filterStartDate,
  filterEndDate: state.allEntriesPage.filterEndDate,
  disabled: !state.allEntriesPage.total,
  isLoading: state.allEntriesPage.isPrintLoading,
});

const actionCreators = dispatch => bindActionCreators({
  fetchEntries: fetchAllEntriesForPrint,
}, dispatch);

export default connect(mapStateToProps, actionCreators)(EntriesExportForPrint);
