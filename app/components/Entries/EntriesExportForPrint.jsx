import React, { PropTypes, Component } from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import groupBy from 'lodash/groupBy';
import transform from 'lodash/transform';
import flatten from 'lodash/flatten';
import Button from 'react-bootstrap-button-loader';

import { ENTRY_DATE_FORMAT } from 'root/app/components/Entries/constants';

class EntriesExportForPrint extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.renderPrintPage = this.renderPrintPage.bind(this);
  }

  handleClick() {
    const { fetchEntries, filterStartDate, filterEndDate } = this.props;
    fetchEntries({ filterStartDate, filterEndDate, perPage: -1, page: 1 })
      .then((response) => {
        const { data, total } = response;

        const newWindow = window.open();
        const titleElement = newWindow.document.createElement('title');
        titleElement.innerText = 'For Print - Time Tracker';
        newWindow.document.head.appendChild(titleElement);

        const containerElement = newWindow.document.createElement('div');
        newWindow.document.body.appendChild(containerElement);
        ReactDOM.render(
          this.renderPrintPage({ total, entries: data }),
          containerElement);
      })
      .catch(err => err);
  }

  renderPrintPage({ entries }) {
    const {
      filterStartDate,
      filterEndDate,
    } = this.props;

    let title = 'Entries';
    if (filterStartDate != null || filterEndDate != null) {
      if (filterStartDate) {
        title += ` from ${moment(filterStartDate).format(ENTRY_DATE_FORMAT)}`;
      }
      if (filterEndDate != null) {
        title += ` to ${moment(filterEndDate).format(ENTRY_DATE_FORMAT)}`;
      }
    }

    const filteredEntries = entries.filter(entry => entry.user != null);
    const entriesByDateByUser = transform(
      groupBy(filteredEntries, e => moment(e.date).startOf('date').valueOf()),
      (result, value, key) => {
        result[key] = groupBy(value, 'user.id');
      }, {});
    const sortedDates = Object.keys(entriesByDateByUser)
      .map(Number)
      .sort((a, b) => b - a);

    const totalTrackedTime = filteredEntries.map(e => e.trackedSeconds)
      .reduce((acc, val) => acc + val, 0);
    const totalDuration = moment.duration({ seconds: totalTrackedTime });

    return (
      <div>
        <h1>{title}</h1>
        <h4>
          {filteredEntries.length} entries,
          total time tracked: {totalDuration.asHours().toFixed(1)} hours.
        </h4>
        <hr />

        {sortedDates.map((date) => {
          const entriesByUserForDate = entriesByDateByUser[date];
          const userIds = Object.keys(entriesByUserForDate);
          return (
            <div key={date}>
              <span>Date: {moment(date).format(ENTRY_DATE_FORMAT)}</span>
              {userIds.map((userId) => {
                const entriesForDateAndUser = entriesByUserForDate[userId];
                const entry = entriesForDateAndUser[0];
                const user = entry.user;
                const notes = flatten(entriesForDateAndUser.map(e => e.notes));
                const duration = moment.duration({ seconds: entry.totalTrackedSeconds });
                return (
                  <ul key={`${date}-${userId}`}>
                    <li key={`${userId}-name`}>
                      User: {user.name} ({user.email})
                    </li>
                    <li key={`${userId}-total-time`}>
                      Total time: {duration.asHours().toFixed(1)} hours
                    </li>
                    {notes.length > 0 && (
                      <li key={`${userId}-notes`}>
                        Notes:
                        <ul>
                          {notes.map((note, index) => {
                            const key = `${userId}-note-${index}`;
                            return (
                              <li key={key}>
                                {note}
                              </li>
                            );
                          })}
                        </ul>
                      </li>
                    )}
                  </ul>
                );
              })}
            </div>
          );
        })}
      </div>
    );
  }

  render() {
    const {
      disabled,
      isLoading,
      total,
    } = this.props;

    const buttonText = total == null ? 'Print' : `Print (${total} entries)`;

    return (
      <Button
        title="Export filtered entries in printable format (will open a new )"
        className="btn btn-default"
        disabled={disabled || isLoading}
        loading={isLoading}
        onClick={this.handleClick}
        spinColor="#444"
      >
        {buttonText}
      </Button>
    );
  }
}

EntriesExportForPrint.propTypes = {
  total: PropTypes.number,
  disabled: PropTypes.bool,
  filterStartDate: PropTypes.number,
  filterEndDate: PropTypes.number,
  fetchEntries: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

EntriesExportForPrint.defaultProps = {
  total: null,
  disabled: false,
  filterStartDate: null,
  filterEndDate: null,
};

export default EntriesExportForPrint;
