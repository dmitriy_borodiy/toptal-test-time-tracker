import React, { PropTypes } from 'react';

import { entryPropType } from 'root/app/proptypes';

import PaginatedList from 'root/app/components/PaginatedList';
import Entry from 'root/app/components/Entries/Entry';

function renderEntry(entry) {
  let color = 'default';
  if (entry.user) {
    color = entry.totalTrackedSeconds < entry.user.preferredHoursPerDay * 3600 ?
      'danger' : 'success';
  }
  return (
    <Entry
      key={entry.id}
      id={entry.id}
      notes={entry.notes}
      date={entry.date}
      trackedSeconds={entry.trackedSeconds}
      totalTrackedSeconds={entry.totalTrackedSeconds}
      color={color}
      user={entry.user}
    />
  );
}

function EntriesList(props) {
  const {
    entries,
    isLoading,
    isFiltered,
    page,
    pages,
    perPage,
    fetchEntries,
    onChangePage,
  } = props;

  return (
    <PaginatedList
      items={entries}
      renderItem={renderEntry}
      fetchItems={fetchEntries}
      isLoading={isLoading}
      isFiltered={isFiltered}
      page={page}
      pages={pages}
      perPage={perPage}
      onChangePage={onChangePage}
    />
  );
}

EntriesList.propTypes = {
  entries: PropTypes.arrayOf(entryPropType),
  isLoading: PropTypes.bool.isRequired,
  isFiltered: PropTypes.bool,
  page: PropTypes.number.isRequired,
  pages: PropTypes.number,
  perPage: PropTypes.number.isRequired,
  fetchEntries: PropTypes.func.isRequired,
  onChangePage: PropTypes.func.isRequired,
};

EntriesList.defaultProps = {
  isFiltered: false,
  entries: null,
  pages: null,
};

export default EntriesList;
