import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { createEntryForUser } from 'root/app/actions/entries';
import { messagesPropType } from 'root/app/proptypes';

import EditEntryForm from 'root/app/components/Entries/EditEntryForm';
import Messages from 'root/app/components/Messages';

class NewEntryPage extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(data) {
    this.props.createEntryForUser(this.props.params.userId, data);
  }

  render() {
    const { messages } = this.props;

    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className="panel">
              <div className="panel-body">
                <Messages messages={messages} />
                <EditEntryForm
                  showUser={false}
                  showTimestamps={false}
                  headerText="New entry"
                  onSubmit={this.handleSubmit}
                  isSubmitting={this.props.isSubmitting}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

NewEntryPage.propTypes = {
  createEntryForUser: PropTypes.func.isRequired,
  params: PropTypes.shape({
    userId: PropTypes.string.isRequired,
  }).isRequired,
  messages: messagesPropType,
  isSubmitting: PropTypes.bool.isRequired,
};

NewEntryPage.defaultProps = {
  messages: null,
};

const mapStateToProps = state => ({
  messages: state.messages,
  isSubmitting: state.newEntryPage.isSubmitting,
});

const actionCreators = dispatch => bindActionCreators({
  createEntryForUser,
}, dispatch);

export default connect(mapStateToProps, actionCreators)(NewEntryPage);
