import React, { PropTypes } from 'react';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import Button from 'react-bootstrap-button-loader';

import UserAvatar from 'root/app/components/Users/UserAvatar';
import { ENTRY_DATE_FORMAT } from 'root/app/components/Entries/constants';
import { userPropType, entryPropType } from 'root/app/proptypes';

function isBefore(v) {
  return v.isBefore(moment());
}

const HOUR_SELECT_OPTIONS = (function createHourSelectOptions() {
  const options = [];
  for (let hour = 0; hour < 24; ++hour) {
    const m = moment().hours(hour);
    options.push((
      <option key={hour} value={hour}>{m.format('hh A  /  HH')}</option>
    ));
  }
  return options;
}());

const MINUTE_SELECT_OPTIONS = (function createMinuteSelectOptions() {
  const options = [];
  for (let minute = 0; minute < 60; minute += 5) {
    const m = moment().minutes(minute);
    options.push((
      <option key={minute} value={minute}>{m.format('mm')}</option>
    ));
  }
  return options;
}());

class EditEntryForm extends React.Component {
  constructor(props) {
    super(props);

    let date;
    if (props.entry.date != null) {
      date = moment(props.entry.date);
    } else {
      date = moment();
      date.seconds(0);
      date.milliseconds(0);
      date.minutes(Math.floor(date.minutes() / 5) * 5);
    }

    this.state = {
      trackedHours: props.entry.trackedSeconds ? props.entry.trackedSeconds / 3600.0 : null,
      notes: props.entry.notes || [],
      date,
    };

    this.handleChangeTrackedHours = this.handleChangeTrackedHours.bind(this);
    this.handleChangeDate = this.handleChangeDate.bind(this);
    this.handleChangeHours = this.handleChangeHours.bind(this);
    this.handleChangeMinutes = this.handleChangeMinutes.bind(this);
    this.handleChangeNote = this.handleChangeNote.bind(this);
    this.handleRemoveNote = this.handleRemoveNote.bind(this);
    this.handleAddNote = this.handleAddNote.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeTrackedHours(event) {
    this.setState({
      trackedHours: event.target.value,
    });
    if (this.props.onChange) {
      this.props.onChange(this.state);
    }
  }

  handleChangeDate(value) {
    if (value) {
      const newDate = value.clone()
        .date(value.date())
        .month(value.month())
        .year(value.year());
      this.setState({
        date: newDate,
      });
      if (this.props.onChange) {
        this.props.onChange(this.state);
      }
    }
  }

  handleChangeHours(event) {
    this.setState({
      date: this.state.date.clone().hours(event.target.value),
    });
    if (this.props.onChange) {
      this.props.onChange(this.state);
    }
  }

  handleChangeMinutes(event) {
    this.setState({
      date: this.state.date.clone().minutes(event.target.value),
    });
    if (this.props.onChange) {
      this.props.onChange(this.state);
    }
  }

  handleChangeNote(event) {
    const newNotes = this.state.notes.slice();
    newNotes[event.target.dataset.index] = event.target.value;
    this.setState({
      notes: newNotes,
    });
    if (this.props.onChange) {
      this.props.onChange(this.state);
    }
  }

  handleRemoveNote(event) {
    event.preventDefault();
    const index = event.target.dataset.index;
    const oldNote = this.state.notes[index];
    const newNotes = this.state.notes.slice();
    newNotes.splice(index, 1);
    this.setState({
      notes: newNotes,
    });
    if (oldNote !== '' && this.props.onChange) {
      this.props.onChange(this.state);
    }
  }

  handleAddNote(event) {
    event.preventDefault();
    const newNotes = this.state.notes.slice();
    newNotes.push('');
    this.setState({
      notes: newNotes,
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    const newNotes = this.state.notes
      .map(e => e.trim())
      .filter(e => e.length > 0);
    this.setState({
      notes: newNotes,
    });

    this.props.onSubmit(Object.assign({}, this.props.entry, {
      date: this.state.date.valueOf(),
      trackedSeconds: Math.round(this.state.trackedHours * 3600),
      notes: newNotes,
    }));
  }

  render() {
    const {
      isSubmitting,
      entry,
      user,
      showUser,
      showTimestamps,
      headerText,
    } = this.props;

    return (
      <form onSubmit={this.handleSubmit} className="form-horizontal">
        <legend>{headerText}</legend>
        {showUser && (
          <div className="form-group">
            <label htmlFor="entry-user" className="col-sm-3">User</label>
            <div className="col-sm-4">
              <UserAvatar
                user={user}
                avatarClassName="entry-form_user_avatar"
                usernameClassName="entry-form_user_name"
              />
            </div>
          </div>
        )}
        <div className="form-group">
          <label htmlFor="date" className="col-sm-3">Date & time</label>
          <div className="col-sm-7">
            <DatePicker
              title="Date when the task has been finished"
              autoFocus={!this.state.date}
              required
              dateFormat={ENTRY_DATE_FORMAT}
              name="date"
              dropdownMode="select"
              selected={this.state.date}
              onChange={this.handleChangeDate}
              todayButton="Today"
              disabledKeyboardNavigation
              placeholderText={ENTRY_DATE_FORMAT}
              peekNextMonth
              showMonthDropdown
              showYearDropdown
              filterDate={isBefore}
            />
            <span className="edit-entry-form_time-picker time-picker">
              <select
                title="Hours, time when the task has been finished"
                className="form-control time-picker_hour"
                id="time-hours"
                name="time-hours"
                onChange={this.handleChangeHours}
                required
                value={this.state.date.hours()}
              >
                {HOUR_SELECT_OPTIONS}
              </select>
              &nbsp;:&nbsp;
              <select
                title="Minutes, time when the task has been finished"
                className="form-control time-picker_minute"
                id="time-minutes"
                name="time-minute"
                onChange={this.handleChangeMinutes}
                required
                value={Math.floor(this.state.date.minutes() / 5) * 5}
              >
                {MINUTE_SELECT_OPTIONS}
              </select>
            </span>
            <p className="time-picker_timezone-info">
              In local timezone, UTC{moment().format('Z')}.
            </p>
          </div>
        </div>
        <div className="form-group">
          <label htmlFor="tracked-hours" className="col-sm-3">Tracked hours</label>
          <div className="col-sm-7">
            <input
              title="How much time has been spent"
              autoFocus={this.state.date != null}
              required
              type="number"
              min="0.25"
              max="24"
              step="0.25"
              name="tracked-hours"
              id="tracked-hours"
              className="form-control"
              value={this.state.trackedHours}
              onChange={this.handleChangeTrackedHours}
            />
          </div>
        </div>
        <legend>Notes</legend>
        {this.state.notes.length === 0 && (
          <p>There are no notes for this entry.</p>
        )}
        {this.state.notes.map((note, index) => {
          const key = index;
          return (
            <div>
              <div className="form-group" key={key}>
                <label htmlFor="tracked-hours" className="col-sm-3">Note {index + 1}</label>
                <div className="col-sm-7">
                  <textarea
                    name={`note-${index}`}
                    id={`note-${index}`}
                    data-index={index}
                    className="form-control"
                    value={note}
                    onChange={this.handleChangeNote}
                  />
                </div>
              </div>
              <div className="form-group">
                <div className="col-sm-7 col-sm-offset-3">
                  <button
                    type="button"
                    className="btn btn-default pull-right"
                    data-index={index}
                    onClick={this.handleRemoveNote}
                  >
                    Remove
                  </button>
                </div>
              </div>
            </div>
          );
        })}
        <div className="form-group">
          <div className="col-sm-offset-3 col-sm-7">
            <button type="button" className="btn btn-default" onClick={this.handleAddNote}>Add Note</button>
          </div>
        </div>
        <legend>Save Changes</legend>
        <div className="form-group">
          <div className="col-sm-offset-3 col-sm-7">
            <Button
              type="submit"
              className="btn btn-success"
              disabled={isSubmitting}
              loading={isSubmitting}
            >
              Save Changes
            </Button>
          </div>
        </div>
        {showTimestamps && (
          <div className="form-group">
            <div className="col-sm-7 col-sm-offset-3">
              <div className="form_timestamps">
                <span className="form_timestamps_item">
                  Created at: {moment(entry.createdAt).format('LL LTS')}
                </span>
                <span className="form_timestamps_item">
                  Last modified: {moment(entry.updatedAt).format('LL LTS')}
                </span>
              </div>
            </div>
          </div>
        )}
      </form>
    );
  }
}

EditEntryForm.propTypes = {
  headerText: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onChange: PropTypes.func,
  entry: PropTypes.oneOfType([
    entryPropType,
    PropTypes.object,
  ]),
  user: userPropType,
  showUser: PropTypes.bool,
  showTimestamps: PropTypes.bool,
  isSubmitting: PropTypes.bool.isRequired,
};

EditEntryForm.defaultProps = {
  headerText: 'Edit Entry',
  onChange: null,
  entry: {},
  user: null,
  showUser: true,
  showTimestamps: true,
  isSubmitting: false,
};

export default EditEntryForm;
