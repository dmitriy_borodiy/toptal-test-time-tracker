import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import moment from 'moment';

import { userPropType } from 'root/app/proptypes';
import { ENTRY_DATE_FORMAT } from 'root/app/components/Entries/constants';
import UserAvatar from 'root/app/components/Users/UserAvatar';

function Entry({ user, date, totalTrackedSeconds, trackedSeconds, notes, id, color }) {
  const notesList = notes.length > 0 && (
    <div className="entry_notes">
      <ul className="entry_notes_list">
        {notes.map((note, index) => {
          const key = `${note}-${index}`;
          return (
            <li
              key={key}
              className="entry_notes_list_item"
            >
              {note}
            </li>
          );
        })}
      </ul>
    </div>
  );

  const dateTitle = totalTrackedSeconds ?
    `User has ${(totalTrackedSeconds / 3600).toFixed(1)} tracked hours for this date` : '';
  const trackedTimeTitle = user ?
    `User's preferred hours per day is ${user.preferredHoursPerDay}` : '';

  return (
    <div className="entry">
      <div className="entry_main">
        <span className="entry_user">
          <UserAvatar
            user={user}
            avatarClassName="entry_user_avatar"
            usernameClassName="entry_user_name"
          />
        </span>
        <div
          className="entry_date"
          title={dateTitle}
        >
          {moment(date).format(ENTRY_DATE_FORMAT)}
        </div>
        <div
          title={trackedTimeTitle}
          className="entry_tracked-time"
        >
          <div className={`label label-${color}`}>
            {`${(trackedSeconds / 3600.0).toFixed(1)} hours`}
          </div>
        </div>
        <Link
          title="Edit this entry"
          className="entry_edit-button btn btn-default"
          to={`/entries/${id}`}
        >
          Edit
        </Link>
      </div>
      {notesList}
    </div>
  );
}

Entry.propTypes = {
  user: userPropType,
  id: PropTypes.string.isRequired,
  date: PropTypes.number.isRequired,
  trackedSeconds: PropTypes.number.isRequired,
  totalTrackedSeconds: PropTypes.number,
  notes: PropTypes.arrayOf(PropTypes.string).isRequired,
  color: PropTypes.oneOf(['default', 'success', 'danger']).isRequired,
};

Entry.defaultProps = {
  user: null,
  notes: [],
  totalTrackedSeconds: null,
};

export default Entry;
