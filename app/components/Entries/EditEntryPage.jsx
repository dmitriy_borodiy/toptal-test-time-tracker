import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as actions from 'root/app/actions/entries';
import { clearMessages } from 'root/app/actions/messages';

import { entryPropType, messagesPropType } from 'root/app/proptypes';

import EditEntryForm from 'root/app/components/Entries/EditEntryForm';
import Loader from 'root/app/components/Loader';
import Messages from 'root/app/components/Messages';

class EditEntryPage extends React.Component {
  constructor(props) {
    super(props);

    this.handleDeleteEntry = this.handleDeleteEntry.bind(this);
  }

  componentDidMount() {
    if (!this.props.entry) {
      this.props.fetchEntryForEditPage(this.props.params.entryId);
    }
  }

  handleDeleteEntry(event) {
    event.preventDefault();
    this.props.deleteEntry(this.props.entry.id);
  }

  render() {
    const { entry, messages, isLoading } = this.props;

    let content;
    if (entry) {
      content = (
        <EditEntryForm
          entry={entry}
          user={entry.user}
          onSubmit={this.props.saveEntry}
          onChange={this.props.clearMessages}
          isSubmitting={this.props.isSubmitting}
        />
      );
    } else if (isLoading) {
      content = <Loader />;
    }

    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className="panel">
              <div className="panel-body">
                <Messages messages={messages} />
                {content}
              </div>
            </div>

            {entry && (
              <div className="panel">
                <div className="panel-body">
                  <form onSubmit={this.handleDeleteEntry} className="form-horizontal">
                    <legend>Delete Entry</legend>
                    <div className="form-group">
                      <div className="col-sm-offset-3 col-sm-9">
                        <button type="submit" className="btn btn-danger">Delete this entry</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

EditEntryPage.propTypes = {
  entry: entryPropType,
  isLoading: PropTypes.bool.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
  fetchEntryForEditPage: PropTypes.func.isRequired,
  saveEntry: PropTypes.func.isRequired,
  deleteEntry: PropTypes.func.isRequired,
  clearMessages: PropTypes.func.isRequired,
  messages: messagesPropType,
  params: PropTypes.shape({
    entryId: PropTypes.string.isRequired,
  }).isRequired,
};

EditEntryPage.defaultProps = {
  messages: null,
  entry: null,
};

const mapStateToProps = state => ({
  messages: state.messages,
  entry: state.editEntryPage.entry,
  isLoading: state.editEntryPage.isLoading,
  isSubmitting: state.editEntryPage.isSubmitting,
});

const actionCreators = dispatch => bindActionCreators({
  fetchEntryForEditPage: actions.fetchEntryForEditPage,
  saveEntry: actions.saveEntry,
  deleteEntry: actions.deleteEntry,
  clearMessages,
}, dispatch);

export default connect(mapStateToProps, actionCreators)(EditEntryPage);
