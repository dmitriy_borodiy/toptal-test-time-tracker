import React, { PropTypes } from 'react';
import moment from 'moment';
import DatePicker from 'react-datepicker';

import { ENTRY_DATE_FORMAT } from 'root/app/components/Entries/constants';

function isBefore(v) {
  return v.isBefore(moment());
}

function DateFilters({ startDate, endDate, onChangeStartDate, onChangeEndDate }) {
  const startMoment = startDate ? moment(startDate) : null;
  const endMoment = endDate ? moment(endDate) : null;

  return (
    <span>
      <DatePicker
        title="Filter only entries for dates after the selected date"
        dateFormat={ENTRY_DATE_FORMAT}
        selected={startMoment}
        selectsStart
        startDate={startMoment}
        endDate={endMoment}
        onChange={onChangeStartDate}
        placeholderText="Filter from"
        disabledKeyboardNavigation
        isClearable
        peekNextMonth
        showMonthDropdown
        showYearDropdown
        dropdownMode="select"
        todayButton="Today"
        filterDate={isBefore}
      />
      <DatePicker
        title="Filter only entries for dates before the selected date"
        dateFormat={ENTRY_DATE_FORMAT}
        selected={endMoment}
        selectsEnd
        startDate={startMoment}
        endDate={endMoment}
        onChange={onChangeEndDate}
        placeholderText="Filter to"
        disabledKeyboardNavigation
        isClearable
        peekNextMonth
        showMonthDropdown
        showYearDropdown
        dropdownMode="select"
        todayButton="Today"
        filterDate={isBefore}
      />
    </span>
  );
}

DateFilters.propTypes = {
  startDate: PropTypes.number,
  endDate: PropTypes.number,
  onChangeStartDate: PropTypes.func.isRequired,
  onChangeEndDate: PropTypes.func.isRequired,
};

DateFilters.defaultProps = {
  startDate: null,
  endDate: null,
};

export default DateFilters;
