import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import EntriesList from 'root/app/components/Entries/EntriesList';
import {
  fetchAllEntries,
  allEntriesChangePage,
} from 'root/app/actions/entries';


const mapStateToProps = state => ({
  entries: state.allEntriesPage.entries,
  isLoading: state.allEntriesPage.isLoading,
  page: state.allEntriesPage.page,
  pages: state.allEntriesPage.pages,
  perPage: state.allEntriesPage.perPage,
  isFiltered: state.allEntriesPage.filterStartDate != null ||
    state.allEntriesPage.filterEndDate != null,
});

const actionCreators = dispatch => bindActionCreators({
  fetchEntries: fetchAllEntries,
  onChangePage: allEntriesChangePage,
}, dispatch);

export default connect(mapStateToProps, actionCreators)(EntriesList);
