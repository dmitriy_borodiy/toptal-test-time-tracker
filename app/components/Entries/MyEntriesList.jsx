import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import EntriesList from 'root/app/components/Entries/EntriesList';
import {
  fetchMyEntries,
  myEntriesChangePage,
} from 'root/app/actions/entries';


const mapStateToProps = state => ({
  entries: state.myEntriesPage.entries,
  isLoading: state.myEntriesPage.isLoading,
  page: state.myEntriesPage.page,
  pages: state.myEntriesPage.pages,
  perPage: state.myEntriesPage.perPage,
  isFiltered: state.myEntriesPage.filterStartDate != null ||
    state.myEntriesPage.filterEndDate != null,
});

const actionCreators = dispatch => bindActionCreators({
  fetchEntries: fetchMyEntries,
  onChangePage: myEntriesChangePage,
}, dispatch);

export default connect(mapStateToProps, actionCreators)(EntriesList);
