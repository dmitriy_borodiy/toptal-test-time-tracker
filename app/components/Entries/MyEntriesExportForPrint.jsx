import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import EntriesExportForPrint from 'root/app/components/Entries/EntriesExportForPrint';
import {
  fetchMyEntriesForPrint,
} from 'root/app/actions/entries';


const mapStateToProps = state => ({
  total: state.myEntriesPage.total,
  filterStartDate: state.myEntriesPage.filterStartDate,
  filterEndDate: state.myEntriesPage.filterEndDate,
  disabled: !state.myEntriesPage.total,
  isLoading: state.myEntriesPage.isPrintLoading,
});

const actionCreators = dispatch => bindActionCreators({
  fetchEntries: fetchMyEntriesForPrint,
}, dispatch);

export default connect(mapStateToProps, actionCreators)(EntriesExportForPrint);
