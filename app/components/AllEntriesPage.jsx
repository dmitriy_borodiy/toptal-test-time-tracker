import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import DateFilters from 'root/app/components/Entries/DateFilters';
import Messages from 'root/app/components/Messages';
import AllEntriesList from 'root/app/components/Entries/AllEntriesList';
import AllEntriesExportForPrint from 'root/app/components/Entries/AllEntriesExportForPrint';

import {
  changeAllEntriesFilterStartDate,
  changeAllEntriesFilterEndDate,
} from 'root/app/actions/entries';

import { messagesPropType } from 'root/app/proptypes';

class AllEntries extends Component {
  render() {
    const { isLoading, messages, filterStartDate, filterEndDate } = this.props;

    const toolbar = !isLoading && (
      <div className="items-list_toolbar">
        <span className="items-list_toolbar_item">
          <DateFilters
            startDate={filterStartDate}
            endDate={filterEndDate}
            onChangeStartDate={this.props.onChangeFilterStartDate}
            onChangeEndDate={this.props.onChangeFilterEndDate}
          />
        </span>
        <span className="items-list_toolbar_item">
          <AllEntriesExportForPrint />
        </span>
      </div>
    );

    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className="panel">
              <div className="panel-body">
                <h3>All Entries</h3>
                <Messages messages={messages} />
                {toolbar}
                <AllEntriesList />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  messages: state.messages,
  isLoading: !state.allEntriesPage.entries && state.allEntriesPage.isLoading,
  filterStartDate: state.allEntriesPage.filterStartDate,
  filterEndDate: state.allEntriesPage.filterEndDate,
});

const actionCreators = dispatch => bindActionCreators({
  onChangeFilterStartDate: changeAllEntriesFilterStartDate,
  onChangeFilterEndDate: changeAllEntriesFilterEndDate,
}, dispatch);

AllEntries.propTypes = {
  messages: messagesPropType,
  isLoading: PropTypes.bool.isRequired,
  filterStartDate: PropTypes.number,
  filterEndDate: PropTypes.number,
  onChangeFilterStartDate: PropTypes.func.isRequired,
  onChangeFilterEndDate: PropTypes.func.isRequired,
};

AllEntries.defaultProps = {
  messages: null,
  filterStartDate: null,
  filterEndDate: null,
};

export default connect(mapStateToProps, actionCreators)(AllEntries);
