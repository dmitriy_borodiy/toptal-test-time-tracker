import React from 'react';
import { IndexRoute, Route } from 'react-router';
import App from './components/App';
import MyEntriesPage from './components/MyEntriesPage';
import AllEntriesPage from './components/AllEntriesPage';
import NewEntryPage from './components/Entries/NewEntryPage';
import EditEntryPage from './components/Entries/EditEntryPage';
import UsersPage from './components/UsersPage';
import EditUserPage from './components/Users/EditUserPage';
import NotFound from './components/NotFound';
import Login from './components/Account/Login';
import Signup from './components/Account/Signup';
import Profile from './components/Account/Profile';
import Forgot from './components/Account/Forgot';
import Reset from './components/Account/Reset';

export default function getRoutes(store) {
  const ensureAuthenticated = (nextState, replace) => {
    if (!store.getState().auth.token) {
      replace('/login');
    }
  };
  const skipIfAuthenticated = (nextState, replace) => {
    if (store.getState().auth.token) {
      replace('/');
    }
  };
  const clearMessages = () => {
    store.dispatch({
      type: 'CLEAR_MESSAGES',
    });
  };
  const onEnterEditEntry = (nextState, replace) => {
    ensureAuthenticated(nextState, replace);
    store.dispatch({
      type: 'EDIT_ENTRY_PAGE_CLEAR',
    });
  };
  const onEnterEditUser = (nextState, replace) => {
    ensureAuthenticated(nextState, replace);
    store.dispatch({
      type: 'EDIT_USER_PAGE_CLEAR',
    });
  };
  return (
    <Route path="/" component={App}>
      <IndexRoute component={MyEntriesPage} onEnter={ensureAuthenticated} onLeave={clearMessages} />
      <Route path="/entries/:entryId" component={EditEntryPage} onEnter={onEnterEditEntry} onLeave={clearMessages} />
      <Route path="/entries" component={AllEntriesPage} onEnter={ensureAuthenticated} onLeave={clearMessages} />
      <Route path="/users/:userId/new-entry" component={NewEntryPage} onEnter={ensureAuthenticated} onLeave={clearMessages} />
      <Route path="/users/:userId" component={EditUserPage} onEnter={onEnterEditUser} onLeave={clearMessages} />
      <Route path="/users" component={UsersPage} onEnter={ensureAuthenticated} onLeave={clearMessages} />
      <Route path="/login" component={Login} onEnter={skipIfAuthenticated} onLeave={clearMessages} />
      <Route path="/signup" component={Signup} onEnter={skipIfAuthenticated} onLeave={clearMessages} />
      <Route path="/account" component={Profile} onEnter={ensureAuthenticated} onLeave={clearMessages} />
      <Route path="/forgot" component={Forgot} onEnter={skipIfAuthenticated} onLeave={clearMessages} />
      <Route path="/reset/:token" component={Reset} onEnter={skipIfAuthenticated} onLeave={clearMessages} />
      <Route path="*" component={NotFound} onLeave={clearMessages} />
    </Route>
  );
}
