import { PropTypes } from 'react';

export const userPropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  name: PropTypes.string,
  email: PropTypes.string,
  preferredHoursPerDay: PropTypes.number,
});

export const usersPropType = PropTypes.objectOf(userPropType);

export const entryPropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  date: PropTypes.number.isRequired,
  trackedSeconds: PropTypes.number.isRequired,
  user: PropTypes.shape({
    id: PropTypes.string,
    preferredHoursPerDay: PropTypes.number,
  }),
});

export const messagesPropType = PropTypes.shape({
  success: PropTypes.arrayOf(PropTypes.string),
  error: PropTypes.arrayOf(PropTypes.string),
  info: PropTypes.arrayOf(PropTypes.string),
});
