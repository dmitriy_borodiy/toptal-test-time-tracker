const initialState = {
  token: null,
  user: null,
};

export default function authReducer(state = initialState, action) {
  const { type, payload } = action;
  const hydratedState = state.hydrated ? state :
    Object.assign({}, initialState, state, { hydrated: true });
  switch (type) {
    case 'LOGIN_SUCCESS':
    case 'SIGNUP_SUCCESS': {
      return Object.assign({}, hydratedState, {
        token: payload.token,
        user: payload.user,
      });
    }
    case 'UPDATE_PROFILE_SUCCESS': {
      const user = action.payload;
      const newUser = Object.assign({}, state.user, user);
      return Object.assign({}, state, { user: newUser });
    }
    case 'LOGOUT_SUCCESS':
      return initialState;
    default:
      return hydratedState;
  }
}
