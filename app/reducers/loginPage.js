const initialState = {
  isSubmitting: false,
};

export default function loginPageReducer(state = initialState, action) {
  switch (action.type) {
    case 'LOGIN_REQUEST': {
      return Object.assign({}, state, {
        isSubmitting: true,
      });
    }
    case 'LOGIN_FAILURE':
    case 'LOGIN_SUCCESS': {
      return Object.assign({}, state, {
        isSubmitting: false,
      });
    }
    default:
      return state;
  }
}
