const initialState = {
  entries: null,
  isLoading: true,
  isPrintLoading: false,
  total: null,
  pages: null,
  page: 1,
  perPage: 20,
  filterStartDate: null,
  filterEndDate: null,
};

export default function myEntriesPageReducer(state = initialState, action) {
  switch (action.type) {
    case 'FETCH_MY_ENTRIES_REQUEST': {
      return Object.assign({}, state, {
        isLoading: true,
      });
    }
    case 'FETCH_MY_ENTRIES_SUCCESS': {
      const { total, data } = action.payload;
      return Object.assign({}, state, {
        entries: data,
        isLoading: false,
        total,
        pages: Math.ceil(total / state.perPage),
      });
    }
    case 'FETCH_MY_ENTRIES_FAILURE': {
      return Object.assign({}, state, {
        isLoading: false,
        entries: null,
      });
    }
    case 'MY_ENTRIES_CHANGE_PAGE': {
      const newPage = action.payload;
      return Object.assign({}, state, {
        page: newPage,
      });
    }
    case 'UPDATE_PROFILE_SUCCESS':
    case 'SAVE_USER_SUCCESS':
    case 'CREATE_ENTRY_SUCCESS':
    case 'DELETE_ENTRY_SUCCESS':
    case 'SAVE_ENTRY_SUCCESS': {
      return Object.assign({}, state, {
        entries: null,
      });
    }
    case 'MY_ENTRIES_CHANGE_FILTER_START_DATE': {
      return Object.assign({}, state, {
        filterStartDate: action.payload,
      });
    }
    case 'MY_ENTRIES_CHANGE_FILTER_END_DATE': {
      return Object.assign({}, state, {
        filterEndDate: action.payload,
      });
    }
    case 'FETCH_MY_ENTRIES_FOR_PRINT_REQUEST': {
      return Object.assign({}, state, {
        isPrintLoading: true,
      });
    }
    case 'FETCH_MY_ENTRIES_FOR_PRINT_FAILURE':
    case 'FETCH_MY_ENTRIES_FOR_PRINT_SUCCESS': {
      return Object.assign({}, state, {
        isPrintLoading: false,
      });
    }
    case 'LOGOUT_SUCCESS':
      return initialState;
    default:
      return state;
  }
}
