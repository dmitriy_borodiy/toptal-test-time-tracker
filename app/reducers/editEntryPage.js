const initialState = {
  entry: null,
  isLoading: true,
  isSubmitting: false,
};

export default function editEntryPageReducer(state = initialState, action) {
  switch (action.type) {
    case 'FETCH_ENTRY_FOR_EDIT_PAGE_REQUEST': {
      return Object.assign({}, state, {
        entry: null,
        isLoading: true,
      });
    }
    case 'FETCH_ENTRY_FOR_EDIT_PAGE_SUCCESS': {
      const data = action.payload;
      return Object.assign({}, state, {
        entry: data,
        isLoading: false,
      });
    }
    case 'FETCH_ENTRY_FOR_EDIT_PAGE_FAILURE': {
      return Object.assign({}, state, {
        entry: null,
        isLoading: false,
      });
    }
    case 'SAVE_ENTRY_REQUEST': {
      return Object.assign({}, state, {
        isSubmitting: true,
      });
    }
    case 'SAVE_ENTRY_FAILURE': {
      return Object.assign({}, state, {
        isSubmitting: false,
      });
    }
    case 'SAVE_ENTRY_SUCCESS': {
      const newEntry = action.payload;
      delete newEntry.user;
      return Object.assign({}, state, {
        entry: Object.assign({}, state.entry, newEntry),
        isSubmitting: false,
      });
    }
    case 'EDIT_ENTRY_PAGE_CLEAR':
    case 'LOGOUT_SUCCESS':
      return initialState;
    default:
      return state;
  }
}
