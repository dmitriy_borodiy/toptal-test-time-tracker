const initialState = {
  isSubmitting: false,
};

export default function forgotPasswordPageReducer(state = initialState, action) {
  switch (action.type) {
    case 'FORGOT_PASSWORD_REQUEST': {
      return Object.assign({}, state, {
        isSubmitting: true,
      });
    }
    case 'FORGOT_PASSWORD_FAILURE':
    case 'FORGOT_PASSWORD_SUCCESS': {
      return Object.assign({}, state, {
        isSubmitting: false,
      });
    }
    default:
      return state;
  }
}
