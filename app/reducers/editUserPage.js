const initialState = {
  user: null,
  isLoading: true,
  isSubmitting: false,
};

export default function editUserPageReducer(state = initialState, action) {
  switch (action.type) {
    case 'FETCH_USER_FOR_EDIT_PAGE_REQUEST': {
      return Object.assign({}, state, {
        user: null,
        isLoading: true,
      });
    }
    case 'FETCH_USER_FOR_EDIT_PAGE_SUCCESS': {
      const data = action.payload;
      return Object.assign({}, state, {
        user: data,
        isLoading: false,
      });
    }
    case 'FETCH_USER_FOR_EDIT_PAGE_FAILURE': {
      return Object.assign({}, state, {
        user: null,
        isLoading: false,
      });
    }
    case 'SAVE_USER_REQUEST': {
      return Object.assign({}, state, {
        isSubmitting: true,
      });
    }
    case 'SAVE_USER_SUCCESS':
    case 'SAVE_USER_FAILURE': {
      return Object.assign({}, state, {
        isSubmitting: false,
      });
    }
    case 'EDIT_USER_PAGE_CLEAR':
    case 'LOGOUT_SUCCESS':
      return initialState;
    default:
      return state;
  }
}
