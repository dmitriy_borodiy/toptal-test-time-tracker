const messageKeyMap = {
  SHOW_SUCCESS_MESSAGE: 'success',
  SHOW_INFO_MESSAGE: 'info',
  SHOW_ERROR_MESSAGE: 'error',
};

export default function messagesReducer(state = {}, action) {
  switch (action.type) {
    case 'SHOW_INFO_MESSAGE':
    case 'SHOW_ERROR_MESSAGE':
    case 'SHOW_SUCCESS_MESSAGE': {
      const payload = action.payload;
      let msgs = [];
      if (Array.isArray(payload)) {
        msgs = payload;
        msgs = msgs.filter(m => m && m.msg).map(m => m.msg);
      } else if (payload && payload.msg) {
        msgs = [payload.msg];
      } else if (typeof payload === 'string') {
        msgs = [payload];
      }
      if (msgs.length === 0) {
        return state;
      }
      return {
        [messageKeyMap[action.type]]: msgs,
      };
    }
    case 'CLEAR_MESSAGES':
      if (Object.keys(state).length === 0) {
        return state;
      }
      return {};
    default:
      return state;
  }
}
