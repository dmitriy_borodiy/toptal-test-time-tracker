const initialState = {
  users: null,
  isLoading: true,
  total: null,
  pages: null,
  page: 1,
  perPage: 20,
};

export default function allUsersPageReducer(state = initialState, action) {
  switch (action.type) {
    case 'FETCH_USERS_REQUEST': {
      return Object.assign({}, state, {
        isLoading: true,
      });
    }
    case 'FETCH_USERS_SUCCESS': {
      const { total, data } = action.payload;
      return Object.assign({}, state, {
        users: data,
        isLoading: false,
        total,
        pages: Math.ceil(total / state.perPage),
      });
    }
    case 'FETCH_USERS_FAILURE': {
      return Object.assign({}, state, {
        isLoading: false,
        users: null,
      });
    }
    case 'USERS_CHANGE_PAGE': {
      const newPage = action.payload;
      return Object.assign({}, state, {
        page: newPage,
      });
    }
    case 'DELETE_USER_SUCCESS':
    case 'SAVE_USER_SUCCESS':
    case 'LOGOUT_SUCCESS':
      return initialState;
    default:
      return state;
  }
}
