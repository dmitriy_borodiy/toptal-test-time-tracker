import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import messages from './messages';

import auth from './auth';
import profilePage from './profilePage';
import loginPage from './loginPage';
import signupPage from './signupPage';
import forgotPasswordPage from './forgotPasswordPage';

import allUsersPage from './allUsersPage';
import editUserPage from './editUserPage';

import newEntryPage from './newEntryPage';
import editEntryPage from './editEntryPage';
import myEntriesPage from './myEntriesPage';
import allEntriesPage from './allEntriesPage';

export default combineReducers({
  messages,

  auth,
  profilePage,
  loginPage,
  signupPage,
  forgotPasswordPage,

  allUsersPage,
  editUserPage,

  myEntriesPage,
  allEntriesPage,
  editEntryPage,
  newEntryPage,

  routing: routerReducer,
});
