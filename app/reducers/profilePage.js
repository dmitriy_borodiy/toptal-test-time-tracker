const initialState = {
  isSubmitting: false,
};

export default function profilePageReducer(state = initialState, action) {
  switch (action.type) {
    case 'UPDATE_PROFILE_REQUEST': {
      return Object.assign({}, state, {
        isSubmitting: true,
      });
    }
    case 'UPDATE_PROFILE_FAILURE':
    case 'UPDATE_PROFILE_SUCCESS': {
      return Object.assign({}, state, {
        isSubmitting: false,
      });
    }
    default:
      return state;
  }
}
