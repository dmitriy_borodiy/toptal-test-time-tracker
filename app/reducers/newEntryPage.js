const initialState = {
  isSubmitting: false,
};

export default function newEntryPageReducer(state = initialState, action) {
  switch (action.type) {
    case 'CREATE_ENTRY_REQUEST': {
      return Object.assign({}, state, {
        isSubmitting: true,
      });
    }
    case 'CREATE_ENTRY_FAILURE':
    case 'CREATE_ENTRY_SUCCESS': {
      return Object.assign({}, state, {
        isSubmitting: false,
      });
    }
    default:
      return state;
  }
}
