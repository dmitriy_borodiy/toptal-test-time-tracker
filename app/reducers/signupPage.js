const initialState = {
  isSubmitting: false,
};

export default function signupPageReducer(state = initialState, action) {
  switch (action.type) {
    case 'SIGNUP_REQUEST': {
      return Object.assign({}, state, {
        isSubmitting: true,
      });
    }
    case 'SIGNUP_FAILURE':
    case 'SIGNUP_SUCCESS': {
      return Object.assign({}, state, {
        isSubmitting: false,
      });
    }
    default:
      return state;
  }
}
