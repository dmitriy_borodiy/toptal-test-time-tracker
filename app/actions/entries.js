import { browserHistory } from 'react-router';
import queryString from 'query-string';
import moment from 'moment';

import { clearMessages } from 'root/app/actions/messages';

import { ENTRY_DATE_FORMAT } from 'root/app/components/Entries/constants';
import { API_BASE_URL } from 'root/app/constants';
import { API_ENTRIES_MAX_LIMIT } from 'root/server/constants';

export function fetchMyEntries() {
  return (dispatch, getState) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'FETCH_MY_ENTRIES_FAILURE',
      });
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Could not load data - please, refresh the page and try again',
      });
    };

    const state = getState();
    const myId = state.auth.user && state.auth.user.id;
    const token = state.auth.token;

    const { perPage, page, filterStartDate, filterEndDate } = state.myEntriesPage;

    const queryParams = {
      utcOffset: moment().utcOffset(),
      limit: perPage,
      offset: perPage * (page - 1),
    };
    if (filterStartDate != null) {
      queryParams.startDate = filterStartDate;
    }
    if (filterEndDate != null) {
      queryParams.endDate = filterEndDate;
    }
    const query = queryString.stringify(queryParams);

    dispatch({
      type: 'FETCH_MY_ENTRIES_REQUEST',
    });

    return fetch(`${API_BASE_URL}/users/${myId}/entries/?${query}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'FETCH_MY_ENTRIES_SUCCESS',
            payload: json,
          });
        });
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}

export function fetchMyEntriesForPrint() {
  return (dispatch, getState) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'FETCH_MY_ENTRIES_FOR_PRINT_FAILURE',
      });
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Could not load data - please refresh the page and try again',
      });
    };

    const state = getState();
    const myId = state.auth.user && state.auth.user.id;
    const token = state.auth.token;

    const { filterStartDate, filterEndDate, total } = state.myEntriesPage;

    if (total > API_ENTRIES_MAX_LIMIT) {
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: 'Too many entries for selected dates to print. ' +
          'Please, choose different filter dates so that the number of entries ' +
          `is at most ${API_ENTRIES_MAX_LIMIT}.`,
      });
      return Promise.reject();
    }

    const queryParams = {
      utcOffset: moment().utcOffset(),
      limit: API_ENTRIES_MAX_LIMIT,
      offset: 0,
    };
    if (filterStartDate != null) {
      queryParams.startDate = filterStartDate;
    }
    if (filterEndDate != null) {
      queryParams.endDate = filterEndDate;
    }
    const query = queryString.stringify(queryParams);

    dispatch({
      type: 'FETCH_MY_ENTRIES_FOR_PRINT_REQUEST',
    });

    return fetch(`${API_BASE_URL}/users/${myId}/entries/?${query}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'FETCH_MY_ENTRIES_FOR_PRINT_SUCCESS',
            payload: json,
          });
          return json;
        });
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}

export function fetchAllEntriesForPrint() {
  return (dispatch, getState) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'FETCH_ALL_ENTRIES_FOR_PRINT_FAILURE',
      });
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Could not load data - please refresh the page and try again',
      });
    };

    const state = getState();
    const token = state.auth.token;

    const { filterStartDate, filterEndDate, total } = state.allEntriesPage;

    if (total > API_ENTRIES_MAX_LIMIT) {
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: 'Too many entries for selected dates to print. ' +
          'Please, choose different filter dates so that the number of entries ' +
          `is at most ${API_ENTRIES_MAX_LIMIT}.`,
      });
      return Promise.reject();
    }

    const queryParams = {
      utcOffset: moment().utcOffset(),
      limit: API_ENTRIES_MAX_LIMIT,
      offset: 0,
    };
    if (filterStartDate != null) {
      queryParams.startDate = filterStartDate;
    }
    if (filterEndDate != null) {
      queryParams.endDate = filterEndDate;
    }
    const query = queryString.stringify(queryParams);

    dispatch({
      type: 'FETCH_ALL_ENTRIES_FOR_PRINT_REQUEST',
    });

    return fetch(`${API_BASE_URL}/entries/?${query}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'FETCH_ALL_ENTRIES_FOR_PRINT_SUCCESS',
            payload: json,
          });
          return json;
        });
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}

export function myEntriesChangePage(page) {
  return (dispatch) => {
    dispatch({
      type: 'MY_ENTRIES_CHANGE_PAGE',
      payload: page,
    });
    dispatch(fetchMyEntries());
  };
}

export function fetchAllEntries() {
  return (dispatch, getState) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'FETCH_ALL_ENTRIES_FAILURE',
      });
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Could not load data - please, refresh the page and try again',
      });
    };

    const state = getState();
    const token = state.auth.token;
    const { perPage, page, filterStartDate, filterEndDate } = state.allEntriesPage;

    const queryParams = {
      utcOffset: moment().utcOffset(),
      limit: perPage,
      offset: perPage * (page - 1),
    };
    if (filterStartDate != null) {
      queryParams.startDate = filterStartDate;
    }
    if (filterEndDate != null) {
      queryParams.endDate = filterEndDate;
    }
    const query = queryString.stringify(queryParams);

    dispatch({
      type: 'FETCH_ALL_ENTRIES_REQUEST',
    });
    return fetch(`${API_BASE_URL}/entries?${query}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'FETCH_ALL_ENTRIES_SUCCESS',
            payload: json,
          });
        });
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}

export function allEntriesChangePage(page) {
  return (dispatch) => {
    dispatch({
      type: 'ALL_ENTRIES_CHANGE_PAGE',
      payload: page,
    });
    dispatch(fetchAllEntries());
  };
}

export function fetchEntryForEditPage(entryId) {
  return (dispatch, getState) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'FETCH_ENTRY_FOR_EDIT_PAGE_FAILURE',
      });
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Could not load data - please, refresh the page and try again',
      });
    };

    const state = getState();
    const token = state.auth.token;
    const queryParams = {
      utcOffset: moment().utcOffset(),
    };
    const query = queryString.stringify(queryParams);
    dispatch({
      type: 'FETCH_ENTRY_FOR_EDIT_PAGE_REQUEST',
    });
    return fetch(`${API_BASE_URL}/entries/${entryId}?${query}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'FETCH_ENTRY_FOR_EDIT_PAGE_SUCCESS',
            payload: json,
          });
        });
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}

export function saveEntry(data) {
  return (dispatch, getState) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Could not save data - please, try again',
      });
      dispatch({
        type: 'SAVE_ENTRY_FAILURE',
        payload: data,
      });
    };
    const state = getState();
    const token = state.auth.token;
    const body = JSON.stringify(Object.assign({},
      data, { utcOffset: moment().utcOffset() }));

    dispatch({
      type: 'SAVE_ENTRY_REQUEST',
    });
    return fetch(`${API_BASE_URL}/entries/${data.id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body,
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'SAVE_ENTRY_SUCCESS',
            payload: json,
          });
          return dispatch({
            type: 'SHOW_SUCCESS_MESSAGE',
            payload: 'Saved changes',
          });
        });
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}

export function deleteEntry(entryId) {
  return (dispatch, getState) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Could not delete entry - please, try again',
      });
      dispatch({
        type: 'DELETE_ENTRY_FAILURE',
        payload: entryId,
      });
    };
    const state = getState();
    const token = state.auth.token;

    dispatch({
      type: 'DELETE_ENTRY_REQUEST',
    });
    return fetch(`${API_BASE_URL}/entries/${entryId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).then((response) => {
      if (response.ok) {
        dispatch({
          type: 'DELETE_ENTRY_SUCCESS',
        });
        browserHistory.replace('/');
        dispatch({
          type: 'SHOW_INFO_MESSAGE',
          payload: 'An entry was deleted',
        });
        return;
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}

export function createEntryForUser(userId, data) {
  return (dispatch, getState) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'CREATE_ENTRY_FAILURE',
      });
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Could not save entry - please, try again later',
      });
    };
    const state = getState();
    const token = state.auth.token;
    const myId = state.auth.user.id;

    if (!data.trackedSeconds) {
      return dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: 'Tracked time cannot be empty',
      });
    }

    const body = JSON.stringify(Object.assign({},
      data, { utcOffset: moment().utcOffset() }));

    dispatch({
      type: 'CREATE_ENTRY_REQUEST',
    });
    return fetch(`${API_BASE_URL}/users/${userId}/entries/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body,
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'CREATE_ENTRY_SUCCESS',
            payload: json,
          });
          if (userId === myId) {
            browserHistory.push('/');
          } else {
            browserHistory.push(`/entries/${json.id}/`);
          }
          dispatch({
            type: 'SHOW_SUCCESS_MESSAGE',
            payload: `A new entry has been created for ${moment(data.date).format(`${ENTRY_DATE_FORMAT} LT`)}`,
          });
        });
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}

function transformDateFilterValue(momentValue, type) {
  if (momentValue) {
    if (type === 'start') {
      return momentValue.startOf('date').valueOf();
    }
    return momentValue.endOf('date').valueOf();
  }
  return null;
}

export function changeMyEntriesFilterStartDate(value) {
  return (dispatch, getState) => {
    const transformedValue = transformDateFilterValue(value, 'start');
    const state = getState();
    const { filterEndDate } = state.myEntriesPage;
    dispatch(clearMessages());
    if (filterEndDate != null && transformedValue != null &&
      transformedValue > filterEndDate) {
      dispatch({
        type: 'MY_ENTRIES_CHANGE_FILTER_END_DATE',
        payload: transformedValue,
      });
    }
    dispatch({
      type: 'MY_ENTRIES_CHANGE_FILTER_START_DATE',
      payload: transformedValue,
    });
    dispatch({
      type: 'MY_ENTRIES_CHANGE_PAGE',
      payload: 1,
    });
    dispatch(fetchMyEntries());
  };
}

export function changeMyEntriesFilterEndDate(value) {
  return (dispatch, getState) => {
    const transformedValue = transformDateFilterValue(value, 'end');
    const state = getState();
    const { filterStartDate } = state.myEntriesPage;
    dispatch(clearMessages());
    if (filterStartDate != null && transformedValue != null &&
      transformedValue < filterStartDate) {
      dispatch({
        type: 'MY_ENTRIES_CHANGE_FILTER_START_DATE',
        payload: transformedValue,
      });
    }
    dispatch({
      type: 'MY_ENTRIES_CHANGE_FILTER_END_DATE',
      payload: transformedValue,
    });
    dispatch({
      type: 'MY_ENTRIES_CHANGE_PAGE',
      payload: 1,
    });
    dispatch(fetchMyEntries());
  };
}

export function changeAllEntriesFilterStartDate(value) {
  return (dispatch, getState) => {
    const transformedValue = transformDateFilterValue(value, 'start');
    const state = getState();
    const { filterEndDate } = state.allEntriesPage;
    dispatch(clearMessages());
    if (filterEndDate != null && transformedValue != null &&
      transformedValue > filterEndDate) {
      dispatch({
        type: 'ALL_ENTRIES_CHANGE_FILTER_END_DATE',
        payload: transformedValue,
      });
    }
    dispatch({
      type: 'ALL_ENTRIES_CHANGE_FILTER_START_DATE',
      payload: transformedValue,
    });
    dispatch({
      type: 'ALL_ENTRIES_CHANGE_PAGE',
      payload: 1,
    });
    dispatch(fetchAllEntries());
  };
}

export function changeAllEntriesFilterEndDate(value) {
  return (dispatch, getState) => {
    const transformedValue = transformDateFilterValue(value, 'end');
    const state = getState();
    const { filterStartDate } = state.allEntriesPage;
    dispatch(clearMessages());
    if (filterStartDate != null && transformedValue != null &&
      transformedValue < filterStartDate) {
      dispatch({
        type: 'ALL_ENTRIES_CHANGE_FILTER_START_DATE',
        payload: transformedValue,
      });
    }
    dispatch({
      type: 'ALL_ENTRIES_CHANGE_FILTER_END_DATE',
      payload: transformedValue,
    });
    dispatch({
      type: 'ALL_ENTRIES_CHANGE_PAGE',
      payload: 1,
    });
    dispatch(fetchAllEntries());
  };
}
