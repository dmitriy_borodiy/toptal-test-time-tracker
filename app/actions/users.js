import { browserHistory } from 'react-router';

import { API_BASE_URL } from 'root/app/constants';
import { logout } from './auth';

export function fetchUsers({ limit, offset }) {
  return (dispatch, getState) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'FETCH_USERS_FAILURE',
      });
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Could not load data - please, refresh the page and try again',
      });
    };

    const state = getState();
    const token = state.auth.token;
    const queryParams = `limit=${limit}&offset=${offset}`;
    dispatch({
      type: 'FETCH_USERS_REQUEST',
    });
    return fetch(`${API_BASE_URL}/users/?${queryParams}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'FETCH_USERS_SUCCESS',
            payload: json,
          });
        });
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}

export function usersChangePage(page) {
  return (dispatch, getState) => {
    const state = getState();
    const perPage = state.allUsersPage.perPage;
    return dispatch(fetchUsers({
      limit: perPage,
      offset: (page - 1) * perPage,
    }))
    .then(() => {
      dispatch({
        type: 'USERS_CHANGE_PAGE',
        payload: page,
      });
    });
  };
}

export function fetchUserForEditPage(userId) {
  return (dispatch, getState) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'FETCH_USER_FOR_EDIT_PAGE_FAILURE',
      });
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Could not load data - please, refresh the page and try again',
      });
    };

    const state = getState();
    const token = state.auth.token;
    dispatch({
      type: 'FETCH_USER_FOR_EDIT_PAGE_REQUEST',
    });
    return fetch(`${API_BASE_URL}/users/${userId}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'FETCH_USER_FOR_EDIT_PAGE_SUCCESS',
            payload: json,
          });
        });
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}

export function saveUser(data) {
  return (dispatch, getState) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Could not save data - please, try again',
      });
      dispatch({
        type: 'SAVE_USER_FAILURE',
      });
    };

    const state = getState();
    const token = state.auth.token;

    dispatch({
      type: 'SAVE_USER_REQUEST',
    });
    return fetch(`${API_BASE_URL}/users/${data.id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    })
    .then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'SAVE_USER_SUCCESS',
            payload: json.user,
          });
          dispatch({
            type: 'SHOW_SUCCESS_MESSAGE',
            payload: 'Saved changes',
          });

          if (data.id === state.auth.user.id) {
            dispatch({
              type: 'UPDATE_PROFILE_SUCCESS',
              payload: json.user,
            });
          }
        });
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}

export function deleteUser(userId) {
  return (dispatch, getState) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Could not delete user - please, try again later',
      });
    };
    const state = getState();
    const token = state.auth.token;
    return fetch(`${API_BASE_URL}/users/${userId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).then((response) => {
      if (response.ok) {
        dispatch({
          type: 'DELETE_USER_SUCCESS',
        });
        if (userId === state.auth.user.id) {
          return dispatch(logout());
        }
        browserHistory.replace('/users/');
        dispatch({
          type: 'SHOW_INFO_MESSAGE',
          payload: 'A user was deleted',
        });
        return;
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}
