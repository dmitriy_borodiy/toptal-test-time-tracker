import moment from 'moment';
import cookie from 'react-cookie';
import { browserHistory } from 'react-router';

import { API_BASE_URL } from '../constants';

export function login(email, password) {
  return (dispatch) => {
    dispatch({
      type: 'CLEAR_MESSAGES',
    });
    dispatch({
      type: 'LOGIN_REQUEST',
    });
    return fetch(`${API_BASE_URL}/login`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email,
        password,
      }),
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'LOGIN_SUCCESS',
            payload: {
              token: json.token,
              user: json.user,
            },
          });
          cookie.save('token', json.token, { expires: moment().add(1, 'hour').toDate() });
          browserHistory.push('/');
        });
      }
      return response.json().then((json) => {
        dispatch({
          type: 'LOGIN_FAILURE',
        });
        dispatch({
          type: 'SHOW_ERROR_MESSAGE',
          payload: json || 'Error while logging in. Please, try again later.',
        });
      });
    });
  };
}

export function signup({ name, email, password, captcha }) {
  return (dispatch) => {
    dispatch({
      type: 'CLEAR_MESSAGES',
    });
    dispatch({
      type: 'SIGNUP_REQUEST',
    });
    return fetch(`${API_BASE_URL}/signup`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ name, email, password, captcha }),
    }).then(response => response.json().then((json) => {
      if (response.ok) {
        dispatch({
          type: 'SIGNUP_SUCCESS',
          payload: {
            token: json.token,
            user: json.user,
          },
        });
        browserHistory.push('/');
        cookie.save('token', json.token, { expires: moment().add(1, 'hour').toDate() });
      } else {
        dispatch({
          type: 'SIGNUP_FAILURE',
        });
        dispatch({
          type: 'SHOW_ERROR_MESSAGE',
          payload: json || 'Error while signing up. Please, try again later.',
        });
      }
    }));
  };
}

export function logout() {
  return (dispatch) => {
    dispatch({
      type: 'LOGOUT_SUCCESS',
    });
    cookie.remove('token');
    browserHistory.push('/login');
    dispatch({
      type: 'SHOW_INFO_MESSAGE',
      payload: 'You\'ve been logged out.',
    });
  };
}

export function forgotPassword({ email, captcha }) {
  return (dispatch) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'FORGOT_PASSWORD_FAILURE',
      });
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Could not send a recovery email. Please double-check the email address.',
      });
    };

    dispatch({
      type: 'CLEAR_MESSAGES',
    });
    dispatch({
      type: 'FORGOT_PASSWORD_REQUEST',
    });
    return fetch(`${API_BASE_URL}/forgot`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ email, captcha }),
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'FORGOT_PASSWORD_SUCCESS',
          });
          dispatch({
            type: 'SHOW_SUCCESS_MESSAGE',
            payload: json || 'An email with further instructions has been sent.',
          });
        });
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}

export function resetPassword(password, confirm, pathToken) {
  return (dispatch) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'RESET_PASSWORD_FAILURE',
      });
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Error while resetting password, maybe the reset link has expired',
      });
    };

    dispatch({
      type: 'CLEAR_MESSAGES',
    });
    dispatch({
      type: 'RESET_PASSWORD_REQUEST',
    });
    return fetch(`${API_BASE_URL}/reset/${pathToken}`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        password,
        confirm,
      }),
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          browserHistory.push('/login');
          dispatch({
            type: 'RESET_PASSWORD_SUCCESS',
          });
          dispatch({
            type: 'SHOW_INFO_MESSAGE',
            payload: json || 'Password has been reset',
          });
        });
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}

export function updateProfile(state, token) {
  return (dispatch) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'UPDATE_PROFILE_FAILURE',
      });
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Could not save changes',
      });
    };

    dispatch({
      type: 'CLEAR_MESSAGES',
    });
    dispatch({
      type: 'UPDATE_PROFILE_REQUEST',
    });
    return fetch(`${API_BASE_URL}/account`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        email: state.email,
        name: state.name,
        gender: state.gender,
        preferredHoursPerDay: state.preferredHoursPerDay,
        location: state.location,
        website: state.website,
      }),
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'UPDATE_PROFILE_SUCCESS',
            payload: json.user,
          });
          dispatch({
            type: 'SHOW_SUCCESS_MESSAGE',
            payload: json || 'Changes saved',
          });
        });
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}

export function changePassword(password, confirm, token) {
  return (dispatch) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'CHANGE_PASSWORD_FAILURE',
      });
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Could not change password.',
      });
    };

    dispatch({
      type: 'CLEAR_MESSAGES',
    });
    dispatch({
      type: 'CHANGE_PASSWORD_REQUEST',
    });
    return fetch(`${API_BASE_URL}/account`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        password,
        confirm,
      }),
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'CHANGE_PASSWORD_SUCCESS',
          });
          dispatch({
            type: 'SHOW_SUCCESS_MESSAGE',
            payload: json || 'Password has been changed.',
          });
        });
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}

export function deleteAccount(token) {
  return (dispatch) => {
    const errorHandler = (msg) => {
      dispatch({
        type: 'SHOW_ERROR_MESSAGE',
        payload: msg || 'Error occured while deleting account',
      });
      dispatch({
        type: 'DELETE_ACCOUNT_FAILURE',
      });
    };

    dispatch({
      type: 'CLEAR_MESSAGES',
    });
    dispatch({
      type: 'DELETE_ACCOUNT_REQUEST',
    });
    return fetch(`${API_BASE_URL}/account`, {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch(logout());
          dispatch({
            type: 'DELETE_ACCOUNT_SUCCESS',
          });
          dispatch({
            type: 'SHOW_INFO_MESSAGE',
            payload: json || 'Your account was deleted',
          });
        });
      }
      return response.json().then(errorHandler);
    }).catch(() => errorHandler());
  };
}
