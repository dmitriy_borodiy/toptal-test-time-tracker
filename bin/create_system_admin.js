// ES6 Transpiler
require('babel-core/register');
require('babel-polyfill');

const USER_ROLES = require('root/server/models/constants').USER_ROLES;
const apiTestUtils = require('root/test/server/api/utils');

const systemAdminEmail = process.env.SYSTEM_ADMIN_EMAIL;
const systemAdminPassword = process.env.SYSTEM_ADMIN_PASSWORD;

if (!systemAdminEmail || !systemAdminPassword) {
  console.error('Can not create a system admin account: SYSTEM_ADMIN_EMAIL ' +
    'or SYSTEM_ADMIN_PASSWORD env vars are not specified.')
  process.exit(1);
}

apiTestUtils.removeUsers({
  isSystemAdmin: true
})
.then(() =>
  apiTestUtils.createUser({
    name: systemAdminEmail,
    email: systemAdminEmail,
    password: systemAdminPassword,
    role: USER_ROLES.ADMIN,
    isSystemAdmin: true,
  }))
.then(() => {
  console.log('(Re-)created system admin user.');
  console.log(`login: ${systemAdminEmail}`);
  console.log(`password: ${systemAdminPassword}`);
  process.exit(0);
})
.catch(function createSystemAdminUserErrorHandler(err) {
  console.error('Error while creating system admin: ', err);
  throw err;
})
