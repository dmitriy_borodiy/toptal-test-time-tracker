/* eslint-disable no-console */

// ES6 Transpiler
require('babel-core/register');
require('babel-polyfill');

const moment = require('moment');

const USER_ROLES = require('root/server/models/constants').USER_ROLES;
const apiTestUtils = require('root/test/server/api/utils');

const N_TEST_USERS = 50;
const DATES_PER_USER = 2 * 365;
let userId;
let managerId;
let adminId;
let testUsersIds = [];

let userToken;
let managerToken;
let adminToken;

apiTestUtils.removeAllUsers()
  .then(() => apiTestUtils.removeAllEntries())
  .then(() => {
    const userPromises = [];
    for (let i = 0; i < N_TEST_USERS; ++i) {
      userPromises.push(
        apiTestUtils.createUserAndLogin({
          name: `test-user-${i}`,
          email: `test-user-${i}@test.com`,
          password: 'qwerty',
          role: USER_ROLES.USER,
        }));
    }
    return Promise.all(userPromises);
  })
  .then((userPromiseResults) => {
    for (let i = 0; i < N_TEST_USERS; ++i) {
      const { user } = userPromiseResults[i];
      testUsersIds.push(user.id);
    }
  })
  .then(() => apiTestUtils.createUserAndLogin({
    name: 'admin',
    email: 'admin@test.com',
    password: 'qwerty',
    role: USER_ROLES.ADMIN,
  }))
  .then(({ token: authToken, user }) => {
    adminToken = authToken;
    adminId = user.id;
  })
  .then(() => apiTestUtils.createUserAndLogin({
    name: 'manager',
    email: 'manager@test.com',
    password: 'qwerty',
    role: USER_ROLES.MANAGER,
  }))
  .then(({ token: authToken, user }) => {
    managerToken = authToken;
    managerId = user.id;
  })
  .then(() => apiTestUtils.createUserAndLogin({
    name: 'user',
    email: 'user@test.com',
    password: 'qwerty',
    role: USER_ROLES.USER,
  }))
  .then(({ token: authToken, user }) => {
    userToken = authToken;
    userId = user.id;
  })
  .then(() => {
    const userIds = testUsersIds.concat([userId, managerId, adminId]);
    let currentUserIndex = 0;
    function createEntriesForUserLoop() {
      console.log(`Creating entries for user ${currentUserIndex + 1}/${userIds.length}...`);

      if (currentUserIndex === userIds.length) {
        return Promise.resolve();
      }

      const uid = userIds[currentUserIndex];
      const entriesData = [];
      const startDate = moment().subtract(2, 'days').startOf('date');

      for (let i = 0; i < DATES_PER_USER; i++) {
        let date = startDate.clone().subtract(i, 'days').add(10, 'hours');

        const entriesPerDate = 1 + Math.round(Math.random() * 3);
        for (let j = 0; j < entriesPerDate; ++j) {
          const trackedSeconds = 3 * 3600 + Math.round(Math.random() * 3) * 3600 +
            Math.round(Math.random() * 3) * 3600 / 4;
          date = date.add(trackedSeconds, 'seconds').add(30, 'minutes');

          const notes = [];
          if (Math.random() < 0.5) {
            const numNotes = Math.floor(1 + (2 * Math.random()));
            for (let noteIndex = 0; noteIndex < numNotes; noteIndex++) {
              notes.push('Lorem ipsum '.repeat(1 + (3 * Math.round(Math.random()))));
            }
          }
          entriesData.push({
            date: date.valueOf(),
            user: uid,
            trackedSeconds,
            notes,
          });
        }
      }

      currentUserIndex++;
      return apiTestUtils.createEntries(entriesData)
        .then(createEntriesForUserLoop);
    }
    return createEntriesForUserLoop();
  })
  .then(() => {
    console.log('Done!');
    console.log('User token: ', userToken);
    console.log('Manager token: ', managerToken);
    console.log('Admin token: ', adminToken);
    process.exit(0);
  });
