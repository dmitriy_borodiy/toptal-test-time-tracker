const jwt = require('jsonwebtoken');
const nunjucks = require('nunjucks');

const React = require('react');
const ReactDOM = require('react-dom/server');
const Router = require('react-router');
const Provider = require('react-redux').Provider;

const utils = require('root/server/utils');

// React and Server-Side Rendering
const routes = require('root/app/routes');
const configureStore = require('root/app/store/configureStore').default;

// Models
const User = require('root/server/models/User');

exports.checkReCaptchaMiddleware = (errorMessage) =>
  function herokuRedirectHttpsMiddleware(req, res, next) {
    if (process.env.NODE_ENV === 'test') {
      return next();
    }

    req.assert('captcha',
      errorMessage || 'You must pass the captcha').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
      return res.status(401).send(errors);
    }
    utils.validateReCaptcha(req.body.captcha)
      .then(
        () => next(),
        () => res.status(401).send({
          msg: 'Captcha is invalid. Please, refresh the page and try again.' }));
  };

exports.herokuRedirectHttps =
  function herokuRedirectHttpsMiddleware(req, res, next) {
    if (req.headers['x-forwarded-proto'] !== 'https') {
      return res.redirect(['https://', req.get('Host'), req.url].join(''));
    }
    return next();
 };

const PAGINATION_DEFAULT_LIMIT_MAX = 100;

exports.paginationParamsMiddleware = (params = {}) =>
  function paginationParamsMiddleware(req, res, next) {
    const defaultParams = {
      limit: 10,
      offset: 0,
    };

    const maxLimit = params.maxLimit || PAGINATION_DEFAULT_LIMIT_MAX;

    req.checkQuery('limit')
      .optional()
      .isInt({ min: 0 });
    req.checkQuery('offset')
      .optional()
      .isInt({ min: 0 });
    req.sanitize('limit').toInt();
    req.sanitize('offset').toInt();
    const errors = req.validationErrors();
    if (errors) {
      return res.status(400).send(errors);
    }

    const offset = req.query.offset != null ?
      req.query.offset : defaultParams.offset;
    const limit = req.query.limit != null ?
      Math.min(req.query.limit, maxLimit) : defaultParams.limit;

    req.paginationParams = {
      offset,
      limit,
    };

    next();
  };

exports.filterParamsMiddleware = function filterParamsMiddleware(req, res, next) {
  req.checkQuery('startDate')
    .optional()
    .isInt({ min: 0 });
  req.checkQuery('endDate')
    .optional()
    .isInt({ min: 0 });
  req.sanitize('startDate').toInt();
  req.sanitize('endDate').toInt();

  const errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }

  const {
    startDate,
    endDate,
  } = req.query;
  if (startDate != null && endDate != null && startDate > endDate) {
    return res.status(400).send({ msg: 'startDate must be less than or equal to endDate' });
  }

  req.filterParams = {
    startDate,
    endDate,
  };
  next();
}

exports.authMiddleware = ({ cookies = false, headers = true }) =>
  function authMiddleware(req, res, next) {
    req.isAuthenticated = function reqIsAuthenticated() {
      return !!req.user;
    };

    const authHeaders = req.headers.authorization;
    let token;
    if (!token && headers) {
      token = authHeaders && authHeaders.split(' ')[1];
    }
    if (!token && cookies) {
      token = req.cookies.token;
    }

    if (!token) {
      return next();
    }
    let jwtPayload;
    try {
      jwtPayload = jwt.verify(token, process.env.TOKEN_SECRET);
    } catch (err) {
      return res.status(400).send({ msg: 'Invalid token' });
    }
    User.findById(jwtPayload.sub, (err, user) => {
      req.user = user;
      req.token = token;
      next();
    });
  };

exports.ssrMiddleware = function ssrMiddleware(req, res) {
  const user = req.user && req.user.toJSON({ virtuals: true });
  const initialState = {
    auth: {
      token: user && req.token,
      user: user,
    },
    messages: {},
  };

  const store = configureStore(initialState);

  Router.match({
    routes: routes.default(store),
    location: req.url,
  }, (err, redirectLocation, renderProps) => {
    if (err) {
      res.status(500).send(err.message);
    } else if (redirectLocation) {
      res.status(302).redirect(redirectLocation.pathname + redirectLocation.search);
    } else if (renderProps) {
      const html = ReactDOM.renderToString(
        React.createElement(Provider, { store },
          React.createElement(Router.RouterContext, renderProps)));

      let jsBundleName = 'bundle.js';
      if (process.env.NODE_ENV === 'production') {
        const   manifest = require('root/public/js/manifest.json');
        if (manifest) {
          jsBundleName = manifest['main.js'];
        }
      }

      const page = nunjucks.render('layout.html', {
        html,
        initialState: JSON.stringify(store.getState()),
        jsBundleName,
      });
      res.status(200).send(page);
    } else {
      res.send(404);
    }
  });
};
