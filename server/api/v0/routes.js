const express = require('express');

const RateLimit = require('express-rate-limit');

const ratelimitConfig = require('root/server/ratelimit-config');
const middleware = require('root/server/middleware');
const userController = require('root/server/controllers/user');
const entryController = require('root/server/controllers/entries');

const constants = require('root/server/constants');

function createApiRouter() {
  const router = express.Router();

  router.patch('/account', userController.ensureAuthenticated, userController.updateAccount);
  router.delete('/account', userController.ensureAuthenticated, userController.deleteAccount);
  router.post('/signup',
    new RateLimit(Object.assign({}, ratelimitConfig.SIGNUP_RATELIMIT_CONFIG)),
    middleware.checkReCaptchaMiddleware('You must pass the captcha to signup'),
    userController.signupPost
  );
  router.post('/login',
    new RateLimit(Object.assign({}, ratelimitConfig.LOGIN_RATELIMIT_CONFIG)),
    userController.loginPost
  );
  router.post('/forgot',
    new RateLimit(Object.assign({}, ratelimitConfig.FORGOT_PASSWORD_RATELIMIT_CONFIG)),
    middleware.checkReCaptchaMiddleware('You must pass the captcha to reset password'),
    userController.forgotPost
  );
  router.post('/reset/:token',
    new RateLimit(ratelimitConfig.LOGIN_RATELIMIT_CONFIG),
    userController.resetPost
  );

  router.get('/users',
    userController.ensureRole(['manager', 'admin']),
    middleware.paginationParamsMiddleware(),
    userController.getUsers,
  );
  router.post('/users',
    userController.ensureRole(['manager', 'admin']),
    userController.createUser,
  );
  router.get('/users/:userId',
    userController.ensureRole(['manager', 'admin']),
    userController.getUser,
  );
  router.patch('/users/:id',
    userController.ensureRole(['manager', 'admin']),
    userController.updateUser,
  );
  router.delete('/users/:id',
    userController.ensureRole(['manager', 'admin']),
    userController.deleteUser,
  );

  router.get('/entries/:entryId',
    userController.ensureAuthenticated,
    entryController.getEntry,
  );
  router.get('/users/:userId/entries',
    userController.ensureAuthenticated,
    middleware.paginationParamsMiddleware({ maxLimit: constants.API_ENTRIES_MAX_LIMIT }),
    middleware.filterParamsMiddleware,
    entryController.getEntriesForUser,
  );
  router.post('/users/:userId/entries',
    userController.ensureAuthenticated,
    entryController.createEntryForUser,
  );

  router.get('/entries',
    userController.ensureRole(['admin']),
    middleware.paginationParamsMiddleware({ maxLimit: constants.API_ENTRIES_MAX_LIMIT }),
    middleware.filterParamsMiddleware,
    entryController.getEntries,
  );
  router.patch('/entries/:entryId',
    userController.ensureAuthenticated,
    entryController.updateEntry,
  );
  router.delete('/entries/:entryId',
    userController.ensureAuthenticated,
    entryController.deleteEntry,
  );

  router.use('*', (req, res) => res.send(404));

  return router;
}

module.exports = createApiRouter;
