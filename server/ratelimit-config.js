const isTest = process.env.NODE_ENV === 'test';

/*
 * To use in API tests for avoiding rate limiting by default.
 * In test environment, rate limiting is "opt-in".
 */
function shouldSkipRateLimiting(req, res) {
  if (isTest) {
    const enableRatelimit = req.headers['x-enable-ratelimit-for-tests'] != null;
    if (enableRatelimit) {
      return false;
    }
    return true;
  } else {
    return false;
  }
};

exports.SSR_RATELIMIT_CONFIG = {
  headers: true,
  skip: shouldSkipRateLimiting,
  windowMs: 10 * 60 * 1000, // 10 minutes
  max: isTest ? 20 : 10 * 30, // average = 0.5 hits / second
  delayAfter: 100, // start delaying responses after 100 accesses
  delayMs: 500,
  message: 'Too many requests from this IP, please try again after 10 minutes',
};

exports.API_RATELIMIT_CONFIG = {
  headers: true,
  skip: shouldSkipRateLimiting,
  windowMs: 5 * 60 * 1000, // 5 minutes
  max: isTest ? 20 : 5 * 60, // average = 1 hit / second
  delayAfter: 4 * 60,
  delayMs: 50,
  message: JSON.stringify({
    msg: 'Too many requests from this IP, please try again after 5 minutes',
  }),
};

exports.LOGIN_RATELIMIT_CONFIG = {
  headers: true,
  skip: shouldSkipRateLimiting,
  windowMs: 5 * 60 * 1000, // 5 minutes
  max: 10,
  delayAfter: 0,
  message: JSON.stringify({
    msg: 'Too many login attempts from this IP, please try again in a few minutes',
  }),
};

exports.SIGNUP_RATELIMIT_CONFIG = {
  headers: true,
  skip: shouldSkipRateLimiting,
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 5,
  delayAfter: 0,
  message: JSON.stringify({
    msg: 'Too many accounts created from this IP, please try again after 15 minutes',
  }),
};

exports.FORGOT_PASSWORD_RATELIMIT_CONFIG = {
  headers: true,
  skip: shouldSkipRateLimiting,
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 5,
  delayAfter: 0,
  message: JSON.stringify({
    msg: 'Too many attempts from this IP, please try again after 15 minutes',
  }),
};
