// ES6 Transpiler
require('babel-core/register');
require('babel-polyfill');

const express = require('express');
const serveStatic = require('serve-static');
const RateLimit = require('express-rate-limit');
const errorhandler = require('errorhandler');
const path = require('path');
const morgan = require('morgan');
const compression = require('compression');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const dotenv = require('dotenv');
const nunjucks = require('nunjucks');
const mongoose = require('mongoose');
const sass = require('node-sass-middleware');
const webpack = require('webpack');
const helmet = require('helmet');
var methodOverride = require('method-override');

const customExpressValidators = require('root/server/controllers/validators');
const middleware = require('root/server/middleware');
const logger = require('root/server/logger');
const ratelimitConfig = require('root/server/ratelimit-config');
const createApiRouter = require('root/server/api/v0/routes');

// Load environment variables from .env file
dotenv.load();

mongoose.connect(process.env.MONGODB || process.env.MONGODB_URI);
mongoose.connection.on('error', (error) => {
  /* eslint-disable no-console */
  console.log('MongoDB Connection Error. Please make sure that MongoDB is running.');
  process.exit(1);
});

function createAppInstance() {
  const app = express();
  app.enable('trust proxy');
  app.set('port', process.env.PORT || 3000);

  // view engine setup
  nunjucks.configure('views', {
    autoescape: true,
    express: app,
  });
  app.set('view engine', 'html');

  // Override HTTP methods with X-HTTP-Method-Override header
  app.use(methodOverride('X-HTTP-Method-Override'));

  // Logging
  if (app.get('env') !== 'test') {
    app.use(morgan('combined', { stream: logger.stream }));
  }

  // Webpack hot reloading
  if (app.get('env') === 'development') {
    const webpackConfig = require('root/webpack.config');
    const compiler = webpack(webpackConfig);
    app.use(require('webpack-dev-middleware')(compiler, {
      noInfo: true,
      publicPath: webpackConfig.output.publicPath,
    }));
    app.use(require('webpack-hot-middleware')(compiler));
  }

  if (app.get('env') === 'production') {
    app.use(helmet());
    app.use(middleware.herokuRedirectHttps);
  }
  app.use(compression());
  app.use(sass({
    src: path.join(__dirname, '../public'),
    dest: path.join(__dirname, '../public'),
  }));
  app.use(serveStatic(path.join(__dirname, '../public'), {
    index: false,
    maxAge: '1d',
    setHeaders: function setCustomCacheControl(res, path) {
      if (serveStatic.mime.lookup(path) === 'text/css') {
        res.setHeader('Cache-Control', 'public, max-age=0');
      }
    },
  }));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cookieParser());

  // API
  const apiRoutes = createApiRouter();

  app.use(expressValidator({
    customValidators: customExpressValidators,
  }));
  const apiAuthMiddleware = middleware.authMiddleware({ cookies: false, headers: true });
  const apiRateLimiter = new RateLimit(
    Object.assign({}, ratelimitConfig.API_RATELIMIT_CONFIG));
  app.use('/api/v0', apiAuthMiddleware, apiRateLimiter, apiRoutes);

  // React server rendering
  const ssrRateLimiter = new RateLimit(
    Object.assign({}, ratelimitConfig.SSR_RATELIMIT_CONFIG));
  const ssrAuthMiddleware = middleware.authMiddleware({ cookies: true, headers: true });
  app.use(ssrAuthMiddleware, ssrRateLimiter, middleware.ssrMiddleware);

  // Error handler
  /* eslint-disable no-unused-vars */
  app.use((err, req, res, next) => {
    logger.info('Error: ', err);
    res.status(err.status || 500).send({
      msg: err.msg || 'Server error',
    });
  });

  if (process.env.NODE_ENV !== 'production') {
    app.use(errorhandler());
  }

  return app;
}

module.exports = createAppInstance;
