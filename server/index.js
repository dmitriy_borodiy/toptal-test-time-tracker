const createAppInstance = require('root/server/server');
const app = createAppInstance();

if (process.env.NODE_ENV !== 'test') {
  app.listen(app.get('port'), () => {
    console.log(`Express server listening on port ${app.get('port')}`);
  });
}

module.exports = app;
