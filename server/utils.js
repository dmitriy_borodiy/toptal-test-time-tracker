const request = require('superagent');

exports.validateReCaptcha = function validateReCaptcha({ captcha, remoteUserIp }) {
  return new Promise((fulfill, reject) => {
    request.post('https://www.google.com/recaptcha/api/siteverify')
      .send({
        response: captcha,
        remoteIp: remoteUserIp,
        secret: process.env.RECAPTCHA_SECRET,
      })
      .end((err, res) => {
        if (err) {
          return reject(err);
        }
        if (!res.ok) {
          return reject(res);
        }
        fulfill(res);
      });
  });
};
