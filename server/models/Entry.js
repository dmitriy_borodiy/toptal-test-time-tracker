const _ = require('lodash');
const mongoose = require('mongoose');
const moment = require('moment');
const mongoosePaginate = require('mongoose-paginate');

const logger = require('root/server/logger');
const utils = require('root/server/utils');

const schemaOptions = {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform(doc, ret) {
      ret.date = doc.date != null && doc.date.getTime();
    },
  },
};

const entrySchema = new mongoose.Schema({
  date: {
    type: Date,
    default: Date.now,
    required: true,
  },
  trackedSeconds: {
    type: Number,
    required: true,
  },
  notes: {
    type: [String],
  },
  user: {
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
}, schemaOptions);
entrySchema.plugin(mongoosePaginate);

entrySchema.pre('save', function entryPreSave(next) {
  this.date = new Date(this.date);
  next();
});

const Entry = mongoose.model('Entry', entrySchema);

Entry.getTotalTimeForDate = function entryGetTotalTimeForDate({ userId, date }) {
  return Entry.aggregate([
    { $match: { date: date, user: userId } },
    { $group: { _id: null, totalTrackedTime: { $sum: "$trackedSeconds" } } },
  ])
  .then((aggregateResult) => {
    if (aggregateResult.length === 0) {
      return 0;
    }
    return aggregateResult[0].totalTrackedTime;
  })
};

Entry.populateTotalTrackedTime =
  function entryPopulateTotalTrackedTime({ entries, utcOffset }) {
  if (entries.length === 0) {
    return Promise.resolve([]);
  }

  const minTimestamp = _.min(_.map(entries, 'date'));
  const maxTimestamp = _.max(_.map(entries, 'date'));

  const minDateMoment = moment.utc(minTimestamp)
    .add(utcOffset, 'minutes')
    .startOf('date')
    .subtract(utcOffset, 'minutes');
  const maxDateMoment = moment.utc(maxTimestamp)
    .add(utcOffset, 'minutes')
    .endOf('date')
    .subtract(utcOffset, 'minutes');

  const utcOffsetMs = utcOffset * 1000 * 60;

  return Entry.aggregate([
    {
      $match: {
        date: { $lte: maxDateMoment.toDate(), $gte: minDateMoment.toDate() },
      },
    },
    {
      $group: {
        _id: {
          dateParts: {
            date: { "$dayOfMonth": { "$add": ["$date", utcOffsetMs] } },
            month: { "$month": { "$add": ["$date", utcOffsetMs] } },
            year: { "$year": { "$add": ["$date", utcOffsetMs] } },
          },
          user: "$user",
        },
        totalTrackedSeconds: {
          $sum: "$trackedSeconds",
        },
      },
    },
  ])
  .then((aggregationResults) => {

    aggregationResults.forEach((result) => {
      const { date, month, year } = result._id.dateParts;
      const dateMoment = moment.utc([year, month - 1, date]);
      result._id.date = dateMoment.valueOf();
    })

    const aggregationResultsTransformed = _.transform(
      _.groupBy(aggregationResults, '_id.date'),
      (result, value, key) => {
        result[key] = _.groupBy(value, '_id.user');
      }, {});

    const result = [];
    for (const entry of entries) {
      let totalTrackedSeconds;
      if (entry.date != null && entry.user != null && entry.user._id != null) {
        const entryDate = moment.utc(entry.date).add(utcOffset, 'minutes').startOf('date');
        const resultsForUsers = aggregationResultsTransformed[entryDate.valueOf().toString()];

        if (resultsForUsers) {
          const resultsForUserAndDate = resultsForUsers[entry.user._id.toString()];
          if (resultsForUserAndDate && resultsForUserAndDate.length > 0) {
            const resultForEntry = resultsForUserAndDate[0];
            totalTrackedSeconds = resultForEntry.totalTrackedSeconds;
          }
        }
      }
      result.push(Object.assign({}, entry.toJSON(), { totalTrackedSeconds }));
    }
    return Promise.resolve(result);
  })
  .catch((err) => {
    logger.error('Error while calculating total tracked time for entries', err);
    return err;
  });
};

module.exports = Entry;
