export const USER_ROLES = {
  USER: 'user',
  MANAGER: 'manager',
  ADMIN: 'admin',
};

export const DEFAULT_USER_ROLE = USER_ROLES.USER;
export const DEFAULT_PREFERRED_HOURS_PER_DAY = 8;
