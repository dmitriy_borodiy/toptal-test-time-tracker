const crypto = require('crypto');
const bcrypt = require('bcrypt-nodejs');
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const constants = require('./constants');

const {
  USER_ROLES,
  DEFAULT_USER_ROLE,
  DEFAULT_PREFERRED_HOURS_PER_DAY,
} = constants;

const schemaOptions = {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform(doc, ret) {
      delete ret.password;
      delete ret.passwordResetToken;
      delete ret.passwordResetExpires;
      delete ret.isSystemAdmin;
    },
  },
};

const userSchema = new mongoose.Schema({
  name: String,
  email: { type: String, unique: true },
  password: String,
  passwordResetToken: String,
  passwordResetExpires: Date,
  preferredHoursPerDay: {
    type: Number,
    required: true,
    default: DEFAULT_PREFERRED_HOURS_PER_DAY,
  },
  location: String,
  website: String,
  picture: String,
  role: {
    type: String,
    default: DEFAULT_USER_ROLE,
    enum: [
      USER_ROLES.USER,
      USER_ROLES.MANAGER,
      USER_ROLES.ADMIN,
    ],
  },
  isSystemAdmin: {
    type: Boolean,
    required: true,
    default: false,
  },
}, schemaOptions);
userSchema.plugin(mongoosePaginate);

userSchema.pre('save', function userPreSave(next) {
  const user = this;
  if (!user.isModified('password')) { return next(); }
  bcrypt.genSalt(10, (saltErr, salt) => {
    bcrypt.hash(user.password, salt, null, (hashErr, hash) => {
      user.password = hash;
      next();
    });
  });
});

userSchema.methods.comparePassword = function userComparePassword(password, cb) {
  bcrypt.compare(password, this.password, (err, isMatch) => {
    cb(err, isMatch);
  });
};

userSchema.virtual('gravatar').get(function userGetGravatar() {
  if (!this.get('email')) {
    return 'https://gravatar.com/avatar/?s=200&d=retro';
  }
  const md5 = crypto.createHash('md5').update(this.get('email')).digest('hex');
  return `https://gravatar.com/avatar/${md5}?s=200&d=retro`;
});

const User = mongoose.model('User', userSchema);

module.exports = User;
