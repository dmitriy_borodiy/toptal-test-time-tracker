
module.exports = {
  isArray: function(value, { maxLength }) {
    console.log()
    if (!maxLength) {
      return Array.isArray(value);
    }
    return Array.isArray(value) && value.length <= maxLength;
  },
  isArrayOf: function(value, { predicate }) {
    if (!Array.isArray(value)) {
      return false;
    }
    for (let i = 0; i < value.length; ++i) {
      if (!predicate(value[i])) {
        return false;
      }
    }
    return true;
  },
};
