const mongoose = require('mongoose');
const moment = require('moment');
const cloneDeep = require('lodash/cloneDeep');

const utils = require('root/server/utils');
const Entry = require('root/server/models/Entry');
const USER_ROLES = require('root/server/models/constants').USER_ROLES;

const ENTRY_SCHEMA = {
  date: {
    isInt: {
      options: [{
        min: 0,
      }],
      errorMessage: 'Date can\'t be empty and should be in valid format',
    },
  },
  trackedSeconds: {
    notEmpty: {
      errorMessage: 'trackedSeconds cannot be blank',
    },
    isInt: {
      options: [{
        min: 1,
        max: 24 * 60 * 60,
      }],
      errorMessage: 'trackedSeconds should be integer >= 1 and <= 86400',
    },
  },
  notes: {
    optional: true,
    isArray: {
      options: [{
        maxLength: 32,
      }],
      errorMessage: 'Maximum number of notes for an entry is 32',
    },
    isArrayOf: {
      options: [{
        predicate: function validateNote(value) {
          return typeof(value) === 'string' && value.length < 1024;
        },
      }],
      errorMessage: 'Each note should be at most 1024 symbols long',
    },
  },
};

const ENTRY_PATCH_SCHEMA = cloneDeep(ENTRY_SCHEMA);
for (const key of Object.keys(ENTRY_SCHEMA)) {
  ENTRY_PATCH_SCHEMA[key].optional = true;
}

const USERS_POPULATE_QUERY = {
  path: 'user',
  select: ['name', 'email', 'preferredHoursPerDay'],
};

/**
 * GET /entries
 * Return a all entries
 */
exports.getEntries = function apiGetEntries(req, res) {
  req.assert('utcOffset', 'utcOffset should be an integer').notEmpty().isInt();
  const utcOffset = req.query.utcOffset;
  const errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }

  const { limit, offset } = req.paginationParams;
  const query = {};
  const filterParams = req.filterParams || {};
  if (filterParams.startDate != null || filterParams.endDate != null) {
    query.date = {};
    if (filterParams.startDate != null) {
      query.date['$gte'] = filterParams.startDate;
    }
    if (filterParams.endDate != null) {
      query.date['$lte'] = filterParams.endDate;
    }
  }

  Entry.paginate(query, {
    limit,
    offset,
    populate: USERS_POPULATE_QUERY,
    sort: [['date', -1], ['createdAt', -1]],
  }).then((result) => {
    return Entry.populateTotalTrackedTime({ entries: result.docs, utcOffset })
      .then((entries) => {
        delete result.docs;
        const transformedResult = Object.assign({}, result, { data: entries });
        res.send(transformedResult);
      })
  }).catch(() => res.status(400).send({ msg: 'Invalid params' }));
};

/**
 * GET /user/:userId/entries
 * Return a user's entries
 */
exports.getEntriesForUser = function apiGetEntriesForUser(req, res) {
  req.assert('utcOffset', 'utcOffset should be an integer').notEmpty().isInt();
  const errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }

  req.sanitize('utcOffset').toInt();
  const utcOffset = req.query.utcOffset;

  const userId = req.params.userId;
  const myUserId = req.user.id;

  if (req.user.role !== USER_ROLES.ADMIN) {
    if (userId !== myUserId) {
      return res.status(401).send({ msg: 'Unauthorized' });
    }
  }

  const { limit, offset } = req.paginationParams;

  const query = { user: userId };
  const filterParams = req.filterParams || {};
  if (filterParams.startDate != null || filterParams.endDate != null) {
    query.date = {};
    if (filterParams.startDate != null) {
      query.date['$gte'] = filterParams.startDate;
    }
    if (filterParams.endDate != null) {
      query.date['$lte'] = filterParams.endDate;
    }
  }

  Entry.paginate(query, {
    limit,
    offset,
    populate: USERS_POPULATE_QUERY,
    sort: [['date', -1], ['createdAt', -1]],
  })
  .then((result) => {
    return Entry.populateTotalTrackedTime({ entries: result.docs, utcOffset })
      .then((entries) => {
        delete result.docs;
        const transformedResult = Object.assign({}, result, { data: entries });
        res.send(transformedResult);
      })
  })
  .catch(() => res.status(400).send({ msg: 'Invalid params' }));
};

/**
 * GET /entries/:entryId
 * Return a single entry
 */
exports.getEntry = function apiGetEntry(req, res) {
  const entryId = req.params.entryId;
  const query = { _id: entryId };
  if (req.user.role !== USER_ROLES.ADMIN) {
    query.user = req.user.id;
  }
  Entry.findOne(query)
  .populate(USERS_POPULATE_QUERY)
  .then((entry) => {
    if (!entry) {
      return res.status(404).send({ msg: 'Entry does not exist or was deleted.' });
    }
    res.send(entry);
  })
  .catch((error) => {
    if (error.name === 'CastError') {
      return res.status(404).send({ msg: 'Entry does not exist or was deleted.' });
    }
    res.status(400).send({ msg: 'Invalid params' })
  });
};

/**
 * POST /user/:userId/entries
 * Create a new entry for user
 */
exports.createEntryForUser = function apiCreateEntryForUser(req, res) {
  const userId = req.params.userId;
  const myUserId = req.user.id;
  const myRole = req.user.role;

  if (myRole !== USER_ROLES.ADMIN) {
    if (myUserId !== userId) {
      return res.status(401).send({ msg: 'Unauthorized' });
    }
  }

  req.checkBody(ENTRY_SCHEMA);
  const errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }

  if (moment(req.body.date).isAfter(moment())) {
    return res.status(400).send({ msg: 'Date can not be in the future' });
  }

  const entry = new Entry({
    date: req.body.date,
    trackedSeconds: req.body.trackedSeconds,
    user: req.params.userId,
  });

  return entry.save()
  .then(savedEntry => res.send(savedEntry))
  .catch((error) => {
    res.status(400).send(error.errors);
  });
};

/**
 * PATCH /entries/:entryId
 */
exports.updateEntry = function apiUpdateEntry(req, res) {
  req.checkBody(ENTRY_PATCH_SCHEMA);
  const errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }

  if (req.body.date && moment(req.body.date).isAfter(moment())) {
    return res.status(400).send({
      msg: 'Date can not be in the future',
    });
  }

  const entryId = req.params.entryId;
  const myUserId = req.user.id.toString();
  const myRole = req.user.role;

  return Promise.resolve(Entry.findById(entryId))
  .then((entry) => {
    if (!entry) {
      res.status(404).send({ msg: 'Not Found' });
      return null;
    }
    if (myRole !== USER_ROLES.ADMIN) {
      if (entry.user.toString() !== myUserId) {
        res.status(401).send({ msg: 'Unauthorized' });
        return null;
      }
    }

    for (const field of Object.keys(ENTRY_SCHEMA)) {
      if (req.body[field] !== undefined) {
        entry[field] = req.body[field];
      }
    }
    return entry.save().then(savedEntry => res.send(savedEntry.toJSON()));
  })
  .catch(error => {
    if (error.name === 'CastError') {
      res.status(404).send({ msg: 'Not Found' });
    }
    res.status(400).send(error.errors);
  });
};

/**
 * DELETE /entries/:entryId
 */
exports.deleteEntry = function apiDeleteEntry(req, res) {
  const entryId = req.params.entryId;
  const myUserId = req.user.id.toString();
  const myRole = req.user.role;

  return Promise.resolve(Entry.findById(entryId))
    .then((entry) => {
      if (!entry) {
        res.status(404).send({ msg: 'Not Found' });
        return null;
      }
      if (myRole !== USER_ROLES.ADMIN) {
        if (entry.user.toString() !== myUserId) {
          res.status(401).send({ msg: 'Unauthorized' });
          return null;
        }
      }
      return entry.remove().then(() => res.send());
    })
    .catch(error => res.status(400).send(error.errors));
};
