const async = require('async');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const cloneDeep = require('lodash/cloneDeep');

const logger = require('root/server/logger');

const User = require('root/server/models/User');
const USER_ROLES = require('root/server/models/constants').USER_ROLES;
const utils = require('root/server/utils');

const USER_CREDENTIALS_SCHEMA = {
  name: {
    notEmpty: {
      errorMessage: 'Name cannot be blank',
    },
    isLength: {
      options: [{ min: 2, max: 256 }],
      errorMessage: 'Name must be at least 2 characters long',
    },
  },
  email: {
    notEmpty: {
      errorMessage: 'Email cannot be blank',
    },
    isEmail: {
      errorMessage: 'Email is not valid',
    },
  },
  password: {
    notEmpty: {
      errorMessage: 'Password cannot be blank',
    },
    isLength: {
      options: [{ min: 6 }],
      errorMessage: 'Password must be at least 6 characters long',
    },
  },
};

const USER_PROFILE_PARAMS_SCHEMA = {
  role: {
    notEmpty: true,
    matches: [USER_ROLES.USER, USER_ROLES.MANAGER, USER_ROLES.ADMIN],
  },
  preferredHoursPerDay: {
    notEmpty: {
      errorMessage: 'Preferred hours per day cannot be blank',
    },
    isInt: {
      options: [{
        min: 0,
        max: 24,
      }],
      errorMessage: 'Preferred hours per day should be integer between 0 and 24',
    },
  },
  name: USER_CREDENTIALS_SCHEMA.name,
  email: USER_CREDENTIALS_SCHEMA.email,
  location: {
    isLength: {
      options: [{ max: 256 }],
      errorMessage: 'Location must be at most 256 characters long',
    },
  },
  website: {
    isLength: {
      options: [{ max: 1024 }],
      errorMessage: 'Location must be at most 1024 characters long',
    },
  },
};

const USER_PROFILE_PARAMS_PATCH_SCHEMA = cloneDeep(USER_PROFILE_PARAMS_SCHEMA);
for (const key of Object.keys(USER_PROFILE_PARAMS_PATCH_SCHEMA)) {
  USER_PROFILE_PARAMS_PATCH_SCHEMA[key].optional = true;
}

function generateToken(user) {
  const payload = {
    iss: 'my.domain.com',
    sub: user.id,
    iat: moment().unix(),
    exp: moment().add(7, 'days').unix(),
  };
  return jwt.sign(payload, process.env.TOKEN_SECRET);
}

/**
 * Login required middleware
 */
exports.ensureAuthenticated = function userEnsureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    res.status(401).send({ msg: 'Unauthorized' });
  }
};

exports.ensureRole = function ensureUserHasRole(roles) {
  return function ensureUserHasRoleInner(req, res, next) {
    if (!req.isAuthenticated() || !roles.includes(req.user.role)) {
      res.status(401).send({ msg: 'Unauthorized' });
    } else {
      next();
    }
  };
};

  /**
   * POST /login
   * Sign in with email and password
   */
exports.loginPost = function apiLoginPost(req, res) {
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('email', 'Email cannot be blank').notEmpty();
  req.assert('password', 'Password cannot be blank').notEmpty();
  req.sanitize('email').normalizeEmail({ remove_dots: false });

  const errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }

  User.findOne({ email: req.body.email }, (err, user) => {
    if (!user) {
      return res.status(401).send({
        msg: `The email address ${req.body.email} is not associated with any account. ` +
          'Double-check your email address and try again.',
      });
    }
    user.comparePassword(req.body.password, (comparePasswordErr, isMatch) => {
      if (!isMatch) {
        return res.status(401).send({ msg: 'Invalid email or password' });
      }
      res.send({ token: generateToken(user), user: user.toJSON({ virtuals: true }) });
    });
  });
};

/**
 * POST /signup
 */
exports.signupPost = function apiSignupPost(req, res) {
  req.checkBody(USER_CREDENTIALS_SCHEMA);
  req.sanitize('email').normalizeEmail({ remove_dots: false });

  const errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }

  User.findOne({ email: req.body.email }, (err, user) => {
    if (user) {
      return res.status(400).send({ msg: 'The email address you have entered is already associated with another account.' });
    }
    user = new User({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
    });
    user.save((error) => {
      if (error) {
        return res.status(400).send(error.errors);
      }
      res.send({ token: generateToken(user), user: user.toJSON({ virtuals: true }) });
    });
  });
};

/**
 * POST /users
 */
exports.createUser = function apiCreateUser(req, res) {
  req.checkBody(USER_CREDENTIALS_SCHEMA);
  req.checkBody(USER_PROFILE_PARAMS_SCHEMA);
  req.sanitize('email').normalizeEmail({ remove_dots: false });

  // Managers can only create regular user accounts
  // Admins, on the other hand, can create any accounts
  if (req.user.role === USER_ROLES.MANAGER) {
    if (req.body.role !== USER_ROLES.USER) {
      return res.status(401).send();
    }
  }

  const errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }

  User.findOne({ email: req.body.email }, (err, user) => {
    if (user) {
      return res.status(400).send({
        msg: 'The email address you have entered is already associated with another account.',
      });
    }
    const userData = {};
    for (const field of Object.keys(USER_PROFILE_PARAMS_SCHEMA)) {
      if (req.body[field] !== undefined) {
        userData[field] = req.body[field];
      }
    }
    userData.password = req.body.password;
    user = new User(userData);
    user.save((error) => {
      if (error) {
        return res.status(400).send(error.errors);
      }
      res.send({ user: user.toJSON({ virtuals: true }) });
    });
  });
};

/**
 * GET /users
 */
exports.getUsers = function apiGetUsers(req, res) {
  const { limit, offset } = req.paginationParams;
  User.paginate({}, {
    limit,
    offset,
    sort: [['name', 1]],
  })
  .then((result) => {
    const transfromedResult = Object.assign({}, result, { data: result.docs });
    delete transfromedResult.docs;
    res.send(transfromedResult);
  })
  .catch(() => res.status(400).send({ msg: 'Invalid params' }));
};

/**
 * GET /users/:userId
 * Return a single entry
 */
exports.getUser = function apiGetUser(req, res) {
  const userId = req.params.userId;
  const query = { _id: userId };
  User.findOne(query)
  .then((user) => {
    if (!user) {
      return res.status(404).send({ msg: 'User profile does not exist or was deleted.' });
    }
    res.send(user);
  })
  .catch((error) => {
    if (error.name === 'CastError') {
      return res.status(404).send({ msg: 'User profile does not exist or was deleted.' });
    }
    res.status(400).send({ msg: 'Invalid params' })
  });
};

function editUser(req, res, { userId, schema, checkCanEdit }) {
  if ('password' in req.body) {
    req.checkBody({ password: USER_CREDENTIALS_SCHEMA.password });
    req.assert('confirm', 'Passwords must match').equals(req.body.password);
  } else {
    req.checkBody(schema);
    req.sanitize('email').normalizeEmail({ remove_dots: false });
  }

  const errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }

  User.findById(userId, (err, user) => {
    if ('password' in req.body) {
      user.password = req.body.password;
    } else {
      if (!checkCanEdit(user)) {
        return res.status(401).send({ msg: 'Unauthorized' });
      }

      for (const field of Object.keys(schema)) {
        if (req.body[field] !== undefined) {
          user[field] = req.body[field];
        }
      }
    }
    user.save((saveErr) => {
      if (saveErr) {
        if (saveErr.code === 11000) {
          return res.status(409)
            .send({ msg: 'The email address you have entered is already associated with another account.' });
        } else {
          return res.status(500);
        }
      }
      if ('password' in req.body) {
        res.send({ user: user.toJSON({ virtuals: true }), msg: 'Password has been changed.' });
      } else {
        res.send({ user: user.toJSON({ virtuals: true }), msg: 'Profile information has been updated.' });
      }
    });
  });
}

/**
 * PATCH /account
 * Update profile information OR change password.
 */
exports.updateAccount = function apiUpdateAccount(req, res) {
  function accountPatchCheckAuth(targetUser) {
    if (!targetUser) {
      return false;
    }
    if (!req.user) {
      return false;
    }
    if (targetUser.isSystemAdmin) {
      // System admin can not change her role
      if (req.body.role !== undefined && targetUser.role !== req.body.role) {
        return false;
      }
    }
    return req.user.id === targetUser.id;
  }

  return editUser(req, res, {
    userId: req.user.id,
    checkCanEdit: accountPatchCheckAuth,
    schema: USER_PROFILE_PARAMS_PATCH_SCHEMA,
  });
};

/**
 * PATCH /users/:id
 * Update user information OR change password.
 */
exports.updateUser = function apiUpdateUser(req, res) {
  function userPatchCheckAuth(targetUser) {
    if (req.user.role === USER_ROLES.USER) {
      return false;
    }
    if (req.user.role === USER_ROLES.MANAGER) {
      // Managers can only edit regular users
      if (targetUser.role !== USER_ROLES.USER) {
        return false;
      }
      // Managers can not change user roles
      if (req.body.role !== undefined && targetUser.role !== req.body.role) {
        return false;
      }
    }

    if (targetUser.isSystemAdmin) {
      // Only system admin herself can edit her account
      if (req.user.id !== targetUser.id) {
        return false;
      }
      // System admin can not change her role
      if (req.body.role !== undefined && targetUser.role !== req.body.role) {
        return false;
      }
    }
    return true;
  }

  return editUser(req, res, {
    userId: req.params.id,
    checkCanEdit: userPatchCheckAuth,
    schema: USER_PROFILE_PARAMS_PATCH_SCHEMA,
  });
};

/**
 * DELETE /users/:id
 */
exports.deleteUser = function apiDeleteUser(req, res) {
  if (req.user.id === req.params.id) {
    return res.status(401).send();
  }
  User.findOneAndRemove({ _id: req.params.id, isSystemAdmin: false })
    .exec((error, item) => {
      if (error) {
          return res.json({ msg: 'Cannot remove user' });
      }
      if (!item) {
          return res.status(404).json({ msg: 'Could not remove the user account' });
      }
      res.send();
    });
};

/**
 * DELETE /account
 */
exports.deleteAccount = function apiDeleteAccount(req, res) {
  User.findOneAndRemove({ _id: req.user.id, isSystemAdmin: false })
    .exec((error, item) => {
      if (error) {
        return res.json({ msg: 'Could not delete your account' });
      }
      if (!item) {
        return res.status(404).json({ msg: 'Could not delete your account' });
      }
      res.send({ msg: 'Your account has been permanently deleted' });
    });
};

/**
 * POST /forgot
 */
exports.forgotPost = function apiForgotPost(req, res) {
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('email', 'Email cannot be blank').notEmpty();
  req.sanitize('email').normalizeEmail({ remove_dots: false });

  const errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }

  async.waterfall([
    function createRandomBytes(done) {
      crypto.randomBytes(16, (err, buf) => {
        const token = buf.toString('hex');
        done(err, token);
      });
    },
    function savePasswordResetToken(token, done) {
      User.findOne({ email: req.body.email }, (err, user) => {
        if (!user) {
          return res.status(400).send({ msg: `The email address ${req.body.email} is not associated with any account.` });
        }
        user.passwordResetToken = token;
        user.passwordResetExpires = Date.now() + 3600000; // expire in 1 hour
        user.save((saveErr) => {
          done(saveErr, token, user);
        });
      });
    },
    function sendForgotPasswordEmail(token, user, done) {
      const transporter = nodemailer.createTransport({
        service: 'Mailgun',
        auth: {
          user: process.env.MAILGUN_USERNAME,
          pass: process.env.MAILGUN_PASSWORD,
        },
      });
      const mailOptions = {
        to: user.email,
        from: 'support@toptal-test-time-tracker.herokuapp.com',
        subject: '✔ Reset your password on Time Tracker',
        text: `${'You are receiving this email because you (or someone else) have requested the reset of the password for your account.\n\n' +
        'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
        'http://'}${req.headers.host}/reset/${token}\n\n` +
        'If you did not request this, please ignore this email and your password will remain unchanged.\n',
      };
      transporter.sendMail(mailOptions, (err) => {
        res.send({ msg: `An email has been sent to ${user.email} with further instructions.` });
        done(err);
      });
    },
  ]);
};

/**
 * POST /reset
 */
exports.resetPost = function apiResetPost(req, res) {
  req.assert('password', 'Password must be at least 4 characters long').len(4);
  req.assert('confirm', 'Passwords must match').equals(req.body.password);

  const errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }

  async.waterfall([
    function updateUserPassword(done) {
      User.findOne({ passwordResetToken: req.params.token })
        .where('passwordResetExpires').gt(Date.now())
        .exec((err, user) => {
          if (!user) {
            return res.status(400).send({ msg: 'Password reset token is invalid or has expired.' });
          }
          user.password = req.body.password;
          user.passwordResetToken = undefined;
          user.passwordResetExpires = undefined;
          user.save((saveErr) => {
            done(saveErr, user);
          });
        });
    },
    function resetPasswordEmailSend(user) {
      const transporter = nodemailer.createTransport({
        service: 'Mailgun',
        auth: {
          user: process.env.MAILGUN_USERNAME,
          pass: process.env.MAILGUN_PASSWORD,
        },
      });
      const mailOptions = {
        from: 'support@toptal-test-time-tracker.herokuapp.com',
        to: user.email,
        subject: 'Your Time Tracker password has been changed',
        text: `${'Hello,\n\n' +
        'This is a confirmation that the password for your account '}${user.email} has just been changed.\n`,
      };
      transporter.sendMail(mailOptions, () => {
        res.send({ msg: 'Your password has been changed successfully.' });
      });
    },
  ]);
};
