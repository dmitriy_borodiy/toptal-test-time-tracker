const request = require('supertest');
const chai = require('chai');
const chaiHttp = require('chai-http');

const apiTestUtils = require('./utils');
const server = require('root/server');
const USER_ROLES = require('root/server/models/constants').USER_ROLES;

const should = chai.should();
chai.use(chaiHttp);

const API_PREFIX = '/api/v0';

describe('Users', function() {

  describe('/users POST', function() {

    describe('For non-authenticated users', function() {
        it('should not allow to create users', function(done) {
          request(server)
            .post(`${API_PREFIX}/users`)
            .send({
              'name': 'a-new-user',
              'email': 'a-new-user@foo.com',
              'password': 'doesnt-relly-matter',
              'role': USER_ROLES.USER,
            })
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401, done);
        });
    });

    describe('For regular users', function() {
        let token;
        before((done) => {
          apiTestUtils.removeAllUsers()
          .then(() => apiTestUtils.createUserAndLogin({
            name: 'user',
            email: 'user@foo.com',
            password: 'qwerty',
          }))
          .then(({ token: authToken }) => {
            token = authToken;
            done();
          })
          .catch(done);
        });

        it('should not allow to create users', function(done) {
          request(server)
            .post(`${API_PREFIX}/users`)
            .send({
              name: 'a-new-user',
              email: 'a-new-user@foo.com',
              password: 'doesnt-relly-matter',
              role: USER_ROLES.USER,
              preferredHoursPerDay: 8,
            })
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401, done);
        });
    });

    describe('For managers', function() {
        let token;
        before((done) => {
          apiTestUtils.removeAllUsers()
          .then(() => apiTestUtils.createUserAndLogin({
            name: 'manager',
            email: 'manager@foo.com',
            password: 'qwerty',
            role: USER_ROLES.MANAGER,
          }))
          .then(({ token: authToken }) => {
            token = authToken;
            done();
          })
          .catch(done);
        });

        it('should create regular users', function(done) {
          request(server)
            .post(`${API_PREFIX}/users`)
            .send({
              name: 'a-new-user',
              email: 'a-new-user@foo.com',
              password: 'doesnt-relly-matter',
              role: USER_ROLES.USER,
              preferredHoursPerDay: 8,
            })
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200, done);
        });

        it('should not allow to create managers', function(done) {
          request(server)
            .post(`${API_PREFIX}/users`)
            .send({
              name: 'a-new-user',
              email: 'a-new-manager@foo.com',
              password: 'doesnt-relly-matter',
              role: USER_ROLES.MANAGER,
              preferredHoursPerDay: 8,
            })
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401, done);
        });

        it('should not allow to create admins', function(done) {
          request(server)
            .post(`${API_PREFIX}/users`)
            .send({
              name: 'a-new-user',
              email: 'a-new-admin@foo.com',
              password: 'doesnt-relly-matter',
              role: USER_ROLES.ADMIN,
              preferredHoursPerDay: 8,
            })
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401, done);
        });
    });

    describe('For admins', function() {
        let token;
        before((done) => {
          apiTestUtils.removeAllUsers()
          .then(() => apiTestUtils.createUserAndLogin({
            name: 'admin',
            email: 'admin@foo.com',
            password: 'qwerty',
            role: USER_ROLES.ADMIN,
          }))
          .then(({ token: authToken }) => {
            token = authToken;
            done();
          })
          .catch(done);
        });

        it('should create regular users', function(done) {
          request(server)
            .post(`${API_PREFIX}/users`)
            .send({
              name: 'a-new-user',
              email: 'a-new-user@foo.com',
              password: 'doesnt-relly-matter',
              role: USER_ROLES.USER,
              preferredHoursPerDay: 8,
            })
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200, done);
        });

        it('should create manager users', function(done) {
          request(server)
            .post(`${API_PREFIX}/users`)
            .send({
              name: 'a-new-user',
              email: 'a-new-manager@foo.com',
              password: 'doesnt-relly-matter',
              role: USER_ROLES.MANAGER,
              preferredHoursPerDay: 8,
            })
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200, done);
        });

        it('should create admin users', function(done) {
          request(server)
            .post(`${API_PREFIX}/users`)
            .send({
              name: 'a-new-user',
              email: 'a-new-admin@foo.com',
              password: 'doesnt-relly-matter',
              role: USER_ROLES.ADMIN,
              preferredHoursPerDay: 8,
            })
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200, done);
        });
    });

    describe('Input validation', function() {
        let token;
        before((done) => {
          apiTestUtils.removeAllUsers()
          .then(() => apiTestUtils.createUserAndLogin({
            name: 'admin',
            email: 'admin@foo.com',
            password: 'qwerty',
            role: USER_ROLES.ADMIN,
          }))
          .then(({ token: authToken }) => {
            token = authToken;
            done();
          })
          .catch(done);
        });

        it('should disallow unknown roles', function(done) {
          request(server)
            .post(`${API_PREFIX}/users`)
            .send({
              name: 'a-new-user',
              email: 'a-new-user@foo.com',
              password: 'doesnt-relly-matter',
              role: 'this-role-doesnt-exist',
              preferredHoursPerDay: 8,
            })
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(400, done);
        });
    });
  });

  describe('/users GET', function() {

    describe('For non-authenticated users', function() {
        it('should not allow to list users', function(done) {
          request(server)
            .get(`${API_PREFIX}/users`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401, done);
        });
    });

    describe('For regular users', function() {
        let token;
        before((done) => {
          apiTestUtils.removeAllUsers()
          .then(() => apiTestUtils.createUserAndLogin({
            name: 'user',
            email: 'user@foo.com',
            password: 'qwerty',
          }))
          .then(({ token: authToken }) => {
            token = authToken;
            done();
          })
          .catch(done);
        });

        it('should not allow to list users', function(done) {
          request(server)
            .get(`${API_PREFIX}/users/`)
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401, done);
        });
    });

    describe('For managers', function() {
        let token;
        before((done) => {
          apiTestUtils.removeAllUsers()
          .then(() => apiTestUtils.createUser({
            name: 'user-test',
            email: 'user-test@foo.com',
            password: 'qwerty',
          }))
          .then(() => apiTestUtils.createUserAndLogin({
            name: 'manager',
            email: 'manager@foo.com',
            password: 'qwerty',
            role: USER_ROLES.MANAGER,
          }))
          .then(({ token: authToken }) => {
            token = authToken;
            done();
          })
          .catch(done);
        });

        it('should list users', function(done) {
          request(server)
            .get(`${API_PREFIX}/users`)
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200)
            .expect((req) => {
              req.should.be.json;
              req.body.should.be.a('object');
              req.body.should.have.property('data');
              req.body.data.should.be.a('array').of.length(2);
              req.body.data[0].should.be.a('object');
              req.body.data[0].should.have.property('id');
              req.body.data[0].should.not.have.property('password');
            })
            .end(done);
        });
    });

    describe('For admins', function() {
        let token;
        before((done) => {
          apiTestUtils.removeAllUsers()
          .then(() => apiTestUtils.createUser({
            name: 'user-test',
            email: 'user-test@foo.com',
            password: 'qwerty',
          }))
          .then(() => apiTestUtils.createUserAndLogin({
            name: 'admin',
            email: 'admin@foo.com',
            password: 'qwerty',
            role: USER_ROLES.ADMIN,
          }))
          .then(({ token: authToken }) => {
            token = authToken;
            done();
          })
          .catch(done);
        });

        it('should list users', function(done) {
          request(server)
            .get(`${API_PREFIX}/users`)
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200)
            .expect((req) => {
              req.should.be.json;
              req.body.should.be.a('object');
              req.body.should.have.property('data');
              req.body.data.should.be.a('array').of.length(2);
              req.body.data[0].should.be.a('object');
              req.body.data[0].should.have.property('id');
              req.body.data[0].should.not.have.property('password');
            })
            .end(done);
        });
    });
  });

  describe('/users/:userId GET', function() {

    describe('For non-authenticated users', function() {
        it('should return 401', function(done) {
          request(server)
            .get(`${API_PREFIX}/users`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401, done);
        });
    });

    describe('For regular users', function() {
        let token;
        before((done) => {
          apiTestUtils.removeAllUsers()
          .then(() => apiTestUtils.createUserAndLogin({
            name: 'user',
            email: 'user@foo.com',
            password: 'qwerty',
          }))
          .then(({ token: authToken }) => {
            token = authToken;
            done();
          })
          .catch(done);
        });

        it('should return 401', function(done) {
          request(server)
            .get(`${API_PREFIX}/users`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401, done);
        });
    });

    describe('For managers', function() {
        let token, userId;
        before((done) => {
          apiTestUtils.removeAllUsers()
          .then(() => apiTestUtils.createUser({
            name: 'user-test',
            email: 'user-test@foo.com',
            password: 'qwerty',
          }))
          .then((user) => {
            userId = user._id.toString();
          })
          .then(() => apiTestUtils.createUserAndLogin({
            name: 'manager',
            email: 'manager@foo.com',
            password: 'qwerty',
            role: USER_ROLES.MANAGER,
          }))
          .then(({ token: authToken }) => {
            token = authToken;
            done();
          })
          .catch(done);
        });

        it('should return user profile', function(done) {
          request(server)
            .get(`${API_PREFIX}/users/${userId}`)
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200)
            .expect((req) => {
              req.should.be.json;
              req.body.should.be.a('object');
              req.body.should.have.property('id').equal(userId);
              req.body.should.have.property('name').equal('user-test');
            })
            .end(done);
        });
    });

    describe('For admins', function() {
        let token, userId;
        before((done) => {
          apiTestUtils.removeAllUsers()
          .then(() => apiTestUtils.createUser({
            name: 'user-test',
            email: 'user-test@foo.com',
            password: 'qwerty',
          }))
          .then((user) => {
            userId = user._id.toString();
          })
          .then(() => apiTestUtils.createUserAndLogin({
            name: 'admin',
            email: 'admin@foo.com',
            password: 'qwerty',
            role: USER_ROLES.ADMIN,
          }))
          .then(({ token: authToken }) => {
            token = authToken;
            done();
          })
          .catch(done);
        });

        it('should return user profile', function(done) {
          request(server)
            .get(`${API_PREFIX}/users/${userId}`)
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200)
            .expect((req) => {
              req.should.be.json;
              req.body.should.be.a('object');
              req.body.should.have.property('id').equal(userId);
              req.body.should.have.property('name').equal('user-test');
            })
            .end(done);
        });
    });
  });

  describe('/users/:id DELETE', function() {

    describe('For non-authenticated users', function() {
        it('should not allow to delete users', function(done) {
          request(server)
            .delete(`${API_PREFIX}/users/a-user-id`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401, done);
        });
    });

    describe('For regular users', function() {
        let token;
        before((done) => {
          apiTestUtils.removeAllUsers()
          .then(() => apiTestUtils.createUserAndLogin({
            name: 'user',
            email: 'user@foo.com',
            password: 'qwerty',
          }))
          .then(({ token: authToken }) => {
            token = authToken;
            done();
          })
          .catch(done);
        });

        it('should not allow to delete users', function(done) {
          request(server)
            .delete(`${API_PREFIX}/users/a-user-id`)
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401, done);
        });
    });

    describe('For managers', function() {
        let token, testUserId;
        before((done) => {
          apiTestUtils.removeAllUsers()
          .then(() => apiTestUtils.createUser({
            name: 'user-test',
            email: 'user-test@foo.com',
            password: 'qwerty',
          }))
          .then((testUser) => {
            testUserId = testUser._id;
          })
          .then(() => apiTestUtils.createUserAndLogin({
            name: 'manager',
            email: 'manager@foo.com',
            password: 'qwerty',
            role: USER_ROLES.MANAGER,
          }))
          .then(() => apiTestUtils.login({
            email: 'manager@foo.com',
            password: 'qwerty',
          }))
          .then(({ token: authToken }) => {
            token = authToken;
            done();
          })
          .catch(done);
        });

        it('should delete a user', function(done) {
          request(server)
            .delete(`${API_PREFIX}/users/${testUserId}`)
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200)
            .end(done);
        });
    });

    describe('For admins', function() {
        let token, testUserId, selfId;
        before((done) => {
          apiTestUtils.removeAllUsers()
          .then(() => apiTestUtils.createUser({
            name: 'user-test',
            email: 'user-test@foo.com',
            password: 'qwerty',
          }))
          .then((testUser) => {
            testUserId = testUser._id;
          })
          .then(() => apiTestUtils.createUserAndLogin({
            name: 'admin',
            email: 'admin@foo.com',
            password: 'qwerty',
            role: USER_ROLES.ADMIN,
          }))
          .then(({ user, token: authToken}) => {
            token = authToken;
            selfId = user.id;
            done();
          })
          .catch(done);
        });

        it('should delete a user', function(done) {
          request(server)
            .delete(`${API_PREFIX}/users/${testUserId}`)
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200)
            .end(done);
        });

        it('should not allow to delete yourself', function(done) {
          request(server)
            .delete(`${API_PREFIX}/users/${selfId}`)
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401)
            .end(done);
        });
    });
  });

  describe('/users/:id PATCH', function() {

    describe('For non-authenticated users', function() {
        it('should not allow to edit users', function(done) {
          request(server)
            .patch(`${API_PREFIX}/users/a-user-id`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401, done);
        });
    });

    describe('For regular users', function() {
        let token;
        before((done) => {
          apiTestUtils.removeAllUsers()
          .then(() => apiTestUtils.createUserAndLogin({
            name: 'user',
            email: 'user@foo.com',
            password: 'qwerty',
          }))
          .then(({ token: authToken }) => {
            token = authToken;
            done();
          })
          .catch(done);
        });

        it('should not allow to edit users', function(done) {
          request(server)
            .patch(`${API_PREFIX}/users/a-user-id`)
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401, done);
        });
    });

    describe('For managers', function() {
        let token, testUserManagerId, testUserAdminId, testUserId;
        before((done) => {
          apiTestUtils.removeAllUsers()
          .then(() => apiTestUtils.createUser({
            name: 'user-test-manager',
            email: 'user-test-manager@foo.com',
            password: 'qwerty',
            role: USER_ROLES.MANAGER,
          }))
          .then((testUser) => {
            testUserManagerId = testUser._id;
          })
          .then(() => apiTestUtils.createUser({
            name: 'user-test-admin',
            email: 'user-test-admin@foo.com',
            password: 'qwerty',
            role: USER_ROLES.ADMIN,
          }))
          .then((testUser) => {
            testUserAdminId = testUser._id;
          })
          .then(() => apiTestUtils.createUser({
            name: 'user-test',
            email: 'user-test@foo.com',
            password: 'qwerty',
            role: USER_ROLES.USER,
          }))
          .then((testUser) => {
            testUserId = testUser._id;
          })
          .then(() => apiTestUtils.createUserAndLogin({
            name: 'manager',
            email: 'manager@foo.com',
            password: 'qwerty',
            role: USER_ROLES.MANAGER,
          }))
          .then(() => apiTestUtils.login({
            email: 'manager@foo.com',
            password: 'qwerty',
          }))
          .then(({ token: authToken }) => {
            token = authToken;
            done();
          })
          .catch(done);
        });

        it('should edit a regular user', function(done) {
          request(server)
            .patch(`${API_PREFIX}/users/${testUserId}`)
            .send({
              name: 'user-test-new-name',
              email: 'user-test@foo.com',
              preferredHoursPerDay: 3,
            })
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200)
            .expect((res) => {
              res.should.be.json;
              res.body.should.be.a('object');
              res.body.should.have.property('user');
              res.body.user.should.be.a('object');
              res.body.user.name.should.be.a('string');
              res.body.user.name.should.equal('user-test-new-name');
              res.body.user.should.have.property('gravatar');
              res.body.user.should.have.property('id');
              res.body.user.should.not.have.property('password');
              res.body.user.should.have.property('preferredHoursPerDay').equal(3);
            })
            .end(done);
        });

        it('should not be able to change roles', function(done) {
          request(server)
            .patch(`${API_PREFIX}/users/${testUserId}`)
            .send({
              role: USER_ROLES.MANAGER,
            })
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401)
            .end(done);
        });

        it('should not be able to edit a manager', function(done) {
          request(server)
            .patch(`${API_PREFIX}/users/${testUserManagerId}`)
            .send({
              name: 'user-test-new-name',
            })
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401)
            .end(done);
        });

        it('should not be able to edit an admin', function(done) {
          request(server)
            .patch(`${API_PREFIX}/users/${testUserAdminId}`)
            .send({
              email: 'user-test-admin@foo.com',
              preferredHoursPerDay: 3,
            })
            .set('Authorization', `Bearer ${token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401)
            .end(done);
        });
    });

    describe('For admins', function() {
        let token, selfId, testUserManagerId, testUserAdminId, testUserId;
        before((done) => {
          apiTestUtils.removeAllUsers()
          .then(() => apiTestUtils.createUser({
            name: 'user-test-manager',
            email: 'user-test-manager@foo.com',
            password: 'qwerty',
            role: USER_ROLES.MANAGER,
          }))
          .then((testUser) => {
            testUserManagerId = testUser._id;
          })
          .then(() => apiTestUtils.createUser({
            name: 'user-test-admin',
            email: 'user-test-admin@foo.com',
            password: 'qwerty',
            role: USER_ROLES.ADMIN,
          }))
          .then((testUser) => {
            testUserAdminId = testUser._id;
          })
          .then(() => apiTestUtils.createUser({
            name: 'user-test',
            email: 'user-test@foo.com',
            password: 'qwerty',
            role: USER_ROLES.USER,
          }))
          .then((testUser) => {
            testUserId = testUser._id;
          })
          .then(() => apiTestUtils.createUserAndLogin({
            name: 'admin',
            email: 'admin@foo.com',
            password: 'qwerty',
            role: USER_ROLES.ADMIN,
          }))
          .then(({ user, token: authToken}) => {
            token = authToken;
            selfId = user.id;
            done();
          })
          .catch(done);
        });

        it('should edit a manager', function(done) {
          Promise.resolve(
            request(server)
              .patch(`${API_PREFIX}/users/${testUserManagerId}`)
              .set('Authorization', `Bearer ${token}`)
              .set('Content-Type', 'application/json')
              .set('Accept', 'application/json')
              .send({
                name: 'user-test-manager-new-name',
                email: 'user-test-manager@foo.com',
                preferredHoursPerDay: 3,
              })
              .expect(200)
          )
          .then(() => apiTestUtils.getUser({ email: 'user-test-manager@foo.com' }))
          .then((user) => {
            user.should.be.a('object');
            user.should.have.property('name').equal('user-test-manager-new-name');
            user.should.have.property('preferredHoursPerDay').equal(3);
            done();
          })
          .catch(done);
        });

        it('should edit an admin', function(done) {
          Promise.resolve(
            request(server)
              .patch(`${API_PREFIX}/users/${testUserAdminId}`)
              .set('Authorization', `Bearer ${token}`)
              .set('Content-Type', 'application/json')
              .set('Accept', 'application/json')
              .send({
                name: 'user-test-admin-new-name',
                email: 'user-test-admin@foo.com',
                preferredHoursPerDay: 3,
              })
              .expect(200)
          )
          .then(() => apiTestUtils.getUser({ email: 'user-test-admin@foo.com' }))
          .then((user) => {
            user.should.be.a('object');
            user.should.have.property('name').equal('user-test-admin-new-name');
            user.should.have.property('preferredHoursPerDay').equal(3);
            done();
          })
          .catch(done);
        });

        it('should be able to edit roles', function(done) {
          Promise.resolve(
            request(server)
              .patch(`${API_PREFIX}/users/${testUserAdminId}`)
              .set('Authorization', `Bearer ${token}`)
              .set('Content-Type', 'application/json')
              .set('Accept', 'application/json')
              .send({
                role: USER_ROLES.MANAGER,
              })
              .expect(200)
          )
          .then(() => apiTestUtils.getUser({ email: 'user-test-admin@foo.com' }))
          .then((user) => {
            user.should.be.a('object');
            user.should.have.property('role').equal(USER_ROLES.MANAGER);
            done();
          })
          .catch(done);
        });
    });
  });
});
