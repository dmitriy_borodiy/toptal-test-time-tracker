const request = require('supertest');
const chai = require('chai');
const uuid = require('uuid-js');
const chaiHttp = require('chai-http');

const apiTestUtils = require('./utils');
const server = require('root/server');
const USER_ROLES = require('root/server/models/constants').USER_ROLES;

const should = chai.should();
chai.use(chaiHttp);

const API_PREFIX = '/api/v0';

describe('Pagination', function() {
  let token, users;
  const TEST_USERS_COUNT = 20;
  const usersData = [];
  for (let i = 0; i < TEST_USERS_COUNT; ++i) {
    const userId = uuid.create();
    usersData[i] = {
      name: `user-${userId}`,
      email: `user-${userId}@foo.com`,
      password: 'qwerty',
    };
  };

  before((done) => {
    apiTestUtils.removeAllUsers()
    .then(() => apiTestUtils.createUserAndLogin({
      name: 'manager',
      email: 'manager@foo.com',
      password: 'qwerty',
      role: USER_ROLES.MANAGER,
    }))
    .then(({ token: authToken }) => {
      token = authToken;
    })
    .then(() => apiTestUtils.createUser(usersData))
    .then(apiTestUtils.getUsers)
    .then((allUsers) => {
      users = allUsers;
      done();
    })
    .catch(done);
  });

  describe('GET /users', function() {
    describe('returns paginated response', function() {
      let response;
      const params = {
        limit: 5,
        offset: 11,
      };

      before((done) => {
        Promise.resolve(request(server)
          .get(`${API_PREFIX}/users`)
          .query(params)
          .set('Authorization', `Bearer ${token}`)
          .set('Accept', 'application/json')
          .expect(200))
        .then(res => response = res)
        .then(() => done(), done);
      });

      it('with field "data" holding list of users', function() {
        response.should.be.json;
        response.body.should.be.a('object');
        response.body.should.have.property('data');
        response.body.data.should.be.a('array').of.length(params.limit);
      });

      it('with field "limit" holding number of users in the current response', function() {
        response.body.should.have.property('limit');
        response.body.limit.should.be.a('number');
        response.body.limit.should.equal(params.limit);
        response.body.limit.should.equal(response.body.data.length);
      });

      it('with field "total" holding total number of users', function() {
        response.body.should.have.property('total');
        response.body.total.should.be.a('number');
        response.body.total.should.equal(TEST_USERS_COUNT + 1);
      });

      it('with field "offset" holding the current offset', function() {
        response.body.should.have.property('offset');
        response.body.offset.should.be.a('number');
        response.body.offset.should.equal(params.offset);
      });
    });

    describe('handles', function() {

      function testGetUsers({ query, status=200, values={} }) {
        it(`for ${JSON.stringify(query)} returns status ${status}`, function(done) {
          Promise.resolve(
            request(server)
              .get(`${API_PREFIX}/users/`)
              .query(query)
              .set('Authorization', `Bearer ${token}`)
              .set('Content-Type', 'application/json')
              .set('Accept', 'application/json')
              .expect(status)
              .expect((res) => {
                res.should.be.json;
                for (const valueKey of Object.keys(values)) {
                  res.body.should.have.property(valueKey);
                  res.body[valueKey].should.equal(values[valueKey]);
                }
              })
          ).then(() => done(), done);
        });
      }

      describe('valid inputs', function() {
        const testCases = [
          {
            query: {},
            values: { limit: 10, offset: 0, total: TEST_USERS_COUNT + 1 },
          },
          {
            query: { limit: 100000 },
            values: { limit: 100, offset: 0, total: TEST_USERS_COUNT + 1 },
          },
          {
            query: { limit: 10, offset: 100000 },
            values: { limit: 10, offset: 100000, total: TEST_USERS_COUNT + 1 },
          },
          {
            query: { limit: 1 },
            values: { limit: 1, offset: 0, total: TEST_USERS_COUNT + 1 },
          },
          {
            query: { limit: 0, },
            status: 200,
            values: { limit: 0, offset: 0, total: TEST_USERS_COUNT + 1 },
          },
        ];
        testCases.forEach(testGetUsers);
      });

      describe('invalid inputs', function() {
        const testCases = [
          { query: { offset: -1 }, status: 400 },
          { query: { limit: 'abc', }, status: 400 },
          { query: { offset: 'abc', }, status: 400 },
          { query: { limit: NaN, }, status: 400 },
          { query: { offset: NaN, }, status: 400 },
          { query: { offset: null, }, status: 400 },
        ];
        testCases.forEach(testGetUsers);
      });
    });
  });
});
