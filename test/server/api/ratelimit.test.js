const request = require('supertest');
const moment = require('moment');
const chai = require('chai');
const chaiHttp = require('chai-http');

const apiTestUtils = require('./utils');
const createAppInstance = require('root/server/server');
const USER_ROLES = require('root/server/models/constants').USER_ROLES;

const ratelimitConfig = require('root/server/ratelimit-config');

const should = chai.should();
const expect = chai.expect;
chai.use(chaiHttp);

const API_PREFIX = '/api/v0';

const utcOffset = moment().utcOffset();

describe(`Rate limit`, function() {

  let server;
  let adminToken;

  beforeEach((done) => {
    apiTestUtils.removeAllUsers()
    .then(() => apiTestUtils.removeAllEntries())
    .then(() => apiTestUtils.createUserAndLogin({
      name: 'admin',
      email: 'admin@foo.com',
      password: 'qwerty',
      role: USER_ROLES.ADMIN,
    }))
    .then(({ token: authToken }) => {
      adminToken = authToken;
    })
    .then(() => server = createAppInstance())
    .then(() => done(), done)
  });

  const testCases = [
    {
      name: 'SSR',
      max: ratelimitConfig.SSR_RATELIMIT_CONFIG.max,
      createRequest: () => request(server).get('/'),
    },
    {
      name: 'GET /entries/',
      max: ratelimitConfig.API_RATELIMIT_CONFIG.max,
      createRequest: () => request(server).get(`${API_PREFIX}/entries/?utcOffset=${utcOffset}`),
    },
    {
      name: 'signup',
      max: ratelimitConfig.SIGNUP_RATELIMIT_CONFIG.max,
      createRequest: (i) => {
        return request(server)
          .post(`${API_PREFIX}/signup/`)
          .send({
            name: `user-${i}`,
            email: `user-${i}@test.com`,
            password: 'qwerty',
          });
      },
    },
    {
      name: 'login',
      max: ratelimitConfig.LOGIN_RATELIMIT_CONFIG.max,
      createRequest: (i) => {
        return request(server)
          .post(`${API_PREFIX}/login/`)
          .send({
            email: `user-${i}@test.com`,
            password: 'qwerty',
          });
      },
      status: 401,
    },
  ];

  for (const { name, max, createRequest, status=200 } of testCases) {
    it(name, function(done) {
      const requestPromises = [];
      for (let i = 0; i < max; ++i) {
        requestPromises.push(Promise.resolve(
          createRequest(i)
            .set('x-enable-ratelimit-for-tests', 'true')
            .set('Authorization', `Bearer ${adminToken}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(status)));
      }
      Promise.all(requestPromises)
        .then(() => Promise.resolve(
          createRequest(max + 1)
            .set('x-enable-ratelimit-for-tests', 'true')
            .set('Authorization', `Bearer ${adminToken}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(429)
        )).then(() => done(), done)
    });
  }
});
