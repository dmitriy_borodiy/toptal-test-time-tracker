const request = require('supertest');
const moment = require('moment');
const chai = require('chai');
const chaiHttp = require('chai-http');

const apiTestUtils = require('./utils');
const server = require('root/server');
const USER_ROLES = require('root/server/models/constants').USER_ROLES;

const should = chai.should();
const expect = chai.expect;
chai.use(chaiHttp);

const API_PREFIX = '/api/v0';

const utcOffset = moment().utcOffset();

describe('Entries', function() {

  const ENTRIES_PER_USER = 3;
  const PREFFERED_HOURS_PER_DAY = 4;
  let userToken, managerToken, adminToken;
  let userId, managerId, adminId;
  let entries;
  let userEntryId, userEntryId2, managerEntryId, adminEntryId;

  before((done) => {
    apiTestUtils.removeAllUsers()
    .then(() => apiTestUtils.removeAllEntries())
    .then(() => apiTestUtils.createUserAndLogin({
      name: 'admin',
      email: 'admin@foo.com',
      password: 'qwerty',
      role: USER_ROLES.ADMIN,
      preferredHoursPerDay: PREFFERED_HOURS_PER_DAY,
    }))
    .then(({ token: authToken, user }) => {
      adminToken = authToken;
      adminId = user.id;
    })
    .then(() => apiTestUtils.createUserAndLogin({
      name: 'manager',
      email: 'manager@foo.com',
      password: 'qwerty',
      role: USER_ROLES.MANAGER,
      preferredHoursPerDay: PREFFERED_HOURS_PER_DAY,
    }))
    .then(({ token: authToken, user }) => {
      managerToken = authToken;
      managerId = user.id;
    })
    .then(() => apiTestUtils.createUserAndLogin({
      name: 'user',
      email: 'user@foo.com',
      password: 'qwerty',
      role: USER_ROLES.USER,
      preferredHoursPerDay: PREFFERED_HOURS_PER_DAY,
    }))
    .then(({ token: authToken, user }) => {
      userToken = authToken;
      userId = user.id;
    })
    .then(() => apiTestUtils.createEntries(
      apiTestUtils.generateTestEntries(userId, ENTRIES_PER_USER))
    )
    .then((createdEntries) => {
      userEntryId = createdEntries[0]._id;
      userEntryId2 = createdEntries[1]._id;
    })
    .then(() => apiTestUtils.createEntries(
      apiTestUtils.generateTestEntries(managerId, ENTRIES_PER_USER))
    )
    .then((createdEntries) => {
      managerEntryId = createdEntries[0]._id;
    })
    .then(() => apiTestUtils.createEntries(
      apiTestUtils.generateTestEntries(adminId, ENTRIES_PER_USER))
    )
    .then((createdEntries) => {
      adminEntryId = createdEntries[0]._id;
    })
    .then(() => done(), done)
  });

  describe('/entries GET', function() {

    describe('For non-authenticated users', function() {
      it('should not allow to list all entries', function(done) {
        request(server)
          .get(`${API_PREFIX}/entries/?utcOffset=${utcOffset}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(401, done);
      });
    });

    describe('For regular users', function() {
      it('should not allow to list all entries', function(done) {
        request(server)
          .get(`${API_PREFIX}/entries/?utcOffset=${utcOffset}`)
          .set('Authorization', `Bearer ${userToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(401, done);
      });
    });

    describe('For manager users', function() {
      it('should not allow to list all entries', function(done) {
        request(server)
          .get(`${API_PREFIX}/entries/?utcOffset=${utcOffset}`)
          .set('Authorization', `Bearer ${managerToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(401, done);
      });
    });

    describe('for admins', function() {
      it('should allow to list all entries', function(done) {
        request(server)
          .get(`${API_PREFIX}/entries/?utcOffset=${utcOffset}`)
          .set('Authorization', `Bearer ${adminToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(200)
          .expect((res) => {
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('data');
            res.body.data.should.be.a('array').of.length(3 * ENTRIES_PER_USER);
            for (const entry of res.body.data) {
              entry.should.be.a('object');
              entry.should.have.property('date');
              entry.should.have.property('trackedSeconds');
              entry.should.have.property('notes');
              entry.should.have.property('user');
              entry.user.should.be.a('object');
              entry.user.should.have.property('_id');
              entry.user.should.have.property('preferredHoursPerDay').equal(PREFFERED_HOURS_PER_DAY);
            }
          })
          .end(done);
      });
    });
  });

  describe('/entries/:entryId GET', function() {

    describe('For non-authenticated users', function() {
      it('should return 401', function(done) {
        request(server)
          .get(`${API_PREFIX}/entries/?utcOffset=${utcOffset}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(401, done);
      });
    });

    const roleTestCases = [
      {
        name: 'for regular users',
        token: () => userToken,
        getEntryId: () => userEntryId,
      },
      {
        name: 'for managers',
        token: () => managerToken,
        getEntryId: () => managerEntryId,
      },
    ];
    for (const { name, token, getEntryId } of roleTestCases) {
      describe(name, function() {
        it('should return an own entry', function(done) {
          request(server)
            .get(`${API_PREFIX}/entries/${getEntryId()}?utcOffset=${utcOffset}`)
            .set('Authorization', `Bearer ${token()}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200)
            .expect((res) => {
              res.should.be.json;
              res.body.should.be.a('object');
              res.body.should.have.property('date');
              res.body.should.have.property('trackedSeconds');
              res.body.should.have.property('notes');
            })
            .end(done);
        });

        it('should return 404 for a non-existent entry', function(done) {
          request(server)
            .get(`${API_PREFIX}/entries/no-such-entry-id?utcOffset=${utcOffset}`)
            .set('Authorization', `Bearer ${token()}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(404)
            .end(done);
        });

        it('should return 404 for an entry of another user', function(done) {
          request(server)
            .get(`${API_PREFIX}/entries/${adminEntryId}?utcOffset=${utcOffset}`)
            .set('Authorization', `Bearer ${token()}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(404)
            .end(done);
        });
      });
    }

    describe('For admins', function() {
      it('should return entry of another user', function(done) {
        request(server)
          .get(`${API_PREFIX}/entries/${userEntryId2}?utcOffset=${utcOffset}`)
          .set('Authorization', `Bearer ${adminToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(200)
          .expect((res) => {
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('date');
            res.body.should.have.property('trackedSeconds');
            res.body.should.have.property('notes');
          })
          .end(done);
      });
    });
  });

  describe('/users/:userId/entries GET', function() {

    describe('For non-authenticated users', function() {
        it('should not allow to list entries', function(done) {
          request(server)
            .get(`${API_PREFIX}/users/a-user-id/entries/?utcOffset=${utcOffset}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(401, done);
        });
    });

    describe('For regular users', function() {
      it('should allow to list own entries', function(done) {
        request(server)
          .get(`${API_PREFIX}/users/${userId}/entries/?utcOffset=${utcOffset}`)
          .set('Authorization', `Bearer ${userToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(200)
          .expect((res) => {
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('data');
            res.body.data.should.be.a('array').of.length(ENTRIES_PER_USER);
            for (const entry of res.body.data) {
              entry.should.be.a('object');
              entry.should.have.property('date');
              entry.should.have.property('trackedSeconds');
              entry.should.have.property('notes');
              entry.should.have.property('user');
              entry.user.should.be.a('object');
              entry.user.should.have.property('_id').equal(userId);
              entry.user.should.have.property('preferredHoursPerDay').equal(PREFFERED_HOURS_PER_DAY);
            }
          })
          .end(done);
      });

      it('should not allow to list entries of other users', function(done) {
        request(server)
          .get(`${API_PREFIX}/users/${managerId}/entries/?utcOffset=${utcOffset}`)
          .set('Authorization', `Bearer ${userToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(401, done);
      });
    });

    describe('For managers', function() {
      it('should allow to list own entries', function(done) {
        request(server)
          .get(`${API_PREFIX}/users/${managerId}/entries/?utcOffset=${utcOffset}`)
          .set('Authorization', `Bearer ${managerToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(200)
          .expect((res) => {
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('data');
            res.body.data.should.be.a('array').of.length(ENTRIES_PER_USER);
            for (const entry of res.body.data) {
              entry.should.be.a('object');
              entry.should.have.property('date');
              entry.should.have.property('trackedSeconds');
              entry.should.have.property('notes');
              entry.should.have.property('user');
              entry.user.should.be.a('object');
              entry.user.should.have.property('_id').equal(managerId);
              entry.user.should.have.property('preferredHoursPerDay').equal(PREFFERED_HOURS_PER_DAY);
            }
          })
          .end(done);
      });

      it('should not allow to list entries of other users', function(done) {
        request(server)
          .get(`${API_PREFIX}/users/${userId}/entries/?utcOffset=${utcOffset}`)
          .set('Authorization', `Bearer ${managerToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(401, done);
      });
    });

    describe('For admins', function() {
      it('should allow to list own entries', function(done) {
        request(server)
          .get(`${API_PREFIX}/users/${adminId}/entries/?utcOffset=${utcOffset}`)
          .set('Authorization', `Bearer ${adminToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(200)
          .expect((res) => {
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('data');
            res.body.data.should.be.a('array').of.length(ENTRIES_PER_USER);
            for (const entry of res.body.data) {
              entry.should.be.a('object');
              entry.should.have.property('date');
              entry.should.have.property('trackedSeconds');
              entry.should.have.property('notes');
              entry.should.have.property('user');
              entry.user.should.be.a('object');
              entry.user.should.have.property('_id').equal(adminId);
              entry.user.should.have.property('preferredHoursPerDay').equal(PREFFERED_HOURS_PER_DAY);
            }
          })
          .end(done);
      });

      it('should allow to list entries of other users', function(done) {
        request(server)
          .get(`${API_PREFIX}/users/${userId}/entries/?utcOffset=${utcOffset}`)
          .set('Authorization', `Bearer ${adminToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(200)
          .expect((res) => {
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('data');
            res.body.data.should.be.a('array').of.length(ENTRIES_PER_USER);
            for (const entry of res.body.data) {
              entry.should.be.a('object');
              entry.should.have.property('date');
              entry.should.have.property('trackedSeconds');
              entry.should.have.property('notes');
              entry.should.have.property('user');
              entry.user.should.be.a('object');
              entry.user.should.have.property('_id').equal(userId);
              entry.user.should.have.property('preferredHoursPerDay').equal(PREFFERED_HOURS_PER_DAY);
            }
          })
          .end(done);
      });
    });
  });

  describe('/users/:userId/entries POST', function() {

    describe('For non-authenticated users', function() {
      it('should not allow to create entries', function(done) {
        request(server)
          .post(`${API_PREFIX}/users/a-user-id/entries/?utcOffset=${utcOffset}`)
          .send({
            trackedSeconds: 1 * 60 * 60,
          })
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(401, done);
      });
    });

    describe('For regular users', function() {
      it('should create own entries', function(done) {
        request(server)
          .post(`${API_PREFIX}/users/${userId}/entries/?utcOffset=${utcOffset}`)
          .send({
            date: new Date(2016, 0, 1, 7, 0, 0).getTime(),
            trackedSeconds: 1 * 60 * 60,
          })
          .set('Authorization', `Bearer ${userToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(200)
          .expect((res) => {
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('id');
          })
          .end(done);
      });

      it('should allow to create a second entry with the same date', function(done) {
        request(server)
          .post(`${API_PREFIX}/users/${userId}/entries/?utcOffset=${utcOffset}`)
          .send({
            date: new Date(2016, 0, 1, 10, 0, 0).getTime(),
            trackedSeconds: 1 * 60 * 60,
          })
          .set('Authorization', `Bearer ${userToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(200)
          .end(done);
      });

      it('should not allow to create an entry with a future date', function(done) {
        request(server)
          .post(`${API_PREFIX}/users/${userId}/entries/?utcOffset=${utcOffset}`)
          .send({
            date: (new Date().getTime() + 2 * 24 * 3600 * 1000),
            trackedSeconds: 1 * 60 * 60,
          })
          .set('Authorization', `Bearer ${userToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(400)
          .end(done);
      });

      it('should not allow to create entries for other users', function(done) {
        request(server)
          .post(`${API_PREFIX}/users/${managerId}/entries/?utcOffset=${utcOffset}`)
          .send({
            date: new Date(2016, 0, 1, 10, 0, 0).getTime(),
            trackedSeconds: 1 * 60 * 60,
          })
          .set('Authorization', `Bearer ${userToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(401, done);
      });
    });

    describe('For managers users', function() {
      it('should not allow to create entries for other users', function(done) {
        request(server)
          .post(`${API_PREFIX}/users/${userId}/entries/?utcOffset=${utcOffset}`)
          .send({
            date: new Date(2016, 0, 1, 10, 0, 0).getTime(),
            trackedSeconds: 1 * 60 * 60,
          })
          .set('Authorization', `Bearer ${managerToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(401)
          .end(done);
      });
    });

    describe('For admin users', function() {
      it('should create entries for other users', function(done) {
        request(server)
          .post(`${API_PREFIX}/users/${managerId}/entries/?utcOffset=${utcOffset}`)
          .send({
            date: new Date(2016, 0, 1, 10, 0, 0).getTime(),
            trackedSeconds: 1 * 60 * 60,
          })
          .set('Authorization', `Bearer ${adminToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(200)
          .expect((res) => {
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('id');
          })
          .end(done);
      });
    });
  });

  describe('/entries/:entryId PATCH', function() {

    describe('Input validation', function() {

      it('should return 404 for non-existent entry', function(done) {
        request(server)
          .patch(`${API_PREFIX}/entries/an-entry-id/`)
          .send({
            trackedSeconds: 1 * 60 * 60,
          })
          .set('Authorization', `Bearer ${userToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(404)
          .end(done);
      });

      it('should check trackedSeconds', function(done) {
        function checkTrackedSeconds(value, status=400) {
          return Promise.resolve(
            request(server)
              .patch(`${API_PREFIX}/entries/${userEntryId}/`)
              .send({
                trackedSeconds: value,
              })
              .set('Authorization', `Bearer ${userToken}`)
              .set('Content-Type', 'application/json')
              .set('Accept', 'application/json')
              .expect(status));
        }
        const testCases = [
          [-1],
          [null],
          ['a string'],
          [24 * 3600 + 1],
        ];
        Promise.all(testCases.map(test => checkTrackedSeconds(...test)))
          .then(() => done(), done);
      });

      it('should check date', function(done) {
        function checkDate(value, status=400) {
          return Promise.resolve(
            request(server)
              .patch(`${API_PREFIX}/entries/${userEntryId}/`)
              .send({
                date: value,
              })
              .set('Authorization', `Bearer ${userToken}`)
              .set('Content-Type', 'application/json')
              .set('Accept', 'application/json')
              .expect(status));
        }
        const testCases = [
          [-1],
          [null],
          ['a string'],
          [new Date().toISOString()],
          [new Date().getTime(), 200],
          [new Date().getTime() + 2 * 24 * 3600 * 1000, 400],
        ];
        Promise.all(testCases.map(test => checkDate(...test)))
          .then(() => done(), done);
      });
    });

    describe('For non-authenticated users', function() {
      it('should not allow to edit entries', function(done) {
        request(server)
          .patch(`${API_PREFIX}/entries/an-entry-id/`)
          .send({
            trackedSeconds: 1 * 60 * 60,
          })
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(401, done);
      });
    });

    describe('For regular users', function() {
      it('should not allow to edit entries of other users', function(done) {
        request(server)
          .patch(`${API_PREFIX}/entries/${managerEntryId}/`)
          .send({
            trackedSeconds: 1 * 60 * 60,
          })
          .set('Authorization', `Bearer ${userToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(401)
          .end(done);
      });

      it('should edit own entries', function(done) {
        request(server)
          .patch(`${API_PREFIX}/entries/${userEntryId}/`)
          .send({
            trackedSeconds: 1 * 60 * 60,
          })
          .set('Authorization', `Bearer ${userToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(200)
          .expect((res) => {
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('trackedSeconds');
            res.body.trackedSeconds.should.equal(1 * 60 * 60);
          })
          .end(done);
      });
    });

    describe('For managers users', function() {

      it('should not allow to edit entries of other users', function(done) {
        request(server)
          .patch(`${API_PREFIX}/entries/${userEntryId}/`)
          .send({
            trackedSeconds: 1 * 60 * 60,
          })
          .set('Authorization', `Bearer ${managerToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(401)
          .end(done);
      });

      it('should edit own entries', function(done) {
        request(server)
          .patch(`${API_PREFIX}/entries/${managerEntryId}/`)
          .send({
            trackedSeconds: 1 * 60 * 60,
          })
          .set('Authorization', `Bearer ${managerToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(200)
          .expect((res) => {
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('trackedSeconds');
            res.body.trackedSeconds.should.equal(1 * 60 * 60);
          })
          .end(done);
      });
    });

    describe('For admin users', function() {

      it('should edit own entries', function(done) {
        request(server)
          .patch(`${API_PREFIX}/entries/${adminEntryId}/`)
          .send({
            trackedSeconds: 1 * 60 * 60,
          })
          .set('Authorization', `Bearer ${adminToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(200)
          .expect((res) => {
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('trackedSeconds');
            res.body.trackedSeconds.should.equal(1 * 60 * 60);
          })
          .end(done);
      });

      it('should edit entries of other users', function(done) {
        request(server)
          .patch(`${API_PREFIX}/entries/${managerEntryId}/`)
          .send({
            trackedSeconds: 1 * 60 * 60,
            date: new Date(1999, 1, 1).getTime(),
          })
          .set('Authorization', `Bearer ${adminToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(200)
          .expect((res) => {
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('trackedSeconds');
            res.body.trackedSeconds.should.equal(1 * 60 * 60);
          })
          .end(done);
      });
    });
  });

  describe('/entries/:entryId DELETE', function() {

    describe('For non-authenticated users', function() {
      it('should not allow to delete entries', function(done) {
        request(server)
          .delete(`${API_PREFIX}/entries/an-entry-id/`)
          .send({
            trackedSeconds: 1 * 60 * 60,
          })
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(401, done);
      });
    });

    describe('For regular users', function() {

      it('should not allow to delete entries of other users', function(done) {
        request(server)
          .delete(`${API_PREFIX}/entries/${managerEntryId}/`)
          .set('Authorization', `Bearer ${userToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(401)
          .end(done);
      });

      it('should delete own entries', function(done) {
        Promise.resolve(
          request(server)
            .delete(`${API_PREFIX}/entries/${userEntryId}/`)
            .set('Authorization', `Bearer ${userToken}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200)
        )
        .then(() => apiTestUtils.getEntry({ id: userEntryId }))
        .then((result) => expect(result).to.be.null)
        .then(() => done(), done);
      });
    });

    describe('For managers users', function() {

      it('should not allow to delete entries of other users', function(done) {
        request(server)
          .delete(`${API_PREFIX}/entries/${adminEntryId}/`)
          .set('Authorization', `Bearer ${managerToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(401)
          .end(done);
      });

      it('should delete own entries', function(done) {
        Promise.resolve(
          request(server)
            .delete(`${API_PREFIX}/entries/${managerEntryId}/`)
            .set('Authorization', `Bearer ${managerToken}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200)
        )
        .then(() => apiTestUtils.getEntry({ id: managerEntryId }))
        .then((result) => expect(result).to.be.null)
        .then(() => done(), done);
      });
    });


    describe('For admin users', function() {

      it('should delete entries of other users', function(done) {
        Promise.resolve(
          request(server)
            .delete(`${API_PREFIX}/entries/${userEntryId2}/`)
            .set('Authorization', `Bearer ${adminToken}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200)
        )
        .then(() => apiTestUtils.getEntry({ id: userEntryId2 }))
        .then((result) => expect(result).to.be.null)
        .then(() => done(), done);
      });
    });
  });
});
