const request = require('supertest');
const moment = require('moment');

const createServer = require('root/server/server');
const server = createServer();

const User = require('root/server/models/User');
const Entry = require('root/server/models/Entry');

const API_PREFIX = '/api/v0';

export function generateTestEntries(userId, n) {
  const entriesData = [];
  let date = moment.utc([2011, 2, 3]);
  for (let i = 0; i < n; ++i) {
    date = date.add(1, 'days').add(24 * 3600 * Math.random(), 'seconds');
    entriesData.push({
      user: userId,
      date: date.valueOf(),
      trackedSeconds: Math.round(Math.random() * 5 * 3600),
    });
  }
  return entriesData;
}

export function removeUsers(data={}) {
  return Promise.resolve(User.remove(data));
}

export function removeAllUsers() {
  return removeUsers();
}

export function removeEntries(data={}) {
  return Promise.resolve(Entry.remove(data));
}

export function removeAllEntries() {
  return removeEntries();
}

export function createEntry(data) {
  return Promise.resolve(Entry.create(data));
}

export function createEntries(data) {
  return Promise.all(data.map((entry) => createEntry(entry)));
}

export function getEntry(data) {
  return Promise.resolve(Entry.findOne(data));
}

export function createUser(data) {
  return Promise.resolve(User.create(data));
}

export function getUser(data) {
  return Promise.resolve(User.findOne(data));
}

export function getUsers(data) {
  return Promise.resolve(User.find(data));
}

export function login(data) {
  return Promise.resolve(
    request(server)
      .post(`${API_PREFIX}/login`)
      .send(data)
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(200)
    ).then(res => res.body);
}

export function createUserAndLogin(data) {
  const loginData = {
    email: data.email,
    password: data.password,
  };
  return createUser(data).then(() => login(loginData));
}
