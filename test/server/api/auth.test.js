const request = require('supertest');
const chai = require('chai');
const chaiHttp = require('chai-http');

const apiTestUtils = require('./utils');
const server = require('root/server');

const should = chai.should();
chai.use(chaiHttp);

const API_PREFIX = '/api/v0';

describe('Auth', function() {
  let tokenOfDeletedUser;

  before((done) => {
    apiTestUtils.removeAllUsers()
    .then(() => apiTestUtils.createUserAndLogin({
      name: 'baz',
      email: 'baz@bar.com',
      password: 'qwerty',
    }))
    .then(({ token, user: { id } }) => {
      tokenOfDeletedUser = token;
      return apiTestUtils.removeUsers({ _id: id });
    })
    .then(() => apiTestUtils.createUser({
      name: 'foobar',
      email: 'foo@bar.com',
      password: 'qwerty',
    }))
    .then(() => done())
    .catch(done);
  });

  describe('/login POST', function() {
    it('Should not login with non-existent email', function(done) {
      request(server)
        .post(`${API_PREFIX}/login`)
        .send({
          'email': 'no-such-email@domain.com',
          'password': 'doesnt-relly-matter',
        })
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .expect(401, done);
    });

    it('Should login and return user and token', function(done) {
      request(server)
        .post(`${API_PREFIX}/login`)
        .send({
          email: 'foo@bar.com',
          password: 'qwerty',
        })
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .expect(200, done);
    });
  });

  describe('/signup POST', function() {

    describe('Input validation', function() {
      it('Should not signup with a blank email', function(done) {
        request(server)
          .post(`${API_PREFIX}/signup`)
          .send({})
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(400, done);
      });

      it('Should not signup with a blank name', function(done) {
        request(server)
          .post(`${API_PREFIX}/signup`)
          .send({
            email: 'foo@bar.com',
            password: 'qwerty',
          })
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(400, done);
      });

      it('Should not signup with a blank password', function(done) {
        request(server)
          .post(`${API_PREFIX}/signup`)
          .send({
            email: 'foo@bar.com',
            name: 'foobar',
          })
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(400, done);
      });

      it('Should not signup with an invalid email', function(done) {
        request(server)
          .post(`${API_PREFIX}/signup`)
          .send({
            email: 'foo@bar',
            name: 'foobar',
            password: 'qwerty',
          })
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(400, done);
      });

      it('Should not signup with a short username', function(done) {
        request(server)
          .post(`${API_PREFIX}/signup`)
          .send({
            email: 'foo@bar.com',
            name: 'f',
            password: 'qwerty',
          })
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(400, done);
      });

      it('Should not signup with a short password', function(done) {
        request(server)
          .post(`${API_PREFIX}/signup`)
          .send({
            email: 'foo@bar.com',
            name: 'fo',
            password: 'abc',
          })
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(400, done);
      });

      it('Should not sign up with an already exising email', function(done) {
        request(server)
          .post(`${API_PREFIX}/signup`)
          .send({
            email: 'foo@bar.com',
            name: 'foobar',
            password: 'qwerty',
          })
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(400, done);
      });
    });

    describe('Signup', function() {
      it('Signup should return a token and the created user', function(done) {
        request(server)
          .post(`${API_PREFIX}/signup`)
          .send({
            email: 'foo@bar2.com',
            name: 'foobar2',
            password: 'qwerty',
          })
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(200)
          .expect((req) => {
            req.should.be.json;
            req.body.should.be.a('object');
            req.body.should.have.property('token');
            req.body.token.should.be.a('string');
            req.body.should.have.property('user');
            req.body.user.should.be.a('object');
            req.body.user.should.have.property('email').to.equal('foo@bar2.com');
            req.body.user.should.have.property('name').to.equal('foobar2');
            req.body.user.should.have.property('role').to.equal('user');
            req.body.user.should.have.property('gravatar');
            req.body.user.should.have.property('id');
            req.body.user.should.not.have.property('password');
          })
          .end(done)
      });
    });
  });

  describe('/account PATCH', function() {
    let token;

    before((done) => {
      apiTestUtils.login({
        email: 'foo@bar.com',
        password: 'qwerty',
      })
      .then(({ token: authToken }) => {
        token = authToken;
        done();
      });
    });

    describe('Should be able to update profile', function() {
      it('Should not update if non authenticated', function(done) {
        request(server)
          .patch(`${API_PREFIX}/account`)
          .send({
            password: 'new_password',
            confirm: 'new_password_not_really',
          })
          .set('Accept', 'application/json')
          .expect(401, done);
      });

      it('Should not be able to access with invalid token', function(done) {
        request(server)
          .patch(`${API_PREFIX}/account`)
          .send({
            password: 'new_password',
            confirm: 'new_password',
          })
          .set('Authorization', `Bearer non-existent-token`)
          .set('Accept', 'application/json')
          .expect(400, done);
      });

      it('Should not be able to access with a valid token with no user associated to it', function(done) {
        request(server)
          .patch(`${API_PREFIX}/account`)
          .send({
            password: 'new_password',
            confirm: 'new_password',
          })
          .set('Authorization', `Bearer ${tokenOfDeletedUser}`)
          .set('Accept', 'application/json')
          .expect(401, done);
      });

      it('Should not update if doesn\'t match confirm password', function(done) {
        request(server)
          .patch(`${API_PREFIX}/account`)
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({
            password: 'new_password',
            confirm: 'new_password_not_really',
          })
          .expect(400, done);
      });

      it('Should update password', function(done) {
        request(server)
          .patch(`${API_PREFIX}/account`)
          .send({
            password: 'new_password',
            confirm: 'new_password',
          })
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .expect(200)
          .expect((req) => {
            req.should.be.json;
            req.body.should.be.a('object');
            req.body.should.have.property('user');
            req.body.user.should.be.a('object');
            req.body.user.should.not.have.property('password');
          })
          .end(done);
      });

      it('Should update profile', function(done) {
        request(server)
          .patch(`${API_PREFIX}/account`)
          .send({
            email: 'new@email.com',
            preferredHoursPerDay: 3,
            location: 'London',
            website: 'my-shiny-website',
          })
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .expect(200)
          .expect((req) => {
            req.should.be.json;
            req.body.should.be.a('object');
            req.body.should.have.property('user');
            req.body.user.should.be.a('object');
            req.body.user.should.have.property('email').equal('new@email.com');
            req.body.user.should.have.property('name').equal('foobar');
            req.body.user.should.have.property('preferredHoursPerDay').equal(3);
            req.body.user.should.have.property('location').equal('London');
            req.body.user.should.have.property('website').equal('my-shiny-website');
          })
          .end(done);
      });
    });
  });

  describe('/account DELETE', function() {
    let token;

    before((done) => {
      apiTestUtils.removeAllUsers()
      .then(() => apiTestUtils.createUser({
        name: 'foobar',
        email: 'foo@bar.com',
        password: 'qwerty',
      }))
      .then(() => apiTestUtils.login({
        email: 'foo@bar.com',
        password: 'qwerty',
      }))
      .then(({ token: authToken }) => {
        token = authToken;
        done();
      })
      .catch(done);
    });

    it('Should delete profile', function(done) {
      request(server)
        .delete(`${API_PREFIX}/account`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .end(done);
    });

    it('Should not login after account was deleted', function(done) {
      request(server)
        .post(`${API_PREFIX}/login`)
        .send({
          email: 'foo@bar.com',
          password: 'qwerty',
        })
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .expect(401, done);
    });
  });
});
