const request = require('supertest');
const chai = require('chai');
const uuid = require('uuid-js');
const chaiHttp = require('chai-http');
const moment = require('moment');

const apiTestUtils = require('./utils');
const server = require('root/server');
const USER_ROLES = require('root/server/models/constants').USER_ROLES;
const API_ENTRIES_MAX_LIMIT = require('root/server/constants').API_ENTRIES_MAX_LIMIT;

const should = chai.should();
chai.use(chaiHttp);

const API_PREFIX = '/api/v0';

const utcOffset = moment().utcOffset();

describe('Pagination', function() {
  let ownId, token, entries;
  const TEST_ENTRIES_COUNT = 20;
  const entriesData = [];

  before((done) => {
    apiTestUtils.removeAllUsers()
    apiTestUtils.removeAllEntries()
    .then(() => apiTestUtils.createUserAndLogin({
      name: 'admin',
      email: 'admin@foo.com',
      password: 'qwerty',
      role: USER_ROLES.ADMIN,
    }))
    .then(({ token: authToken, user }) => {
      token = authToken;
      ownId = user.id;
    })
    .then(() => apiTestUtils.createEntries(
      apiTestUtils.generateTestEntries(ownId, TEST_ENTRIES_COUNT))
    )
    .then(() => done(), done);
  });

  const urlTestCases = [
    { url: () => `${API_PREFIX}/users/${ownId}/entries/`, name: 'GET /users/:userId/entries/' },
    { url: () => `${API_PREFIX}/entries/`, name: 'GET /entries/' },
  ];
  for (const { url, name } of urlTestCases) {

    describe(name, function() {
      describe('returns paginated response', function() {
        let response;
        const params = {
          limit: 5,
          offset: 11,
        };

        before((done) => {
          Promise.resolve(request(server)
            .get(url())
            .query(Object.assign({}, params, { utcOffset }))
            .set('Authorization', `Bearer ${token}`)
            .set('Accept', 'application/json')
            .expect(200))
          .then(res => response = res)
          .then(() => done(), done);
        });

        it('with field "data" holding list of entries', function() {
          response.should.be.json;
          response.body.should.be.a('object');
          response.body.should.have.property('data');
          response.body.data.should.be.a('array').of.length(params.limit);
        });

        it('with field "limit" holding number of entries in the current response', function() {
          response.body.should.have.property('limit');
          response.body.limit.should.be.a('number');
          response.body.limit.should.equal(params.limit);
          response.body.limit.should.equal(response.body.data.length);
        });

        it('with field "total" holding total number of entries', function() {
          response.body.should.have.property('total');
          response.body.total.should.be.a('number');
          response.body.total.should.equal(TEST_ENTRIES_COUNT);
        });

        it('with field "offset" holding the current offset', function() {
          response.body.should.have.property('offset');
          response.body.offset.should.be.a('number');
          response.body.offset.should.equal(params.offset);
        });
      });

      describe('handles', function() {

        function testGetEntries({ query, status=200, values={} }) {
          it(`for ${JSON.stringify(query)} returns status ${status}`, function(done) {
            Promise.resolve(
              request(server)
                .get(url())
                .query(Object.assign({}, query, { utcOffset }))
                .set('Authorization', `Bearer ${token}`)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json')
                .expect(status)
                .expect((res) => {
                  res.should.be.json;
                  for (const valueKey of Object.keys(values)) {
                    res.body.should.have.property(valueKey);
                    res.body[valueKey].should.equal(values[valueKey]);
                  }
                })
            ).then(() => done(), done);
          });
        }

        describe('valid inputs', function() {
          const testCases = [
            {
              query: {},
              values: { limit: 10, offset: 0, total: TEST_ENTRIES_COUNT },
            },
            {
              query: { limit: 100000 },
              values: { limit: API_ENTRIES_MAX_LIMIT, offset: 0, total: TEST_ENTRIES_COUNT },
            },
            {
              query: { limit: 10, offset: 100000 },
              values: { limit: 10, offset: 100000, total: TEST_ENTRIES_COUNT },
            },
            {
              query: { limit: 1 },
              values: { limit: 1, offset: 0, total: TEST_ENTRIES_COUNT },
            },
            {
              query: { limit: 0, },
              status: 200,
              values: { limit: 0, offset: 0, total: TEST_ENTRIES_COUNT },
            },
          ];
          testCases.forEach(testGetEntries);
        });

        describe('invalid inputs', function() {
          const testCases = [
            { query: { offset: -1 }, status: 400 },
            { query: { limit: 'abc', }, status: 400 },
            { query: { offset: 'abc', }, status: 400 },
            { query: { limit: NaN, }, status: 400 },
            { query: { offset: NaN, }, status: 400 },
            { query: { offset: null, }, status: 400 },
          ];
          testCases.forEach(testGetEntries);
        });
      });
    });
  }
});
