const request = require('supertest');
const moment = require('moment');
const chai = require('chai');
const chaiHttp = require('chai-http');

const apiTestUtils = require('./utils');
const server = require('root/server');
const USER_ROLES = require('root/server/models/constants').USER_ROLES;

const should = chai.should();
const expect = chai.expect;
chai.use(chaiHttp);

const API_PREFIX = '/api/v0';

const utcOffset = moment().utcOffset();

describe('Entries tracked time aggregation', function() {

  const N_DATES = 10;
  const ENTRIES_PER_DATE = 20;
  let adminToken;
  let userId1, userId2;
  let allEntries;

  before((done) => {
    apiTestUtils.removeAllUsers()
    .then(() => apiTestUtils.removeAllEntries())
    .then(() => apiTestUtils.createUserAndLogin({
      name: 'admin',
      email: 'admin@foo.com',
      password: 'qwerty',
      role: USER_ROLES.ADMIN,
    }))
    .then(({ token: authToken }) => {
      adminToken = authToken;
    })
    .then(() => apiTestUtils.createUserAndLogin({
      name: 'user1',
      email: 'user1@foo.com',
      password: 'qwerty',
      role: USER_ROLES.USER,
    }))
    .then(({ user }) => {
      userId1 = user.id;
    })
    .then(() => apiTestUtils.createUserAndLogin({
      name: 'user2',
      email: 'user2@foo.com',
      password: 'qwerty',
      role: USER_ROLES.USER,
    }))
    .then(({ user }) => {
      userId2 = user.id;
    })
    .then(() => {
      const entries = [];
      for (const userId of [userId1, userId2]) {
        let date = moment.utc([2011, 2, 3]);
        for (let i = 0; i < N_DATES; ++i) {
          for (let j = 0; j < ENTRIES_PER_DATE; ++j) {
            entries.push({
              user: userId,
              date: date.valueOf(),
              trackedSeconds: Math.round(Math.random() * 5 * 3600),
            });
          }
          date = date.add(1, 'days');
        }
      }
      allEntries = entries;
      return apiTestUtils.createEntries(entries);
    })
    .then(() => done(), done)
  });

  const testCases = [
    { name: 'GET /entries/', url: () => '/entries/' },
    { name: 'GET /user/:userId/entries/', url: () => `/users/${userId1}/entries/` },
  ];
  for (const { name, url } of testCases) {
    describe(name, function() {
      it('entries should have totalTrackedSeconds for corresponding dates', function(done) {
        request(server)
          .get(`${API_PREFIX}${url()}?limit=78&utcOffset=${utcOffset}`)
          .set('Authorization', `Bearer ${adminToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .expect(200)
          .expect((res) => {
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('data');
            res.body.data.should.be.a('array').of.length(78);
            for (const entry of res.body.data) {
              entry.should.be.a('object');
              entry.should.have.property('date');
              entry.should.have.property('user');
              entry.user.should.have.property('id');

              let totalTrackedSeconds = 0;
              for (const e of allEntries) {
                if (entry.user.id === e.user && e.date === entry.date) {
                  totalTrackedSeconds += e.trackedSeconds;
                }
              }
              entry.should.have.property('totalTrackedSeconds').equal(totalTrackedSeconds);
            }
          })
          .end(done);
      });
    });
  }
});
