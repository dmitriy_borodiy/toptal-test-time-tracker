const request = require('supertest');
const chai = require('chai');
const chaiHttp = require('chai-http');

const should = chai.should();
chai.use(chaiHttp);

const apiTestUtils = require('root/test/server/api/utils');
const server = require('root/server');

describe('GET /', function() {
  let token;
  before((done) => {
    apiTestUtils.removeAllUsers()
    .then(() => apiTestUtils.createUserAndLogin({
      name: 'user',
      email: 'user@foo.com',
      password: 'qwerty',
    }))
    .then(({ token: authToken }) => {
      token = authToken;
      done();
    })
    .catch(done);
  });

  describe('For unauthenticated users', function() {
    it('should redirect to /login', function(done) {
      request(server)
        .get('/')
        .expect(302)
        .expect((res) => {
          res.header.should.have.property('location').equal('/login');
        })
        .end(done);
    });
  });

  describe('For unauthenticated users', function() {
    it('should render HTML', function(done) {
      request(server)
        .get('/')
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .expect((res) => {
          res.type.should.equal('text/html');
        })
        .end(done);
    });
  });
});
